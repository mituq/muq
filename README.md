# README #

Note that this version of MUQ has been deprecated and is no longer being actively supported.  Please find the latest version of MUQ, aptly named "MUQ2", in our [MUQ2 repository](https://bitbucket.org/mituq/muq2).

If you are currently using a capability that has not yet been added to MUQ2, please create an issue on the MUQ2 repository and we will try to add that capability as soon as possible.
