
#ifndef _FRAuctioneer_h
#define _FRAuctioneer_h


// eigen includes
#include <Eigen/Core>

// std library includes
#include <set>

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace muq {
namespace Optimization {
class FRAuctioneer {
public:

  FRAuctioneer(const Eigen::MatrixXd& dists);

  FRAuctioneer(const Eigen::MatrixXd& xi, const Eigen::MatrixXd& xo);


  void             solve(const std::string& paramFile);

  void             solve(boost::property_tree::ptree& params);

  void             solve();


  std::vector<int> GetSolution() const
  {
    return Bidder2Object;
  }

private:

  void solveEps(double eps);

  // get the indices of the maximum and second maximum values for the bidder
  void getPriceMaxes(int bidder, int& maxObj, int& secObj);

  //void getProfitMaxes(int object, int &maxBidder, int &secBidder);

  // store the benefit matrix
  const Eigen::MatrixXd a;
  const int size;

  //std::queue<int> UnassignedObjects;
  std::set<int> UnassignedBidders;

  // store the assignment
  std::vector<int> Bidder2Object;
  std::vector<int> Object2Bidder;

  // store the prices and profits
  Eigen::VectorXd prices;

  //Eigen::VectorXd profits;
};
} // namespace Optimization
} // namespace muq
#endif // ifndef _FRAuctioneer_h
