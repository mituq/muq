
#ifndef _RobbinsMonro_h
#define _RobbinsMonro_h

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
namespace Optimization {
/** @class RobbinsMonro
 *   @ingroup Optimization
 *   @brief Implementation of the Robbins-Monro stochastic approximation method applied to stochastic optimization.
 *
 *   @details The Robbins-Monro stochastic approximation method applied to optimization is a way of optimizing an
 *      objective of the form E[f(x)], where f(x) is some scalar random variable dependent on some design parameters x.
 *       This class implements a simple version of the method.
 */
class RobbinsMonro : public OptAlgBase {
public:

  REGISTER_OPT_CONSTRUCTOR(RobbinsMonro)
  /** Construct an instance of the RobbinsMonro method with the parameters found in paramList.
   *  @param[in] ProbPtr A shared pointer to the optimization problem
   *  @param[in] properties A boost ptree holding the line search settings and any SD-specific parameters
   */
  RobbinsMonro(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);


  /** Solve the stochastic optimization problem using x0 as a starting point for the iteration.
   *  @param[in] x0 The initial point for the optimizer.
   *  @return A vector holding the best point found by the optimizer.  Note that the optimizer may not have converged
   * and
   *     the GetStatus() function should be called to ensure that the algorithm converged and this vector is meaningful.
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0);

protected:

  // factor describing "forgetting factor" in AvgState window -- bn = b0/it
  double b0;
  double k;

  // old function value and new function value
  double fold, fnew;

  // step size reduction parameter
  double beta;

  // the power in time -- must be less than or equal to 1 for convergence
  double epsPow;

private:

  REGISTER_OPT_DEC_TYPE(RobbinsMonro)
};

// class RobbinsMonro
} // namespace Optimization
} //namespace muq


#endif // ifndef _RobbinsMonro_h
