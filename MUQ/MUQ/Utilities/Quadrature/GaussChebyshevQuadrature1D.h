#ifndef GAUSSCHEBYSHEVQUADRATURE1D_H_
#define GAUSSCHEBYSHEVQUADRATURE1D_H_

#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * Implements Gauss-Chebyshev quadrature, i.e. weighted with 1/sqrt(1-x)^2 over [-1,1].
 *
 * This class directly implements QuadratureFamily1D because there is an analytic form
 * for the nodes and weights.
 */
class GaussChebyshevQuadrature1D : public QuadratureFamily1D {
public:

  GaussChebyshevQuadrature1D();
  virtual ~GaussChebyshevQuadrature1D();


  ///Implements the polynomial order. Gauss-Chebyshev accuracy is approximate.
  unsigned int GetPrecisePolyOrder(unsigned int const order) const;

private:

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);


  ///Implementations must provide a way of deriving the nth rule
  virtual void ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const;
};
}
}
#endif /* GAUSSCHEBYSHEVQUADRATURE1D_H_ */
