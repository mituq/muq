
#ifndef GAUSSHERMITEQUADRATURE1D_H_
#define GAUSSHERMITEQUADRATURE1D_H_

#include <boost/serialization/access.hpp>

#include <Eigen/Core>

#include "MUQ/Utilities/Quadrature/GaussianQuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * Impelements 1D gaussian quadrature for gaussian weighted integrals by extending the
 * abstract class GaussianQuadratureFamily1D with the Hermite polynomials. */
class GaussHermiteQuadrature1D : public GaussianQuadratureFamily1D {
public:

  GaussHermiteQuadrature1D();
  virtual ~GaussHermiteQuadrature1D();

  ///Implements the function that provides the monic  coefficients.
  virtual Eigen::RowVectorXd GetMonicCoeff(unsigned int const j) const;

  ///Implements the function that provides the integral of the weight function over the domain.
  virtual double             IntegralOfWeight() const;

private:

  ///Make class serializable
  friend class boost::serialization::access;

  ///Make class serializable
  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);
};
}
}
#endif /* GAUSSHERMITEQUADRATURE1D_H_ */
