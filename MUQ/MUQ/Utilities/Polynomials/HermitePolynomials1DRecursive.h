#ifndef HERMITEPOLYNOMIALS1DRECURSIVE_H_
#define HERMITEPOLYNOMIALS1DRECURSIVE_H_


#include <boost/serialization/access.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * @class HermitePolynomials1DRecursive
 * @ingroup Polynomials1D
 * @brief Implements the Physicist Hermite Polynomials.
 * @details Subclasses the numerically stable RecursivePolynomialFamily1D with the three-term
 * recurrence necessary to generate the Hermite polynomials. Uses the physicists's
 * version of the Hermite, from http://en.wikipedia.org/wiki/Hermite_polynomials
 **/
class HermitePolynomials1DRecursive : public Static1DPolynomial<HermitePolynomials1DRecursive> {
public:

  friend class Static1DPolynomial<HermitePolynomials1DRecursive>;
  
  
  typedef std::shared_ptr<HermitePolynomials1DRecursive> Ptr;

  HermitePolynomials1DRecursive() = default;
  virtual ~HermitePolynomials1DRecursive() = default;

  ///Implements normalization.
  static double  GetNormalization_static(unsigned int n);

  static Eigen::VectorXd GetMonomialCoeffs(const int order);
  
  static double gradient_static(unsigned int order, double x);
  static double secondDerivative_static(unsigned int order, double x);
  
  
private:

  ///Grant access to serialization
  friend class boost::serialization::access;

  ///Serialization method
  template<class Archive>
  void   serialize(Archive& ar, const unsigned int version);

  //implement methods defining recurrence
  static double alpha(double x, unsigned int k);
  static double beta(double x, unsigned int k);
  static double phi0(double x);
  static double phi1(double x);
};
}
}

BOOST_CLASS_EXPORT_KEY(muq::Utilities::Static1DPolynomial<muq::Utilities::HermitePolynomials1DRecursive>)
BOOST_CLASS_EXPORT_KEY(muq::Utilities::HermitePolynomials1DRecursive)

#endif /* HERMITEPOLYNOMIALS1DRECURSIVE_H_ */
