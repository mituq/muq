
#ifndef _LinOpBase_h
#define _LinOpBase_h

#include <Eigen/Core>

namespace muq {
namespace Utilities {
/** @class LinOpBase
 *  @ingroup Utilities
 *  @brief Generic linear operator base class
 *  @details In many situations, it is convenient to work with general linear operators instead of specific matrices.
 *      This class provides that general functionality.
 */
class LinOpBase {
public:

  LinOpBase(int rowsIn, int colsIn) : ncols(colsIn), nrows(rowsIn) {}

  /** Apply the linear operator to a vector */
  virtual Eigen::VectorXd apply(const Eigen::VectorXd& x) = 0;

  /** The output dimension of the linear operator. */
  int                     rows() const
  {
    return nrows;
  }

  /** The input dimension of the linear operator. */
  int cols() const
  {
    return ncols;
  }

protected:

  int ncols;
  int nrows;
};

// class LinOpBase
} // namespace Utilities
} // namespace muq


#endif // ifndef _LinOpBase_h
