
#ifndef _MINRES_h
#define _MINRES_h

#include <Eigen/Core>

#include "MUQ/Utilities/LinearOperators/Solvers/SolverBase.h"

namespace muq {
namespace Utilities {
class MinRes : public SolverBase {
public:

  MinRes() {}

  MinRes(std::shared_ptr<LinOpBase> Ain) : SolverBase(Ain) {}

  /** Solve the system using the minimum residual method. */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& b) override;

protected:

  double tol = 1e-8;
};

// class MinRes
} // namespace Utilities
} // namespace muq

#endif // ifndef _CG_h
