
#ifndef _Splitters_h
#define _Splitters_h

//std includes
#include <memory>
#include <map>

// eigen includes
#include <Eigen/Core>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other muq includes
#include "MUQ/Utilities/Trees/IndexType.h"
#include "MUQ/Utilities/LanczosEigenSolver.h"


#define  REGISTER_SPLITTER_DEC_TYPE(NAME) \
  static std::shared_ptr < muq::Utilities::DerivedSplitterRegister < TrainingTypes ... >> reg;


namespace muq {
namespace Utilities {
template<typename Container>
double median(Container& v)
{
  // get the size of the array
  int size = std::distance(v.begin(), v.end());

  // get the location of where the median should be and build an iterator to that location
  int  medianInd = floor(size / 2);
  auto target    = v.begin();
  std::advance(target, medianInd);

  // get the value at this median
  std::nth_element(v.begin(), target, v.end());

  // if there is an even number of elements, we have to do a bit more work
  if (size % 2 == 0) {
    double m1 = *target;
    --target;
    std::nth_element(v.begin(), target, v.end());
    return 0.5 * (m1 + (*target));
  } else {
    return *target;
  }
}

template<typename ... TrainingTypes>
struct DerivedSplitterRegister;

template<typename ... TrainingTypes>
class Splitter {
public:

  Splitter(int IntParams, int RealParams) : numIntParams(IntParams), numRealParams(RealParams) {}

  /** Is this vector to the left or to the right? */
  virtual bool                                                                                            Test(
    const Eigen::VectorXd& x) const = 0;

  virtual std::pair<muq::Utilities::TreeIndexContainer, muq::Utilities::TreeIndexContainer> Split(
    const Eigen::MatrixXd                          & pts,
    const muq::Utilities::TreeIndexContainer& inds)
  {
    std::pair<TreeIndexContainer, TreeIndexContainer> output;


    for (auto i = inds.begin(); i != inds.end(); ++i) {
      if (Test(pts.col(*i))) {
        output.first.push_front(*i);
      } else {
        output.second.push_front(*i);
      }
    }

    //std::cout << "--> LeftSize: " << output.first.size() << " vs RightSize: " << output.second.size() << std::endl;
    return output;
  }

  virtual void SetParams(const Eigen::VectorXd& p) = 0;

  const int numIntParams;
  const int numRealParams;


  // //////////////////////////////////////////////
  // REGISTRATION CODE
  // //////////////////////////////////////////////

  typedef std::function < std::shared_ptr <
    Splitter<TrainingTypes ... >>
             (const Eigen::MatrixXd&, const muq::Utilities::TreeIndexContainer&, int,
              boost::property_tree::ptree&,
              TrainingTypes ...)> CreateFuncType;

  typedef std::map<std::string, CreateFuncType> Name2ConstructorMapType;


  /// Globally available way to get the map that tells you what splitter methods are available.
  static std::shared_ptr<Name2ConstructorMapType> GetConstructorMap()
  {
    static std::shared_ptr<Splitter::Name2ConstructorMapType> map;

    // if the map doesn't yet exist, create it
    if (!map) {
      map = std::make_shared<Splitter<TrainingTypes ...>::Name2ConstructorMapType>();
    }
    return map;
  }

  static std::shared_ptr < Splitter < TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                  const muq::Utilities::TreeIndexContainer & inds,
                                                                  int depth,
                                                                  boost::property_tree::ptree & properties,
                                                                  TrainingTypes ... params)
  {
    // extract the boost any object from the register
    CreateFuncType f =
      Splitter<TrainingTypes ...>::GetConstructorMap()->at(properties.get("Trees.SplitterMethod", "KdSplitter"));

    return f(pts, inds, depth, properties, params ...);
  };

  // ///////////////////////////////////////////////
  // ///////////////////////////////////////////////
};

// class Splitter


template<typename ... TrainingTypes>
struct DerivedSplitterRegister {
  DerivedSplitterRegister(std::string s, typename Splitter<TrainingTypes ...>::CreateFuncType func)
  {
    Splitter<TrainingTypes ...>::GetConstructorMap()->insert(std::make_pair(s, func));
  }
};


/** @class KdSplitter
 *  @ingroup Utilities
 *  @brief Decision rule for defining KDTree
 *  @details This class compares Eigen vectors based on if a particular dimension is less than val.
 */
template<typename ... TrainingTypes>
class KdSplitter : public Splitter<TrainingTypes ...> {
public:

  KdSplitter(const Eigen::MatrixXd                          & pts,
             const muq::Utilities::TreeIndexContainer& inds,
             int                                              depth,
             boost::property_tree::ptree                    & properties) : Splitter<TrainingTypes ...>(1, 1)
  {
    // set the dimension based on the depth
    dim = depth % pts.rows();
    int Npts = std::distance(inds.begin(), inds.end());

    // set the value based on the median of the pts
    std::vector<double> dimPts(Npts);
    for (int i = 0; i < Npts; ++i) {
      dimPts[i] = pts(dim, i);
    }

    val = median(dimPts);
  }

  virtual bool Test(const Eigen::VectorXd& x) const override
  {
    return x(dim) < val;
  }

  virtual void SetParams(const Eigen::VectorXd& p) override
  {
    val = p[1];
    dim = static_cast<int>(p[0]);
  }

  // //////////////////////////////////////////////
  // REGISTRATION CODE
  // //////////////////////////////////////////////

  static std::shared_ptr < Splitter < TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                  const muq::Utilities::TreeIndexContainer & inds,
                                                                  int depth,
                                                                  boost::property_tree::ptree & properties,
                                                                  TrainingTypes ... unused)
  {
    return std::dynamic_pointer_cast < Splitter < TrainingTypes ... >>
           (std::make_shared < KdSplitter < TrainingTypes ... >> (pts, inds, depth, properties));
  };

  // ///////////////////////////////////////////////
  // ///////////////////////////////////////////////

protected:

private:

  REGISTER_SPLITTER_DEC_TYPE(KdSplitter) double val;
  int dim;
};

// class KdSplitter


/** @class CovSplitter
 *  @ingroup Utilities
 *  @brief Decision rule for defining KDTree
 *  @details This class compares Eigen vectors based on the largest eigenvector of the covariance of the training
 * points.
 */
template<typename ... TrainingTypes>
class CovSplitter : public Splitter<TrainingTypes ...> {
public:

  CovSplitter(const Eigen::MatrixXd                          & pts,
              const muq::Utilities::TreeIndexContainer& inds,
              int                                              depth,
              boost::property_tree::ptree                    & properties) : Splitter<TrainingTypes ...>(0,
                                                                                                         2 * pts.rows())
  {
    int Npts = std::distance(inds.begin(), inds.end());

    Eigen::MatrixXd locPts(pts.rows(), Npts);
    int j = 0;

    for (auto i = inds.begin(); i != inds.end(); ++i, ++j) {
      locPts.col(j) = pts.col(*i);
    }

    // get the mean
    mu = locPts.rowwise().sum() / Npts;

    // get the covariance
    auto diff           = locPts.colwise() - mu;
    Eigen::MatrixXd cov = diff * diff.transpose() / (Npts - 1);

    // get the largest eigenvector of the covariance
    muq::Utilities::LanczosEigenSolver solver;
    solver.compute(cov);
    dir = solver.eigenvectors().col(0);
  }

  virtual bool Test(const Eigen::VectorXd& x) const override
  {
    return (x - mu).dot(dir) < 0;
  }

  virtual void SetParams(const Eigen::VectorXd& p) override
  {
    assert(p.size() == (dir.size() + mu.size()));
    dir = p.head(dir.size());
    mu  = p.tail(dir.size());
  }

  // //////////////////////////////////////////////
  // REGISTRATION CODE
  // //////////////////////////////////////////////

  static std::shared_ptr < Splitter < TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                  const muq::Utilities::TreeIndexContainer & inds,
                                                                  int depth,
                                                                  boost::property_tree::ptree & properties,
                                                                  TrainingTypes ... unused)
  {
    return std::dynamic_pointer_cast < Splitter < TrainingTypes ... >>
           (std::make_shared < CovSplitter < TrainingTypes ... >> (pts, inds, depth, properties));
  };

  // ///////////////////////////////////////////////
  // ///////////////////////////////////////////////

protected:

private:

  REGISTER_SPLITTER_DEC_TYPE(CovSplitter) Eigen::VectorXd dir;
  Eigen::VectorXd mu;
};

// class KdSplitter
} // namespace Utilities
} //namespace muq


#endif // ifndef _Splitters_h
