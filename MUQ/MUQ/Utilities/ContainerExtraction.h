
#ifndef _ContainerExtraction_h
#define _ContainerExtraction_h

#include <algorithm>
#include <vector>
#include <list>

namespace muq {
namespace Utilities {
/** For an aribtrary container of doubles compute the median.
 *  @param[in] vIN container of doubles.
 */
template<class ContainerType>
inline double median(const ContainerType& vIn)
{
  ContainerType v = vIn;

  int n   = floor(v.size() / 2);
  int rem = v.size() % 2;

  if (rem == 0) {
    std::nth_element(v.begin(), v.begin() + n, v.end());
    return v[n];
  } else {
    std::nth_element(v.begin(), v.begin() + n, v.end());
    double val1 = v[n];

    std::nth_element(v.begin(), v.begin() + n + 1, v.end());
    double val2 = v[n];

    return 0.5 * (val1 + val2);
  }
}

inline double median(const Eigen::VectorXd& vIn)
{
  Eigen::VectorXd v = vIn;

  int n   = floor(v.size() / 2);
  int rem = v.size() % 2;

  if (rem == 0) {
    std::nth_element(v.data(), v.data() + n, v.data() + v.size());
    return v[n];
  } else {
    std::nth_element(v.data(), v.data() + n, v.data() + v.size());
    double val1 = v[n];

    std::nth_element(v.data(), v.data() + n + 1, v.data() + v.size());
    double val2 = v[n];

    return 0.5 * (val1 + val2);
  }
}

/** For an aribtrary container of doubles compute the max.
 *  @param[in] vIN container of doubles.
 */
template<class ContainerType>
inline double max(const ContainerType& vIn)
{
  return *max_element(vIn.begin(), vIn.end());
}

/** For an aribtrary container of doubles compute the min.
 *  @param[in] vIN container of doubles.
 */
template<class ContainerType>
inline double min(const ContainerType& vIn)
{
  return *min_element(vIn.begin(), vIn.end());
}
}
}

#endif // ifndef _ContainerExtraction_h
