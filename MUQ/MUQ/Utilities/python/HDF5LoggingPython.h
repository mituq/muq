#ifndef HDF5LOGGINGPYTHON_H_
#define HDF5LOGGINGPYTHON_H_

#include "MUQ/Utilities/HDF5Logging.h"

namespace muq {
namespace Utilities {
void ExportHDF5Logging();
} // namespace Utilities
} // namespace muq

#endif // ifndef HDF5LOGGINGPYTHON_H_
