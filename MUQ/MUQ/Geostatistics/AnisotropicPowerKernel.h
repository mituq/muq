
#ifndef ANISOTROPICPOWERKERNEL_H
#define ANISOTROPICPOWERKERNEL_H

#include <Eigen/Dense>

#include "MUQ/Geostatistics/CovKernel.h"

namespace muq {
namespace Geostatistics {
class AnisotropicPowerKernel : public muq::Geostatistics::CovKernel {
public:

  AnisotropicPowerKernel(Eigen::VectorXd lengthScalesIn, double PowerIn = 2, double VarIn = 1);
  virtual ~AnisotropicPowerKernel() = default;

  virtual double  Kernel(const Eigen::Ref<const Eigen::VectorXd>& x, const Eigen::Ref<const Eigen::VectorXd>& y) const;

  ///Optimized for this kernel
  Eigen::MatrixXd BuildCov(const Eigen::MatrixXd& x) const;


  /** Set the covariance kernel parameters (Length, power, variance) from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta) override;

  virtual Eigen::VectorXd GetParms() override;


  virtual void            GetGrad(const Eigen::Ref<const Eigen::VectorXd>& x,
                                  const Eigen::Ref<const Eigen::VectorXd>& y,
                                  Eigen::VectorXd                        & grad) const;


  virtual double EstimateLength() const
  {
    return 1.0;
  }

  virtual void                BuildGradMat(const Eigen::MatrixXd& xs, std::vector<Eigen::MatrixXd>& GradMat) const;

  virtual     Eigen::VectorXd KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt) override;

protected:

  Eigen::VectorXd lengthScales;
  double Power;
  double variance;
  int    Nparams;
};
}
}

#endif // ANISOTROPICPOWERKERNEL_H
