#ifndef REGRESSORBASEPYTHON_H_
#define REGRESSORBASEPYTHON_H_

#include "MUQ/Approximation/Regression/RegressorBase.h"

namespace muq {
  namespace Approximation {
    void ExportRegressorBase();
  } // namespace Approximation 
} // namespace muq

#endif
