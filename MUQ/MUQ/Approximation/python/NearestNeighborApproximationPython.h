#ifndef NEARESTNEIGHBORAPPROXIMATIONPYTHON_H_
#define NEARESTNEIGHBORAPPROXIMATIONPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Approximation/Incremental/NearestNeighborApproximation.h"

namespace muq {
namespace Approximation {
class NearestNeighborApproximationPython : public NearestNeighborApproximation,
                                           public boost::python::wrapper<NearestNeighborApproximation> {
public:

  ///Wraps the model with a cached mod piece, then calls the other
  NearestNeighborApproximationPython(std::shared_ptr<muq::Modelling::ModPiece> model,
                                     boost::python::dict const               & dict);

  virtual ~NearestNeighborApproximationPython() = default;

  NearestNeighborApproximation::RefinementStatus PyRefineNear(boost::python::list const& input);

  NearestNeighborApproximation::RefinementStatus PyRefineWithPoint(boost::python::list const& input);

private:
};

void ExportNearestNeighborApproximation();
} // namespace Regression
} // namespace muq

#endif // ifndef NEARESTNEIGHBORAPPROXIMATIONPYTHON_H_
