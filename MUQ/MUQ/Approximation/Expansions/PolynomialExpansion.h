#ifndef POLYNOMIALEXPANSION_H_
#define POLYNOMIALEXPANSION_H_

#include "MUQ/Approximation/Expansions/ExpansionBase.h"

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"
#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace muq{
  
  namespace Approximation{
    
    class PolynomialExpansion;
    
    std::ostream& operator<<(std::ostream& output, const PolynomialExpansion& expansion);
    
    
    /** @class PolynomialExpansion
        @ingroup PolynomialChaos
        @ingroup TransportMaps
        @brief Defines a multivariate polynomial expansion.
        @details The ExpansionBase class provides an abstract interface for finite expansions with arbitrary basis functions.  This class specializes ExpansionBase for multivariate polynomial basis functions. Note that this class does not assume the polynomials are orthogonal, the additional functionality that comes with orthogonality is provided in the PolynomialChaosExpansion class.
     
     This class implements expansions of the form \f[ f_i(x) = \sum_{\mathbf{j}\in\mathcal{J}} c_{i,\mathbf{j}}\Phi_{i,\mathbf{j}}(x), \f] where \f$ f_i(x) \f$ is the \f$i^{\mbox{th}}\f$ output of the expansion, \f$c_{i,\mathbf{j}}\f$ is a scalar coefficient, \f$\mathcal{J}\f$ is a multiindex set (see MultiIndexSet) that does not depend on the dimension \f$i\f$, \f$\mathbf{j}\f$ is a multiindex (see MultiIndex), and \f$\Phi_{i,\mathbf{j}}\f$ is a multivariate polynomial constructed from univariate polynomials through \f[ \Phi_{\mathbf{j}} = \prod_{d=0}^{|\mathbf{j}|} \phi_{d,j_d}(x_d).\f]  Here \f$j_d\f$ is the \f$d^{\mbox{th}}\f$ component of \f$\mathbf{j}\f$, and \f$\phi_{d,j_d}\f$ is a 1D polynomial of degree \f$j_d\f$.  The 1D polynomial can be any child of the muq::Utilities::RecursivePolynomialFamily1D class.  Each input dimension \f$d\f$ can also be related to a different polynomial family; hence, the \f$d\f$ subscript in \f$\phi_{d,j_d}\f$.
     
     */
    class PolynomialExpansion : public ExpansionBase{
    
      friend class boost::serialization::access;
      friend std::ostream& operator<<(std::ostream& output, const PolynomialExpansion& expansion);
      
    public:
    
      /** Construct the expansion with a set of known coefficients, a multiindex set, and the same 1D polynomial family for all output dimensions.  When directly creating a polynomial expansion, this is the constructor that should be used.  Other constructors do not initialize the coefficients and should only be used as part of a factory method for creating the expansion.
          @param[in] coeffs An \f$M\times K\f$ matrix of coefficients, where \f$M\f$ is the output dimension of the expansion and \f$K\f$ is the number of terms in the multiindex set, i.e., the number of basis functions.
          @param[in] multiSet A shared_ptr to the multiindex set defining the basis functions.  The number of terms in the set must be equal to \f$K\f$, the number of columns in coeffs.
          @param[in] poly A shared_ptr to the 1D polynomial family that defines \f$\phi_d\f$ for all \f$d\f$.  When using this constructor, the same polynomial family will be used for all input dimensions of the map.
       */
      PolynomialExpansion(Eigen::MatrixXd const& coeffs,
                          std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                          std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D> poly);
      
      PolynomialExpansion(Eigen::MatrixXd const& coeffs,
                          std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                          std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polys);
      
      /** Construct the expansion without initializing the coefficients but still setting the multiindex set and the same 1D polynomial family.  Because the coefficients are not initialized with this constructor, it should be used carefully and mostly as part of other factory methods or children.
       @param[in] outputDim The output dimension of the expansion.
       @param[in] multiSet A shared_ptr to the multiindex set defining the basis functions.  The number of terms in the set must be equal to \f$K\f$, the number of columns in coeffs.
       @param[in] poly A shared_ptr to the 1D polynomial family that defines \f$\phi_d\f$ for all \f$d\f$.  When using this constructor, the same polynomial family will be used for all input dimensions of the map.
       */
      PolynomialExpansion(int outputDim,
                          std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                          std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D> poly);
      
      /** Construct the expansion without initializing the coefficients but still setting the multiindex set and the same 1D polynomial family.  Notice that in this constructor, a vector of 1D polynomial families is accepted, which allows different polynomial families to be used in different directions.  Because the coefficients are not initialized with this constructor, it should be used carefully and mostly as part of other factory methods or children.
       @param[in] outputDim The output dimension of the expansion.
       @param[in] multiSet A shared_ptr to the multiindex set defining the basis functions.  The number of terms in the set must be equal to \f$K\f$, the number of columns in coeffs.
       @param[in] polys A vector of shared_ptrs to 1D polynomial families.  Each family defines a single 1D polynomial \f$\phi_d\f$.
       */
      PolynomialExpansion(int outputDim,
                          std::shared_ptr<muq::Utilities::MultiIndexSet> multiSet,
                          std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polys);
      
      /** A very basic constructor used only to initialize things during serialization.  End users should never need to call this constructor directly. */
      PolynomialExpansion(int inputDim, int outputDim) : ExpansionBase(inputDim,outputDim,0){};
      
      /** Default virtual destructor. */
      virtual ~PolynomialExpansion() = default;
      
      
      /** Implementation of the pure abstract EvaluateTerms function in ExpansionBase.  This function constructs a \f$K \times N\f$ matrix containing each of the \f$K\f$ basis functions evaluated at each of the \f$N\f$ points in the xs matrix.  This function is used by the EvaluateImpl method of ExpansionBase to evaluate the expansion.
          @param[in] xs A matrix of \f$N\f$ input points.  Each of the \f$N\f$ columns of xs corresponds to a different location where we want to evaluate the basis functions.
          @return A \f$K\times N\f$ matrix containing each basis function evaluated at each point.
       */
      virtual Eigen::MatrixXd EvaluateTerms(Eigen::MatrixXd const& xs) const override;
      
      /** Implementation of the pure abstract EvaluateTermDerivs function in ExpansionBase.  This function constructs a \f$K \times N\f$ matrix containing the derivative of each of the \f$K\f$ basis functions evaluated at each of the \f$N\f$ points in the xs matrix.  The derivative is taken with respect to the dimWrt input dimension.  This function is used by the JacobianImpl method of ExpansionBase to evaluate the expansion.
       @param[in] xs A matrix of \f$N\f$ input points.  Each of the \f$N\f$ columns of xs corresponds to a different location where we want to evaluate the basis function derivative.
       @param[in] dimWrt The input dimension we want to take the derivative with respect to.
       @return A \f$K\times N\f$ matrix containing the derivative of each basis function with respect to \f$x_{\mbox{dimWrt}}\f$.
       */
      virtual Eigen::MatrixXd EvaluateTermDerivs(Eigen::MatrixXd const& xs, int dimWrt) const override;
      
      /** Get a matrix of unsigned integers containing all the multiindices used in this expansion.  This function is equivalent to calling MultiIndexSet::GetAllMultiIndices() on the set defining this expansion.
       */
      Eigen::MatrixXu GetMultiIndices() const{return multis->GetAllMultiIndices();};
      
      /** Return a vector of the 1D polynomial families used to define the basis functions in this expansion. */
      virtual std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> GetPolys() const{return polys;};
      
      /** Return a shared_ptr to the multiindex set. */
      virtual const std::shared_ptr<muq::Utilities::MultiIndexSet> GetMultiSet() const{return multis;};
      
      ///Load an expansion using boost::serialization and return the result
      static std::shared_ptr<PolynomialExpansion> LoadFromFile(std::string fileName);

      ///Save an expansion to a file using boost::serialization
      static void SaveToFile(std::shared_ptr<PolynomialExpansion> expansion, std::string fileName);
  
      /** Check if a polynomial is used in this expansion.
          @param[in] toFind A row vector of unsigned integers representing the multiindex of the polynomial in question.
          @return True if the multiindex is found in the set and False otherwise.
       */
      virtual bool IsPolynomialInExpansion(Eigen::RowVectorXu const& toFind){return multis->IsActive(toFind);};
                                                          
      
    protected:
      
      std::shared_ptr<muq::Utilities::MultiIndexSet> multis;
      std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> polys;
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version);
    };
    
    
  }// namespace Approximation
}// namespace muq

namespace boost { 
namespace serialization {

  template<class Archive>
  inline void save_construct_data(
    Archive & ar, const muq::Approximation::PolynomialExpansion * t, const unsigned int file_version){
    ar << t->inputSizes(0);
    ar << t->outputSize;
  }

  template<class Archive>
  inline void load_construct_data(
    Archive & ar, muq::Approximation::PolynomialExpansion * t, const unsigned int file_version){
    
    int inputSize,outputSize;
    
    ar >> inputSize;
    ar >> outputSize;
    ::new(t)muq::Approximation::PolynomialExpansion(inputSize,outputSize);
  }

} // namespace serialization
} // namespace boost


#endif