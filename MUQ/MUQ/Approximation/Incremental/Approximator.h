#ifndef APPROXIMATOR_H
#define APPROXIMATOR_H

#include "MUQ/config.h"

#include <boost/optional.hpp>

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

# include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include "MUQ/Modelling/CachedModPiece.h"

#include "MUQ/Optimization/Problems/OptProbBase.h"

#include "MUQ/Approximation/Regression/PolynomialRegressor.h"

namespace muq {
namespace Approximation {
/// Common base class for approximators, which build, evaluate, and refine approximations.
class Approximator : public muq::Modelling::OneInputJacobianModPiece {
public:

  /// Construct a polynomial approximator
  /**
   *  The boost::property_tree::ptree must also include the options for the polynomial regressor
   * muq::Approximation::PolynomialRegressor and optimization muq::Optimization::OptAlgBase.
   *  <ol>
   *  <li> The number of nearest neighbors to use <EM>PolynomialApproximator.kN</EM>
   *    <ul>
   *      <li> Defaults to the number of points to interpolate
   *    </ul>
   *  <li> The tolerance for space filling <EM>PolynomialApproximator.SpaceFillTol</EM>
   *    <ul>
   *      <li> Defaults to \f$1 \times 10^{-3}\f$
   *    </ul>
   *  <li> The number of times to try space filling refinement <EM>PolynomialApproximator.SpaceFillTries</EM>
   *    <ul>
   *      <li> Defaults to \f$3\f$
   *    </ul>
   *  <li> An upper bound on the \f$(p+1)^{th}\f$ derivative of the approximated function
   * <EM>PolynomialApproximator.DerivativeBound</EM>
   *    <ul>
   *      <li> \f$p\f$ is the order of the polynomial approximation
   *      <li> Defaults to \f$1\f$
   *    </ul>
   *   <li> The variance of the function to be approximated <EM>PolynomialApproximator.FunctionVariance</EM>
   *     <ul>
   *       <li> Defaults to \f$0.0\f$ (Nonnoisy function)
   *     </ul>
   *  </ol>
   *  @param[in] fnIn The model we wish to approximate (must have only one input)
   *  @param[in] para Options for the approximator
   */
  Approximator(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn, boost::property_tree::ptree para);

  virtual ~Approximator() = default;

  /// Reset the number of nearest neighbors to use
  /**
   *  @param[in] newKN The new number of nearest neighbors to use
   *  \return True the newKN is less than (or equal to) the number of points in the cache, false if not
   */
  virtual bool ResetNumNearestNeighbors(unsigned int const newKN);

  /// Get the number of nearest neighbors
  /**
   *  \return The number of nearest neighbors currently being used by the regression
   */
  virtual unsigned int NumNearestNeighbors() const;

  /// Get the number of available nearest neighbors
  /**
   *  \return The number of nearest neighbors currently available for use by the regression
   */
  virtual unsigned int NumAvailableNearestNeighbors() const;

  /// Add points to cached data set
  /**
   *  @param[in] pts Each column is a point to add to the cache
   *  \return True if all points were actually added successfully, false otherwise. Adding may fail if the result
   *  is inf or NaN.
   */
  virtual bool AddToCache(Eigen::MatrixXd const& pts);

#if MUQ_PYTHON == 1
  /// Add points to cached data set
  /**
   *  @param[in] pts A list of lists; each element is a point to add to the Cache
   */
  void PyAddToCache(boost::python::list const& pts);
#endif // MUQ_PYTHON == 1

  /// Refine near a selected point
  /**
   *  @param[in] point Refinement should happen near this point
   *  \return The point we used for refinement (it has already been added to the cache). Returned optionally
   *  because the refinement may have failed, either because the space filling broke or the model returned
   *  NaN or inf.
   */
  virtual boost::optional<Eigen::VectorXd> RefineNear(Eigen::VectorXd point);

#if MUQ_PYTHON == 1
  /// Refine near a selected point
  /**
   *  @param[in] point Refinement should happen near this point
   *  \return The point we used for refinement (it has already been added to the cache)
   */
  boost::python::list PyRefineNear(boost::python::list const& point);
#endif // MUQ_PYTHON == 1

  /// Get the cached points of the model
  /**
   *  \return The points in the cache of the muq::Approximation::PolynomialApproximator::fn
   */
  virtual Eigen::MatrixXd GetCachePoints() const;

#if MUQ_PYTHON == 1
  /// Get the cached points of the model (python version)
  /**
   *  \return The points in the cache of the muq::Approximation::PolynomialApproximator::fn
   */
  boost::python::list PyGetCachePoints() const;
#endif // if MUQ_PYTHON == 1

  /// Get the model being approximated
  /**
   *  \return The model used to construct the approximation
   */
  std::shared_ptr<muq::Modelling::CachedModPiece> GetModel() const;

  ///Possible modes of operation for the ModPiece, although the default implementation does not use them
  /**
   * The ModPiece can behave in three different ways:
   * - standard -
   * - crossValidation - return one of the cross validation fits
   * - trueDirect - circumvent the approximation and evalute the true value directly, adding
   * it to the cache.
   */
  enum Modes { standard, crossValidation, trueDirect };

  ///Set the mode, but by default only standard works.
  /**
   * Provided in the base class so the interface is consistent, but cannot be used except for the *WithModes variants.
   */
  virtual void SetMode(Modes newMode);

  virtual int  GetNumberOfCrossValidationFits() const = 0;

  ///Select which index is desired, and set to crossValidation mode.
  /**
   * Provided in the base class so the interface is consistent, but cannot be used except for the *WithModes variants.
   */
  virtual void SetCrossValidationIndex(int const i);

private:

  /// Fit the regressor using local points
  /**
   *  @param[in] point The point to center the approximation around
   */
  virtual void ComputeRegression(Eigen::VectorXd const& point) = 0;

  /// Turn the input ModPiece into a cached mod piece
  /**
   *  @param[in] fnIn The input function we wish to approximate, may or may not be a muq::Modelling::CachedModPiece
   *  \return The same input but is a muq::Modelling::CachedModPiece
   */
  static std::shared_ptr<muq::Modelling::CachedModPiece> MakeCached(
    std::shared_ptr<muq::Modelling::ModPiece> const& fnIn);

protected:

  /// Fine the \f$kN\f$ nearest points
  /**
   *  @param[out] inputs Each column is the input where the function has been evaluated
   *  @param[out] outputs Each column is the output to the corresponding input
   *  @param[in] point We want points near this point
   */
  void NearestNeighbors(Eigen::MatrixXd& inputs, Eigen::MatrixXd& outputs, Eigen::VectorXd const& point) const;


  /// The function being approximated
  /**
   *  The function is evaluated to build and refine the approximation but not to evaluate.
   */
  const std::shared_ptr<muq::Modelling::CachedModPiece> fn;

  /// Regression valid?
  /**
   *  False if no points have been added to the cache or if more points have been added since the last time the
   * regression was built
   */
  bool usedCurrentCache;

  /// The current point the regression is centered around
  Eigen::VectorXd center;

  /// The number of nearest neighbors to use in the approximation
  unsigned int kN;

  /// A ptree of options to solve the space filling optimization
  /**
   *  The options correspond to a muq::Optimization::OptAlgBase object.
   */
  boost::property_tree::ptree optOptions;

private:

  /// The space filling tolerance
  const double spaceFillTol;

  /// The number of times to try space filling
  const unsigned int spaceFillTries;
};
} // namespace Approximation
} // namespace muq


#endif // APPROXIMATOR_H
