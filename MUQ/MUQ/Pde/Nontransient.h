#ifndef NONTRANSIENT_H_
#define NONTRANSIENT_H_

#include "MUQ/Pde/PDE.h"

namespace muq {
  namespace Pde {
    class Nontransient : public PDE {
    public:
      Nontransient(Eigen::VectorXi const& inSizes, std::shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para);

    protected:

      /// Override the Dirichlet boundary conditions 
      /**
	 Since this is a nontransient problem call a different DirichletBC function that does not have time.  
	 @param[in] var The variable name 
	 @param[in] pt The point on the Dirichlet boundary 
	 @param[in] time The current time (should be zero since this is a nontransient problem)
       */
      virtual double DirichletBCs(std::string const& var, libMesh::Point const& pt, double const time) const;

      /// Override the Dirichlet boundary conditions 
      /**
	 Can be overwritten by the user but return zero (homogenous) by default.
	 @param[in] var The variable name 
	 @param[in] pt The point on the Dirichlet boundary 
       */
      virtual double DirichletBCs(std::string const&, libMesh::Point const& pt) const;

    private:
      /// Compute the input sizes for the solver
      /**
	 @param[in] outSize The output size 
	 @param[in] paraInSizes The input sizes for the non initial guess or time parameters
	 \return The input sizes
       */
      Eigen::VectorXi InputSizes(unsigned int const outSize, Eigen::VectorXi const& paraInSizes) const;
    };
  } // namespace Pde
} // namespace muq

#endif
