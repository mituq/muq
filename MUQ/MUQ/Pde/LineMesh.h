
#ifndef LINEMESH_H_
#define LINEMESH_H_

#include "MUQ/Pde/GenericMesh.h"

namespace muq {
namespace Pde {
/// Construct a generic line mesh
class LineMesh : public GenericMesh {
public:

  virtual ~LineMesh() = default;

  /**
     Construct a regular mesh for a \f$[B, L]\f$ line mesh. 
     Ptree options:
     -# The right boundary (<EM>mesh.L</EM>)
       - Defaults to zero
     -# The left boundary (<EM>mesh.B</EM>)
       - Defaults to zero
     -# The number of points (<EM>mesh.N</EM>)
       - Defaults to zero, which is invalid
   */
  LineMesh(boost::property_tree::ptree const& params);

private:

};

} // namespace Pde
} // namespace muq

#endif // ifndef LINEMESH_H_
