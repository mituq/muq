#ifndef GENERICEQUATIONSYSTEMSPYTHON_H_
#define GENERICEQUATIONSYSTEMSPYTHON_H_

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Pde/GenericEquationSystems.h"

namespace muq {
namespace Pde {
inline void ExportGenericEquationSystems()
{
  // Tell python about GenericEquationSystems
  boost::python::class_<GenericEquationSystems, std::shared_ptr<GenericEquationSystems>,
                        boost::noncopyable> exportEqnSys("GenericEquationSystems",
                                                         boost::python::init<boost::python::dict const&>());

  // xml file cosntructor
  exportEqnSys.def(boost::python::init<std::string const&>());

  // wrap print function
  exportEqnSys.def("PrintToExodus", &GenericEquationSystems::PrintToExodus);
}
} // namespace Pde
} // namespace muq

#endif // ifndef GENERICEQUATIONSYSTEMSPYTHON_H_
