
#ifndef NONTRANSIENTPDE_H_
#define NONTRANSIENTPDE_H_

#include "MUQ/Pde/PDEBase.h"

namespace muq {
namespace Pde {
/// Compute the residual for a (steady state) partial differential equations
/**
 *  If the partial differential equation has the form
 *  \f{eqnarray}{
 *  D(u; \theta_1) - f(u;\theta_2) = 0
 *  \f}
 *  where \f$D(u, t; \theta_1)\f$ is a, potentially nonlinear, differential operator and \f$f(u, t; \theta_2)\f$ is a,
 *potentially nonlinear, forcing.  Using a finite element discretization the weak form is
 *  \f{eqnarray}{
 *  \mathbf{J}(\mathbf{u}; \theta_1)\mathbf{u} - \mathbf{f}(\mathbf{u};\theta_2) = 0
 *  \f}
 *  where \f$\mathbf{J} \equiv \nabla_{\mathbf{u}} \left(D(u; \theta_1) - f(u; \theta_2)\right)\f$ is the stiffness
 *matrix and \f$\mathbf{f}\f$ is the discretized forcing.  The residual
 *  \f{eqnarray}{
 *  \mathbf{r}(\mathbf{u}; \theta) \equiv \mathbf{J}\mathbf{u} - \mathbf{f}
 *  \f}
 *  is computed by evaluating children of this class.  Derivative information is computed using automatic
 *differentiation.
 */
class NontransientPDE : public PDEBase {
public:

protected:

  /// Protected construct called by children
  /**
   *  @param[in] inputSizes A vector determining the size of each input parameter
   *  @param[in] outputSize The output size
   *  @param[in] eqnsystem The libmesh::EquationSystems that holds the mesh for this problem
   *  @param[in] param A boost::ptree will all the information needed to make the PDEBase
   */
  NontransientPDE(Eigen::VectorXi const& inputSizes, unsigned int const outputSize,
                  std::shared_ptr<GenericEquationSystems> const& eqnsystem,
                  boost::property_tree::ptree const& param);

private:

  /// Evaluate the residual
  /**
   *  Compute the residual
   *  \f{eqnarray}{
   *  \mathbf{r}(\mathbf{u}; \theta) \equiv \mathbf{J}\mathbf{u} - \mathbf{f}.
   *  \f}
   *  The first input is the initial guess for the solver, the rest are inputs for the system
   *  @param[in] inputs A vector of parameter inputs \f$\theta\f$
   *  \return The residual \f$\mathbf{r}\f$
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  /// Compute the Jacobian with respect to the state
  /**
   *  Compute the Jacobian
   *  \f{eqnarray}{
   *  \nabla_{\mathbf{u}} \mathbf{r}(\mathbf{u}; \theta) \equiv \nabla_{\mathbf{u}}\left( \mathbf{J}\mathbf{u} -
   * \mathbf{f}\right)
   *  \f}
   *  @param[in] inputs A vector of parameter inputs \f$\theta\f$
   *  \return The Jacobian with respect to the state
   */
  virtual Eigen::MatrixXd JacobianWRTState(std::vector<Eigen::VectorXd> const& inputs) override;

  /// Compute the Jacobian with respect to an input parameter
  /**
   *  Compute the Jacobian
   *  \f{eqnarray}{
   *  \nabla_{\theta_i} \mathbf{r}(\mathbf{u}; \theta) \equiv \nabla_{\theta_i}\left( \mathbf{J}\mathbf{u} -
   * \mathbf{f}\right)
   *  \f}
   *  @param[in] inputs A vector of parameter inputs \f$\theta\f$
   *  @param[in] inputDimWrt The index of the parameter we are taking the Jacobian with respect to
   *  \return The Jacobian with respect to the state
   */
  virtual Eigen::MatrixXd JacobianWRTParameter(std::vector<Eigen::VectorXd> const& inputs,
                                               unsigned int const                  inputDimWrt) override;

  /// The first input is an initial guess
  /**
   *  Add one input (the same size as outputSize) since we know the first input has to be an initial guess for the
   *solver.
   *  @param[in] inputSizes Size of each input (except initial guess)
   *  @param[in] outputSize The size of the solution vector
   */
  static Eigen::VectorXi UpdateInputSizes(Eigen::VectorXi const& inputSizes, unsigned int const outputSize);

  /// Enforce the Dirichlet boundary condtions on the residual
  /**
   *  The residual at the Dirichlet conditions is the difference between the current solution and the prescribed
   *Dirichlet boundary condtion, which is zero since we have already set the dirichlet conditions.
   *  @param[in] residual The residual before enforcing Dirichlet conditions
   */
  void EnforceDirichletBCs(Eigen::VectorXd& residual) const;

  /// Return the Dirichlet boundary condition at a point
  /**
   *  Override time dependent Dirichlet boundary condtions because in the system is nontransient.  Call a nontransient
   *version of the Dirichlet boundary condtions.
   *  @param[in] varName The name of the unknown
   *  @parem[in] point A Dirichlet point
   *  @param[in] time The current time
   *  \return The Dirichlet bounary condition at the current time
   */
  virtual double DirichletBCs(std::string const& varName, libMesh::Point const& point,
                              double const time) const override;

  /// Return the Dirichlet boundary condition at a point
  /**
   *  Nontransient Dirichlet boundary conditions.  Return zero (homogenous) by default but can be overridden by
   *children.
   *  @param[in] varName The name of the unknown
   *  @parem[in] point A Dirichlet point
   *  \return The Dirichlet bounary condition at the current time
   */
  virtual double DirichletBCs(std::string const& varName, libMesh::Point const& point) const;
};
} // namespace Pde
} // namespace muq

#endif // ifndef NONTRANSIENTPDE_H_
