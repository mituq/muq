#ifndef MixtureProposal_H_
#define MixtureProposal_H_


#include "MUQ/Inference/MCMC/MCMCProposal.h"

namespace muq {
namespace Inference {
/**
 *         \class MixtureProposal
 *         \brief Implements a Mixture of two other Proposals.
 */
class MixtureProposal : public MCMCProposal {
public:

  /** Read in parameters a from parsed xml document.
   *  @param[in] paramList Parameter list containing the mixture proposal definition
   */
  MixtureProposal(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties);

  virtual ~MixtureProposal() {}

  virtual std::shared_ptr<muq::Inference::MCMCState> DrawProposal(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
    int const currIteration = 0) override;

  virtual double ProposalDensity(
    std::shared_ptr<muq::Inference::MCMCState> currentState,
    std::shared_ptr<muq::Inference::MCMCState> proposedState) override;

  void SetWeights(const Eigen::VectorXd& newWeights);

  int  GetNumProps() const
  {
    return proposalWeights.size();
  }

  /** Write important information about the settings in this proposal as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const;

protected:

  // the weight of each proposal
  Eigen::VectorXd proposalWeights;

  // a containiner holding each of the proposals
  std::vector < std::shared_ptr < MCMCProposal >> proposals;

private:
};
} // namespace inference
} // namespace muq


#endif // ifndef MixtureProposal_H_
