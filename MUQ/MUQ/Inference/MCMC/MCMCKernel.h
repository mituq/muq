#ifndef MCMCKERNEL_H
#define MCMCKERNEL_H

#include "MUQ/Utilities/RegisterClassNameHelper.h"
#include <MUQ/Utilities/HDF5Logging.h>
#include "MUQ/Inference/MCMC/MCMCState.h"

namespace muq {
namespace Inference {
class SingleChainMCMC;

///Abstract parent of MCMC Kernels.
/**
 * One of the more MCMC classes.
 * 
 * A kernel is responsible for taking a state of the MCMC chain and evolving it to become the next one. Hence, it does 
 * most of the work of MCMC, but not the book-keeping, which MCMCBase handles for you. 
 * 
 * SetupStartState is called once, intially, then ConstructNextState is called repeatedly. As desired, adaptation may be performed - hence, the
 * MCMCKernel may have state, but need not manage the chain to report to the user. This class typically collaborates closely with MCMCProposals to accomplish 
 * this task. Generally it's designed so that the kernel decides whether the proposal makes a good suggestion, and perhaps decides among several proposals,
 * but doesn't modify them. Child classes are responsible for instantiating and storing their own proposals. AbstractSamplingProblem holds the specification
 * of the problem, which it uses to produce MCMCState objects.
 * 
 * Canonical examples include MHKernel and DRKernel.
 * 
 * Uses string registration so the Create method can read from the parameter list. Registration macro is REGISTER_MCMCKERNEL.
 * See MUQ/Utilities/RegisterClassNameHelper.h for an explanation.
 * 
 * See also, MCMCState, MCMCBase, MCMCProposal, HDF5LogEntry, and AbstractSamplingProblem.
 */
class MCMCKernel {
public:

  friend class SingleChainMCMC;

  MCMCKernel(std::shared_ptr<AbstractSamplingProblem> samplingProblem, boost::property_tree::ptree& properties);

  virtual ~MCMCKernel() {}

  /**
   * Print the status of the Kernel.  This function will be called by the MCMCBase instance and should be overloaded
   * by child classes that have something interesting to say (such as acceptance rate in a Metropolis-Hastings
   * setting).
   **/
  virtual void                       PrintStatus() const {}

  ///Use reflection-like ability to call the constructor of the proposal indicated in the properties
  static std::shared_ptr<MCMCKernel> Create(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                            boost::property_tree::ptree            & properties);

  ///Perform setup using the start state. Most do not use this, but the Stan interfaces do.
  virtual void                                       SetupStartState(Eigen::VectorXd const& xStart) {}

  /**
   * @brief Perform the one step evolution of the chain.
   * 
   * @param[in] currentState A non-null state of the MCMC, against which we're comparing.
   * @param[in] iteration Which iteration this is. Behavior is allowed to change, e.g., DR only continues for so long.
   * @param[out] logEntry An object used for recording interesting statistics or occurrences that happens during this iteration, e.g., the probability of acceptance, or whether the move was accepted. 
   * @return std::shared_ptr< muq::Inference::MCMCState > The new state of the chain.
   * 
   * This method does not need to report whether this move is an "acceptance" or "rejection", even though this is a common distinction.
   * If interesting, the kernel child class should record any relevant metrics internally and/or through the log.
   * 
   * Ensure that the adaptation methods of MCMCProposals are called during this step.
   */
  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) = 0;

  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   * 
   * WriteAttributes is typically called at the end of a run to dump the parameters.
   */
  virtual void WriteAttributes(std::string const& groupName,
                               std::string        prefix = "") const {}

protected:

  ///The actual samplingProblem we're working on.
  /**
   * It is important that the samplingProblem is told which things it needs to compute, e.g., gradients of the posterior/model. This typically happens at construction time. 
   */
  std::shared_ptr<AbstractSamplingProblem> samplingProblem;

public:

  //start of registration boilerplate from MUQ/Utilities/RegisterClassNameHelper.h, should be public ************************************
  typedef boost::function<std::shared_ptr<MCMCKernel>(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                                      boost::property_tree::ptree& properties)> FactorySignature;

  typedef std::map<std::string, FactorySignature> FactoryMapType;


  ///Boilerplate code to have a static copy of a map from
  static std::shared_ptr<FactoryMapType> GetFactoryMap()
  {
    static std::shared_ptr<FactoryMapType> map;

    // if the map doesn't exist yet, create it
    if (!map) {
      map = std::make_shared<FactoryMapType>();
    }

    return map;
  }
};
}
}
  #define REGISTER_MCMCKERNEL(NAME) static auto reg ## NAME =                       \
  muq::Inference::MCMCKernel::GetFactoryMap()->insert(std::make_pair(#              \
                                                                     NAME,          \
                                                                     boost          \
                                                                     ::             \
                                                                     shared_factory \
                                                                     <              \
                                                                       NAME>()));

#endif // MCMCKERNEL_H
