
#ifndef _PreMALA_h
#define _PreMALA_h

// include other MUQ related headers
#include "MUQ/Inference/MCMC/MCMCProposal.h"

#include "MUQ/Modelling/GaussianPair.h"

namespace muq {
namespace Inference {
class PreMALA : public MCMCProposal {
public:

  /** Default constructor. */
  PreMALA(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
          boost::property_tree::ptree                            & properties);

  virtual ~PreMALA() = default;

  virtual std::shared_ptr<muq::Inference::MCMCState> DrawProposal(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
    int const currIteration) override;

  virtual void PostProposalProcessing(
    std::shared_ptr<muq::Inference::MCMCState> const newState,
    MCMCProposal::ProposalStatus const               proposalAccepted,
    int const                                        iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry) override;

  virtual double ProposalDensity(
    std::shared_ptr<muq::Inference::MCMCState> currentState,
    std::shared_ptr<muq::Inference::MCMCState> proposedState) override;


  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const;

private:

  double stepLength;
  double maxDrift;
  double acceptanceTarget;
  double stepLengthScaling;
  int    adaptDelay;
  double stepLengthExp        = 0;
  int    numProposals         = 0;
  int    numProposalsAccepted = 0;

  Eigen::LDLT<Eigen::MatrixXd> preCondSolver;
  Eigen::MatrixXd preCondMat;

  std::shared_ptr<muq::Modelling::GaussianRV>   propRV;
  std::shared_ptr<muq::Modelling::GaussianDensity> propDens;
};
} //namespace muq
} // namespace muq


#endif // ifndef _PreMALA_h
