#ifndef MCMCBASEPYTHON_H_
#define MCMCBASEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/MCMC/MCMCBase.h"

#include "MUQ/Modelling/python/EmpiricalRandVarPython.h"

namespace muq {
namespace Inference {
/// Python wrapper around muq::Inference::MCMCBase
class MCMCBasePython : public MCMCBase, public boost::python::wrapper<MCMCBase> {
public:

  /// Construct from python dictionary
  static std::shared_ptr<MCMCBase> ConstructMCMCFromDict(std::shared_ptr<AbstractSamplingProblem> density,
                                                         boost::python::dict const              & dict);

  /// Construct from string
  static std::shared_ptr<MCMCBase> ConstructMCMCFromString(std::shared_ptr<AbstractSamplingProblem> density,
                                                           std::string                              xmlFile);
  
  virtual void WriteAttributes1(std::string const& groupName) const;
};

/// Expose muq::inference::MCMCBase to python
void ExportMCMCBase();
} // namespace muq
} // namespace inference

#endif // ifndef MCMCBASEPYTHON_H_
