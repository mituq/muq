#ifndef LinearBasis_h_
#define LinearBasis_h_

#include <Eigen/Core>

namespace muq {
  namespace Inference{
    
    class ConstantBasis : public UnivariateBasis{
      
    public:
      ConstantBasis(int dimIn) : UnivariateBasis(dimIn){};
      virtual ~ConstantBasis() = default;
      
      std::shared_ptr<UnivariateBasis> Clone() const override{return std::make_shared<ConstantBasis>(dim);};
      
      virtual double Evaluate(const double x) const{return 1.0;};
      virtual double Derivative(const double x) const{return 0.0;};
      virtual double SecondDerivative(const double x) const{return 0.0;};
      
    }; // class ConstantBasis
    
    class LinearBasis : public UnivariateBasis{
      
    public:
      LinearBasis(int dimIn) : UnivariateBasis(dimIn){};
      virtual ~LinearBasis() = default;
      
      std::shared_ptr<UnivariateBasis> Clone() const override{return std::make_shared<LinearBasis>(dim);};
      
      virtual double Evaluate(const double x) const{return x;};
      virtual double Derivative(const double x) const{return 1.0;};
      virtual double SecondDerivative(const double x) const{return 0.0;};
      
    }; // class LinearBasis
  }
}

#endif