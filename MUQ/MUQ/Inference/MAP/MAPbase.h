
#ifndef _MAPbase_h
#define _MAPbase_h

// std library includes
#include <memory>

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

# include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

// other muq includes
#include "MUQ/Inference/ProblemClasses/InferenceProblemBase.h"
#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
  
namespace Modelling{
  class UniformDensity;
}
  
namespace Inference {
class AbstractSamplingProblem;

/** @class MAPbase
 *   @ingroup Inference
 *   @brief Base class and factory for computing MAP estimates.
 */
class MAPbase {
public:

  /** Create a MAP solver using the problem defined in the density and the optimization method, etc... defined in the
   * properties ptree.
   *  @param[in] InfProb The density we want to maximum
   *  @param[in] properties A property tree defining the optimization method and other parameters needed to find the MAP
   *  @return A shared_ptr to a MAP solver.
   */
  static std::shared_ptr<MAPbase> Create(std::shared_ptr<AbstractSamplingProblem> InfProb,
                                         boost::property_tree::ptree            & properties);

  /** Create a MAP solver using the problem defined in the density and the optimization method, etc... that are defined
   * in the xml file.
   *  @param[in] InfProb The density we want to maximum
   *  @param[in] xmlFile An xml file containing the information to create a ptree holding the optimization parameters,
   *etc...
   *  @return A shared_ptr to a MAP solver.
   */
  static std::shared_ptr<MAPbase> Create(std::shared_ptr<AbstractSamplingProblem> InfProb, std::string xmlFile);

  /** Construct the map solver from the problem defined in the density, and the optimization parameters stored in the
   * ptree.
   *  @param[in] InfProb The density we want to maximum
   *  @param[in] properties A property tree defining the optimization method and other parameters needed to find the MAP
   */
  MAPbase(std::shared_ptr<AbstractSamplingProblem> InfProb, boost::property_tree::ptree& properties);

  /** Compute the maximum of the InferenceProblem starting from the point xc.
   *  @param[in] xc The location to start the optimization.
   *  @return A vector holding the location found by the optimizer.  Note: with a local optimization strategy this is
   *not guaranteed to be the global minimum.
   */
  Eigen::VectorXd Solve(const Eigen::VectorXd& xc);

#if MUQ_PYTHON == 1

  /* NOTE: since the create methods create and instance of MAPbase (not a boost.python wrapper) the python wrapping
   * methods need to be implemented as part of this class, not a child. */

  static std::shared_ptr<MAPbase> PyCreateFromDict(std::shared_ptr<AbstractSamplingProblem> InfProb,
                                                   boost::python::dict const              & dict);

  static std::shared_ptr<MAPbase> PyCreateFromString(std::shared_ptr<AbstractSamplingProblem> InfProb,
                                                     std::string                              xmlFile);

  boost::python::list             PySolve(boost::python::list const& xc);
#endif // if MUQ_PYTHON == 1

  // store a shared pointer to an optimizer
  std::shared_ptr<muq::Optimization::OptAlgBase> OptSolver;
  
  private:
    static void AddUniformConstraints(const std::shared_ptr<muq::Modelling::UniformDensity> uniformCast, std::shared_ptr<muq::Optimization::OptProbBase> optProb);
};

// class MAPbase
} // namespace Inference
} // namespace muq


#endif // ifndef _MAPbase_h
