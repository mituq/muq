
#ifndef IdentityObserver_h_
#define IdentityObserver_h_

// muq includes
#include "MUQ/Modelling/ModPiece.h"


namespace muq {
namespace Modelling {
/** @class IdentityObserver
 * @brief Provides a method for extracting one input from a vector of inputs.
 */
class IdentityObserver : public ModPiece {
public:

  /** Constructor
   * @param[in] inputDims A vector holding the sizes of all the input vectors.
   * @param[in] extractDim The dimension of the inputs that this class extracts.
   */
  IdentityObserver(Eigen::VectorXi const& inputDims, int extractDimIn);

protected:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input,
                                       int const                           inputDimWrt) override;


  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override;


  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitvity,
                                      int const                           inputDimWrt) override;

private:

  const int extractDim;
};

// IdentityObserver
} // namespace Modelling
} // namespace muq


#endif // ifndef IdentityObserver_h_
