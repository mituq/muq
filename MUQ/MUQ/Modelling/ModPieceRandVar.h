
#ifndef _ModPieceRandVar_h
#define _ModPieceRandVar_h

#include "MUQ/Modelling/RandVar.h"

namespace muq {
namespace Modelling {
///Subclass of RandVar for the conversion of a modpiece to a RandVar.
class ModPieceRandVar : public RandVar {
public:

  ModPieceRandVar(const std::shared_ptr<ModPiece>& rvIn) : RandVar(rvIn->inputSizes,
                                                                   rvIn->outputSize,
                                                                   rvIn->hasDirectGradient,
                                                                   rvIn->hasDirectJacobian,
                                                                   rvIn->hasDirectJacobianAction,
                                                                   rvIn->hasDirectHessian),
                                                           rv(rvIn)
  {}

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    assert(rv != nullptr); return rv->Evaluate(input);
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    assert(rv != nullptr);
    return rv->Gradient(input, sensitivity, inputDimWrt);
  }

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override
  {
    assert(rv != nullptr);
    return rv->Jacobian(input, inputDimWrt);
  }

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override
  {
    assert(rv != nullptr);
    return rv->JacobianAction(input, target, inputDimWrt);
  }

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override
  {
    assert(rv != nullptr);
    return rv->Hessian(input, sensitivity, inputDimWrt);
  }

protected:

  std::shared_ptr<ModPiece> rv;
};
}
}
#endif // ifndef _ModPieceRandVar_h
