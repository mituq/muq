
#ifndef _EmpiricalRandVar_h
#define _EmpiricalRandVar_h

#include <vector>
#include <list>

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include "MUQ/Modelling/RandVar.h"

namespace muq {
namespace Modelling {
class EmpiricalRandVar;
class DiscreteOptimalMap;

typedef std::shared_ptr<EmpiricalRandVar> EmpRvPtr;


/** @ingroup Modelling
 *  @class EmpiricalRandVar
 *  @author Matthew Parno
 *  @brief Class for treating set of samples as a random variable
 *  @details This class implements an empirical random variable based and many samples from a continuous random variable
 *with equal weight.  To sample from this random variable, one of the stored samples is selected at random and
 *     returned as the sample.
 *
 *  NOTE: If this class is modified, ensure that SwapLastSample still works, as parallel chain MCMC uses it.
 */
class EmpiricalRandVar : public RandVar {
  friend class DiscreteOptimalMap;
  friend class InferenceMap;

public:

  /** build an empty RandVar with the given dimension*/
  EmpiricalRandVar(int dim);

  /** construct the object from a single sample */
  EmpiricalRandVar(const Eigen::VectorXd& input);

  /** build a RandVar starting with the samples in this vector.  When using this constructor, the user should make sure
   * that all vectors passed as input have the same dimension. */
  EmpiricalRandVar(const Eigen::MatrixXd& samps);

  ~EmpiricalRandVar();

  /** compute and fill in a componentwise quantile.
   *  @param[in] frac The quantile to compute in [0,1].
   *  @return The frac quantile, computed separately for each dimension.
   */
  virtual Eigen::VectorXd                  getQuantile(double frac) const;

  /** compute and fill in the mean of the samples
   *  @return Sample mean
   */
  virtual Eigen::VectorXd                  getMean() const;

  /** compute and fill in the variance of the samples.  This is more efficient than computing the entire covariance.
   *  @return Sample var
   */
  virtual Eigen::VectorXd                  getVar() const;

  /** compute and fill in the covariance of the samples
   *  @return Unbiased sample covariance
   */
  virtual Eigen::MatrixXd                  getCov() const;

  /** Allocate memory for approximately N samples.  Using this function can dramatically increase sampling speed.
   *  @param[in] N the expected number of samples to be taken
   */
  virtual void                             ExpectedSize(int N);

  /** Declare functions to add new samples */
  virtual void                             addSamp(const Eigen::VectorXd& samp);
  virtual void                             addSamp(const Eigen::MatrixXd& samps);

  /** function to access a sample */
  virtual Eigen::VectorXd                  getSamp(int sampID) const;

  /** return the number of samples in the RandVar */
  virtual int                              NumSamps() const;

  /** compute and return the effective sample size of each dimension, assuming the samples vector is order */
  virtual Eigen::VectorXd                  getEss();

  virtual Eigen::MatrixXd                  GetAllSamples() const;

  /** write sample to a raw binary file. See cpp file for details on file format. Read into matlab with
   * modules/MatlabHelpers/ReadSamples
   *  @param[in] file Name of binary file to create
   */
  void                                     WriteRawBinary(const std::string& file) const;

  /** Like WriteRawBinary, this function will write the samples to a binary file. However, this function will write the
   * sample matrix to a dataset in an HDF5 formatted file. If the dataset already exists, this function will overwrite
   * it.  This function is a small wrapper around muq::utilities::HDF5Wrapper, so you need to have opened an HDF5
   * file with muq::utilities::HDF5Wrapper before calling this function.
   *   @param[in] file The name of the hdf5 file we want to write into to.
   *   @param[in] hdf5Path The path within the hdf5 file to the dataset we wish to write.
   */
  void                                     WriteHDF5(const std::string& hdf5Path) const;

  /** Use the samples in a raw binary file to define an empirical random variable.  This function is the read
   * counterpart of the WriteRawBinary member.
   *  @param[in] file Binary file writtten by a call to WriteRawBinary
   */
  static std::shared_ptr<EmpiricalRandVar> CreateFromRawBinary(const std::string& file);


  void                                     WriteRawText(const std::string& file) const;

  ///Exchange the last samples in the RVs, useful for parallel MCMC.
  void                                     SwapLastSample(EmpRvPtr otherSample);

  //#if HAVE_HDF5==1
  //
  //            /** Write the samples to an hdf5 formatted file.  See cpp file for details on file format.
  //             @param[in] file Name of binary file to create, extension should be .h5
  //             @param[in] Dataname The name of the dataset to write to the file
  //             @param[in] WriteType Should we overwrite the file, or append this dataset to existing datasets in the
  // file?  WriteType=='w' will overwrite the file.  WriteType=='a' will append this dataset to the existing data.
  //             */
  //            void WriteHdf5(const std::string &FileName, const std::string &DataName="data", char WriteType='w');
  //
  //#endif

#if MUQ_PYTHON == 1

  /* NOTE:
   *  These functions are here instead of muq::Modelling::EmpiricalRandVarPython because the MCMC sampling method
   *creates this class, not the wrapper class. Thus, if these were part of the wrapper class the result from MCMC
   *sampling would not be "usable" in python.
   */

  /// Python wrapper to get the mean
  boost::python::list PyGetMean() const;

  /// Python wrapper to get the cov
  boost::python::list PyGetCov() const;

  /// Python wrapper to get ESS
  boost::python::list PyGetEss();

  /// Python wrapper to add single sample
  void                PyAddSamp(boost::python::list const& samp);

  /// Python wrapper to add multiple samples
  void                PyAddSamps(boost::python::list const& samps);

  /// Python wrapper to get a sample
  boost::python::list PyGetSamp(int sampID) const;

  /// Python wrapper to get all samples
  boost::python::list PyGetAllSamples();
#endif // if MUQ_PYTHON == 1

private:

  /** keep a vector to hold samples */
  Eigen::MatrixXd samples;
  int Nsamps;



  /** return a sample */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
};

// class EmpiricalRandVar
} //namespace Modelling
} // namespace muq

#endif // ifndef _EmpiricalRandVar_h
