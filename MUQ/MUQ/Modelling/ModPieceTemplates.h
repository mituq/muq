
#ifndef ModPieceTemplates_h_
#define ModPieceTemplates_h_

#if MUQ_PYTHON == 1
# include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include "MUQ/Modelling/ModPiece.h"

#define PYTHON_EVALUATE()                                                                      \
  virtual inline boost::python::list PyEvaluate(boost::python::list const & inputs) override { \
    std::vector<Eigen::VectorXd> eigenInputs(1);                                               \
    eigenInputs[0] = muq::Utilities::GetEigenVector<Eigen::VectorXd>(inputs);           \
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(Evaluate(eigenInputs));     \
  }                                                                                            \

#define PYTHON_GRADIENT()                                                                   \
  inline boost::python::list PyGradientDefaultInputDim(boost::python::list const & pyInput, \
                                                       boost::python::list const & pySens)  \
  {                                                                                         \
    std::vector<Eigen::VectorXd> input(1);                                                  \
    input[0] = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pyInput);             \
    Eigen::VectorXd sens = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pySens);  \
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(Gradient(input, sens));  \
  }                                                                                         \
                                                                                            \
  virtual inline boost::python::list PyGradient(boost::python::list const & pyInput,        \
                                                boost::python::list const & pySens,         \
                                                int const inputDimWrt = 0) override {       \
    return PyGradientDefaultInputDim(pyInput, pySens);                                      \
  }                                                                                         \

#define PYTHON_JACOBIAN()                                                              \
  boost::python::list PyJacobianDefaultInputDim(boost::python::list const & pyInput)   \
  {                                                                                    \
    std::vector<Eigen::VectorXd> eigenInputs(1);                                       \
    eigenInputs[0] = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pyInput);  \
    return muq::Utilities::GetPythonMatrix(Jacobian(eigenInputs, 0));           \
  }                                                                                    \
                                                                                       \
  boost::python::list PyJacobian(boost::python::list const & pyInput, int inputDimWrt) \
  {                                                                                    \
    return PyJacobianDefaultInputDim(pyInput);                                         \
  }                                                                                    \

#define PYTHON_JACOBIAN_ACTION()                                                                                 \
  boost::python::list PyJacobianActionDefaultInputDim(boost::python::list const & pyInput,                       \
                                                      boost::python::list const & pyTarget)                      \
  {                                                                                                              \
    std::vector<Eigen::VectorXd> eigenInputs(1);                                                                 \
    eigenInputs[0] = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pyInput);                            \
    Eigen::VectorXd eigenTarget = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pyTarget);              \
                                                                                                                 \
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(JacobianAction(eigenInputs, eigenTarget, 0)); \
  }                                                                                                              \
  boost::python::list PyJacobianAction(boost::python::list const & pyInput,                                      \
                                       boost::python::list const & pyTarget,                                     \
                                       int inputDimWrt)                                                          \
  {                                                                                                              \
    return PyJacobianActionDefaultInputDim(pyInput, pyTarget);                                                   \
  }                                                                                                              \

#define PYTHON_HESSIAN()                                                                                                \
  boost::python::list PyHessianDefaultInputDim(boost::python::list const & pyInput, boost::python::list const & pySens) \
  {                                                                                                                     \
    std::vector<Eigen::VectorXd> input(1);                                                                              \
    input[0] = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pyInput);                                         \
    Eigen::VectorXd sens = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pySens);                              \
                                                                                                                        \
    return muq::Utilities::GetPythonMatrix(Hessian(input, sens));                                                \
  }                                                                                                                     \
                                                                                                                        \
  boost::python::list PyHessian(boost::python::list const & pyInput,                                                    \
                                boost::python::list const & pySens,                                                     \
                                int const inputDimWrt)                                                                  \
  {                                                                                                                     \
    return PyHessianDefaultInputDim(pyInput, pySens);                                                                   \
  }                                                                                                                     \

namespace muq {
namespace Modelling {
  
  
  /** \defgroup ModPieceTemplates ModPiece Templates
  \ingroup Modelling
  @{
  The muq::Modelling::ModPiece class provides a general interface for defining forward models -- possibly with multiple inputs, and with arbitrary combinations of derivative information (i.e. implementations of GradientImpl, JacobianImpl, etc...).  While the ModPiece class is general, it can be overkill when you have a single-input single-output (SISO) model.  The classes in this group define templates for defining such SISO models.  The inherit from ModPiece and provide an easier interface for defining models.
  */
  
  /** @brief Base class for all one input templates.
      @details This class simply adds a few key pieces to the ModPiece class that are shared with all one input templates (e.g., EvalauteImpl and EvaluateMultiImpl).  
   */
  class OneInputTemplate : public ModPiece{
    
  public:
    OneInputTemplate(int                inputSize,
                     int                outputSize,
                     bool const         hasDirectGradient,
                     bool const         hasDirectJacobian,
                     bool const         hasDirectJacobianAction,
                     bool const         hasDirectHessian,
                     bool const         isRandom,
                     std::string const& name = "");
    
    
    virtual ~OneInputTemplate() = default;
    
    
  private:
    
    virtual Eigen::MatrixXd EvaluateMultiImpl(Eigen::MatrixXd const& input);
    virtual Eigen::MatrixXd EvaluateMultiImpl(std::vector<Eigen::MatrixXd> const& input) override;
    
    
    virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) = 0;
    virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
    
    
#if MUQ_PYTHON == 1
    PYTHON_EVALUATE()
#endif // if MUQ_PYTHON == 1
    
  };
  
  /** \brief One input template for models without any derivative information
  */
class OneInputNoDerivModPiece : public OneInputTemplate {
public:

  OneInputNoDerivModPiece(int const inputSize, int const outputSize);

  virtual ~OneInputNoDerivModPiece() = default;

};

/** \brief One input template to wrap a std::function implemention fo the model.
*/
class WrapVectorFunctionModPiece : public OneInputNoDerivModPiece {
public:

  WrapVectorFunctionModPiece(std::function<Eigen::VectorXd(Eigen::VectorXd const&)> fn,
                             int const inputSize,
                             int const outputSize);

  virtual ~WrapVectorFunctionModPiece() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override;

  std::function<Eigen::VectorXd(Eigen::VectorXd const&)> fn;
};

/** \brief One input template for models that can define the GradientImpl function to compute the gradient.
*/
class OneInputAdjointModPiece : public OneInputTemplate {
public:

  OneInputAdjointModPiece(int const inputSize, int const outputSize);

  virtual ~OneInputAdjointModPiece() = default;

#if MUQ_PYTHON == 1
  PYTHON_EVALUATE() PYTHON_GRADIENT()
#endif // if MUQ_PYTHON == 1

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) = 0;
  
  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) = 0;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;
};

/** \brief One input template for models that can compute the Jacobian of the model, but not the gradient of JacobianAction.
*/
class OneInputJacobianModPiece : public OneInputTemplate {
public:

  OneInputJacobianModPiece(int const inputSize, int const outputSize);

  virtual ~OneInputJacobianModPiece() = default;

#if MUQ_PYTHON == 1
  PYTHON_EVALUATE() PYTHON_JACOBIAN()
#endif // if MUQ_PYTHON == 1

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) = 0;

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) = 0;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;
};

/** \brief One input template for models that can construct the Jacobian matrix and evaluate the gradient.
*/
class OneInputAdjointJacobianModPiece : public OneInputTemplate {
public:

  OneInputAdjointJacobianModPiece(int const inputSize, int const outputSize);

  virtual ~OneInputAdjointJacobianModPiece() = default;

#if MUQ_PYTHON == 1
  PYTHON_EVALUATE() PYTHON_GRADIENT() PYTHON_JACOBIAN() PYTHON_JACOBIAN_ACTION()
#endif // if MUQ_PYTHON == 1

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) = 0;

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) = 0;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) = 0;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;
  
  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target) = 0;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;
  
};

/** \brief One input template for models with full dervative information (i.e. GradientImpl, JacobianImpl, JacobianActionImpl, and HessianImpl are implmented).
*/
class OneInputFullModPiece : public OneInputTemplate {
public:

  OneInputFullModPiece(int const inputSize, int const outputSize);

  virtual ~OneInputFullModPiece() = default;

#if MUQ_PYTHON == 1
  PYTHON_EVALUATE() PYTHON_GRADIENT() PYTHON_JACOBIAN() PYTHON_JACOBIAN_ACTION() PYTHON_HESSIAN()
#endif // if MUQ_PYTHON == 1

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) = 0;

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) = 0;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) = 0;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target) = 0;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) = 0;

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override;
  
};

/**@}*/

} //namespace Modelling
} //namespace muq



#endif //ModPieceTemplates_h_
