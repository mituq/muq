#ifndef ROSENBROCKDENSITY_H_
#define ROSENBROCKDENSITY_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/RosenbrockSpecification.h"

namespace muq {
  namespace Modelling {
    class RosenbrockDensity : public Density {
    public:
      RosenbrockDensity(std::shared_ptr<RosenbrockSpecification> const& specification);
      
      template<typename ... Args>
	RosenbrockDensity(Args ... args) : RosenbrockDensity(std::make_shared<RosenbrockSpecification>(args ...)) {}
      
      /// Default deconstructor
      virtual ~RosenbrockDensity() = default;
      
    private:

      virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override;

      virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int inputDimWrt) override;

      virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                          Eigen::VectorXd const             & sensitvity,
                                          int const                           inputDimWrt) override;
      
      // specification that determines the parameters 
      std::shared_ptr<RosenbrockSpecification> specification;
    };
  } // namespace Modelling 
} // namespmace muq

#endif
