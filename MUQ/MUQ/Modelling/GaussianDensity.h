#ifndef GAUSSIANDENSITY_H_
#define GAUSSIANDENSITY_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/ConcatenateModel.h"
#include "MUQ/Modelling/GaussianSpecification.h"
#include "MUQ/Modelling/Density.h"

namespace muq {
namespace Modelling {
/// A Gaussian density
class GaussianDensity : public Density {
public:

  /// Conreate a Gaussian density based on the specification
  /**
   *  @param[in] specification Everything requried to make a density
   */
  GaussianDensity(std::shared_ptr<GaussianSpecification> const& specification);

#if MUQ_PYTHON == 1
  GaussianDensity(boost::python::list const& dimIn);

  GaussianDensity(boost::python::list const& mu, double const scaledCov);

  GaussianDensity(boost::python::list const& mu, boost::python::list const& diag);

  static std::shared_ptr<GaussianDensity> PyCreate(boost::python::list const& mu, boost::python::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode);

  static std::shared_ptr<GaussianDensity> PyCreateJoint(boost::python::list const& dimIn, boost::python::list const& mu, boost::python::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode);

  static std::shared_ptr<GaussianDensity> PyCreateConditional(unsigned int const dim, boost::python::list const& mu, boost::python::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode);

  boost::python::list PyMean() const;

  boost::python::list PyMeanIns(boost::python::list const& ins) const;
#endif // MUQ_PYTHON == 1

  template<typename ... Args>
  GaussianDensity(Args ... args) : GaussianDensity(std::make_shared<GaussianSpecification>(args ...)) {}

  /// Default deconstructor
  virtual ~GaussianDensity() = default;

  /// Return the mean
  /**
   *  @param[in] ins The inputs for both the mean and the covariance or precision (NOT the state)
   *  \return The mean
   */
  Eigen::VectorXd Mean(std::vector<Eigen::VectorXd> const& ins = std::vector<Eigen::VectorXd>()) const;


  /// The specification that determines the mean and covariance
  std::shared_ptr<GaussianSpecification> const specification;

protected:


private:

  /// Return the input sizes for the density
  static Eigen::VectorXi                   InputSizes(std::shared_ptr<GaussianSpecification> const& specification);

  /// Make a model that concatenates vectors into the state
  static std::shared_ptr<ConcatenateModel> MakeStateModel(std::shared_ptr<GaussianSpecification> const& specification);

  /// Implement the log density for a Gaussian
  virtual double                           LogDensityImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  /// Compute the graident
  virtual Eigen::VectorXd                  GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
                                                        Eigen::VectorXd const             & sensitivity,
                                                        int const                           inputDimWrt) override;

  /// Compute the Jacobian
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override;

  /// Compute the Jacobian action
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& inputs,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  /// Compute the Hessian
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& inputs,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override;

  const std::shared_ptr<ConcatenateModel> stateMod;
};
} // namespace Modelling
} // namespace muq

#endif // ifndef GAUSSIANDENSITY_H_
