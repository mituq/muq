
#ifndef RandVar_h_
#define RandVar_h_

#include "MUQ/Modelling/ModPiece.h"


namespace muq {
namespace Modelling {
///Subclass of ModPiece for random variables. Obviously must be random.
class RandVar : public ModPiece {
public:

  RandVar(Eigen::VectorXi const& inputSizes,
          int const              outputSize,
          bool const             hasDirectGradient,
          bool const             hasDirectJacobian,
          bool const             hasDirectJacobianAction,
          bool const             hasDirectHessian);


  virtual ~RandVar() = default;


  /** This Sample method can prvoide a convenient override for Evaluate when NumSamps==1 or can provide a mechanism for
   *  generating many samples when NumSamps>1.  In some situations, (i.e. Gaussian RV), it can be more efficient to
   *  generate many samples at once, than repeatedly generating a single sample.  In those cases, this function can be
   *  overridden by child classes.
   */
  virtual Eigen::MatrixXd Sample(std::vector<Eigen::VectorXd> const& input, int NumSamps = 1);

  ///A generic way to call Sample(vec1, vec2, ...)
  template<typename ... Args>
  Eigen::MatrixXd Sample(Eigen::VectorXd const& firstInput, Args ... args)
  {
    std::vector<Eigen::VectorXd> startVec;

    startVec.push_back(firstInput);
    return Sample(startVec, args ...);
  }

  template<typename ... Args>
  Eigen::MatrixXd Sample(std::vector<Eigen::VectorXd> collectingVector, Eigen::VectorXd const& nextInput, Args ... args)
  {
    collectingVector.push_back(nextInput);
    return Sample(collectingVector, args ...);
  }

  ///Allow call to evaluate with no inputs, which will fail if the ModPiece requires inputs
  virtual Eigen::MatrixXd Sample(int NumSamps = 1)
  {
    return Sample(std::vector<Eigen::VectorXd>(), NumSamps);
  }
};

// class RandVar
} // namespace Modelling
} // namespace muq

#endif // ifndef RandVar_h_
