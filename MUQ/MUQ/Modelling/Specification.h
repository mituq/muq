#ifndef SPECIFICATION_H_
#define SPECIFICATION_H_

#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModGraphPiece.h"

namespace muq {
namespace Modelling {
/// A generic specification to generate muq::Modelling::RandVar and muq::Modelling::Density
class Specification {
public:

  Specification(Eigen::VectorXi const            dim,
                std::shared_ptr<ModGraph> const& graph,
                std::vector<std::string> const & names);

  Specification(Eigen::VectorXi const            dim,
                std::shared_ptr<ModGraph> const& graph,
                std::vector<std::string> const & names,
                Eigen::VectorXi const          & inputSizes);

  /// Virtual deconstructor
  virtual ~Specification() = default;

  /// The size of each input
  Eigen::VectorXi inputSizes;

  /// Parameter space dimensions, a vector to allow for joint densities
  const Eigen::VectorXi dim;

protected:

  /// The names of the parameters
  const std::vector<std::string> names;

  /// For each parameter, store a muq::Modelling::ModPiece and the indices for the input.
  /**
   *  Each entry of this map is a pair.  The first element of the pair is the muq::Modelling::ModPiece and the second is
   *a vector of indices.  The indices are input indices required for this parameter.
   */
  std::map<std::string, std::pair<std::shared_ptr<ModGraphPiece>, std::vector<unsigned int> > > paraMods;

private:
};
} // namespace Modelling
} // namespace muq

#endif // ifndef SPECIFICATION_H_
