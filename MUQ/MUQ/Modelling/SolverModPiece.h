
#ifndef SolverModPiece_h_
#define SolverModPiece_h_

#include <boost/property_tree/ptree_fwd.hpp>

// muq includes
#include "MUQ/Modelling/ModPiece.h"

#include <string>
#include <ostream>

namespace muq {
namespace Modelling {
/** @struct SolverStatistics
 *  @brief A structure to hold some information about a nonlinear solve.
 *  @details This structure holds post-solve statistics about solver performance and timing.
 */
struct SolverStatistics {
  /// The number of nonlinear iterations taken by KINSOL
  long int nonlinearIterations = 0;

  /// The total solution time, including problem set up and KINSOL call
  double solveTime = 0.0;

  /// The linear solver used within the nonlinear solver
  std::string linSolver = "NotSet";

  /// The number of times the residual function was evaluated
  long int numResEvals = 0;

  /// The number of times a full Jacobian matrix was constructed (i.e. the number of calls to the residual's
  // muq::ModPiece::Jacobian)
  long int numJacEvals = 0;

  /// THe number of times the action of the Jacobian on a vector was computed (muq::ModPiece::JacobianAction)
  long int numJacActs = 0;
};
std::ostream& operator<<(std::ostream& os, const SolverStatistics& stats);


/** @class SolverPiece
 * @brief Provides a mechanism for defining inverse models and steady state ODE models.
 * @details This class solves a nonlinear system, such as
 * \f[
 * f(x,p) = 0
 * \f] for \f$x\f$ wher \f$p\f$ is a parameter vector.  The output of this modpiece is given by an ``observation"
 *function $g(f^{-1}(x,p),p)$ that is evaluated at the solution. This additional observation function is included to
 *match the behavior of muq::Modelling::OdeModPiece.
 */
class SolverModPiece : public ModPiece {
public:

  /** Constructor
   * @param[in] fIn A modpiece defining the residual function.  Must take at least one input: an initial guess for the
   *solution.
   * @param[in] gIn A modpiece defining the observation vector, must have the same number of inputs as fIn.
   * @param[in] parameterSizes Holds the size of each parameter input.  Note that the first size must be the size of the
   *state vector passed to f and g.
   * @param[in] properties contains solver properties such as line search and linear solver
   */
  SolverModPiece(std::shared_ptr<ModPiece>    fIn,
                 std::shared_ptr<ModPiece>    gIn,
                 boost::property_tree::ptree& properties);


  SolverModPiece(std::shared_ptr<ModPiece> fIn, boost::property_tree::ptree& properties);

  SolverStatistics getStats() const
  {
    return stats;
  }

protected:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;


  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override;


  //virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
  //                                    Eigen::VectorXd const             & sensitvity,
  //                                    int const                           inputDimWrt) override;


  // the right hand side and observation modPieces
  std::shared_ptr<ModPiece> f, g;

  // store the integration properties
  // boost::property_tree::ptree& integrateProperties;

  /// There is one observation time (say at t=1).
  /**
   *  Used to mimic OdeModPiece.
   */
  const Eigen::VectorXd obsTimes = Eigen::VectorXd::Ones(1);

private:

  Eigen::VectorXd Solve(std::vector<Eigen::VectorXd> const& inputs);

  std::string linSolver;
  std::string solveMethod;
  int verboseLevel;
  double maxNewtonStep; // maximum length of Newton step
  int maxNewtonIters; // maximum number of Newton iterations

  SolverStatistics stats;
};

// OdeModPiece
} // namespace Modelling
} // namespace muq


#endif // ifndef OdeModPiece_h_
