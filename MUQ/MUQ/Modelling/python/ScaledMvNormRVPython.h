
#ifndef SCALEDMVNORMRVPYTHON_H_
#define SCALEDMVNORMRVPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/ScaledMvNormRV.h"
#include "MUQ/Modelling/python/RandVarPython.h"

namespace muq {
namespace Modelling {
/// Python wrapper around muq::Modelling::ScaledMvNormRV

class ScaledMvNormRVPython : public ScaledMvNormRV, public boost::python::wrapper<ScaledMvNormRV> {
public:

  /// Constructor using a specification
  ScaledMvNormRVPython(std::shared_ptr<ConstantMVNormSpecification> const& specification);

  /// Gaussian with identity covariance and zero mean of dimension dimIn
  ScaledMvNormRVPython(int dimIn);

  /// Gaussian with mean and scaled identity covariance
  ScaledMvNormRVPython(boost::python::list const& meanIn, double scalarCovariance);

  ScaledMvNormRVPython(Eigen::VectorXd const                           & meanIn,
                       Eigen::VectorXd const                           & covDiag,
                       ConstantMVNormSpecification::SpecificationMode    mode =
                         ConstantMVNormSpecification::SpecificationMode::DiagonalCovariance);

  ScaledMvNormRVPython(Eigen::VectorXd const                           & meanIn,
                       Eigen::MatrixXd const                           & cov,
                       ConstantMVNormSpecification::SpecificationMode    mode =
                         ConstantMVNormSpecification::SpecificationMode::CovarianceMatrix);

  static std::shared_ptr<ScaledMvNormRVPython> Create(boost::python::list const& meanIn,
                                                      boost::python::list const& covIn,
                                                      bool                       isDiag);

  static std::shared_ptr<ScaledMvNormRVPython> CreateMode(boost::python::list const                    & meanIn,
                                                          boost::python::list const                    & covIn,
                                                          bool                                           isDiag,
                                                          ConstantMVNormSpecification::SpecificationMode mode);

  boost::python::list PySampleInputs(double const sigma, int numSamps);
  boost::python::list PySampleInputsOneSamp(double const sigma);
};

/// Tell python about muq::modelling::ScaledMvNormRV
void ExportScaledMvNormRV();
} // namespace modelling
} // namespace muq

#endif // ifndef MVNORMRVPYTHON_H_
