
#ifndef _LogExpr_h
#define _LogExpr_h


#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"
#include "MUQ/Modelling/ModGraph.h"

namespace muq {
namespace Modelling {
/**
 *  @class LogModel
 *  @ingroup Modelling
 *  @brief Implemenation of componentwise Log
 *
 */
class LogModel : public ComponentwiseModel {
public:

  LogModel(int dim) : ComponentwiseModel(dim) {}

  virtual ~LogModel() = default;
  
  /** Apply the base symbolic function
   *  @param[in] input  the value to be altered
   *  @return f(x) where f is a simple analytic functions such as exp(x), sin(x), etc...
   */
  virtual double BaseFunc(const double& x) const
  {
    return log(x);
  }

  /** return the derivative of the base symbolic function evaluated at point
   *  @param[in] input  where we want to evaluate the derivative
   *  @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseDeriv(const double& x) const
  {
    return (1/x);
  }
  
   /** return the second derivative of the base symbolic function evaluated at point
   *   @param[in] input  where we want to evaluate the second derivative
   *   @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseSecondDeriv(const double& x) const
  {
    return -(1.0/pow(x,2.0));

   }
};

///Create direct operator-like calls
inline std::shared_ptr<ModGraph> Log(const std::shared_ptr<ModGraph>& x)
{
  //copy the graph
  auto newGraph = x->DeepCopy();
  
  //Find out the size, asserting that the graph only has one output
  auto outputSize = newGraph->outputSize();
  
  //Create the new model
  auto newComponentwiseNode = std::make_shared<LogModel>(outputSize);

  //add it to the graph
  newGraph->AddNode(newComponentwiseNode, newComponentwiseNode->GetName());
  //connect the nodes
  newGraph->AddEdge(x->GetOutputNodeName(), newComponentwiseNode->GetName(), 0);

  return newGraph;
}

inline std::shared_ptr<ModGraph> Log(const std::shared_ptr<ModPiece>& x)
{
  return Log(std::make_shared<ModGraph>(x));
}
}
} // namespace muq


#endif // ifndef _LogExpr_h
