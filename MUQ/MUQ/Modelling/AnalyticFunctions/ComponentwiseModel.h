
#ifndef _ComponentwiseExpr_h
#define _ComponentwiseExpr_h

#include "MUQ/Modelling/ModPieceTemplates.h"
#include <math.h>

namespace muq {
namespace Modelling {
extern double sign(double val);

extern double pow2(double val);


/**
 *  @class ComponentwiseModel
 *
 *  @brief Abstract base class for analytic componentwise transforms of input vector.
 *
 *  Many times we just want to take the exponential of each component of an expression, or maybe the sine of each
 *     component.  This abstract base class provides the structure for doing this.  All that needs to be defined in
 * child
 *     classes for a full implementation is the BaseFunc and BaseDeriv functions.
 *
 *  @author Matthew Parno
 *
 */
class ComponentwiseModel : public OneInputFullModPiece {
public:

  ComponentwiseModel(int dim);

  /** Evaluate this expression by applying the base symbolic function to each component of update. */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override;

  /** Evaluate the gradient of the analytic transformation */
  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override;


  /** build the Jacobian of the analytic transformation. */
  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override;

  /** Evaluate of Jacobian on a vector. */
  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target) override;

  /** Evaluate the Hessian on a vector. */
  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override;

  /** Apply the base symbolic function
   *  @param[in] input  the value to be altered
   *  @return f(input) where f is a simple analytic functions such as exp(x), sin(x), etc...
   */
  virtual double          BaseFunc(const double& input) const = 0;

  /** return the derivative of the base symbolic function evaluated at point
   *  @param[in] input  where we want to evaluate the derivative
   *  @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double          BaseDeriv(const double& input) const = 0;

  /** return the second derivative of the base symbolic function evaluated at point
   *   @param[in] input where we want to evaluate the derivative
   *   @return \f$\frac{d^2 f}{dx^2} the second derivative of the analytic function at input point
   */
  virtual double          BaseSecondDeriv(const double& input) const = 0;
};

// class ComponentwiseExpr
} // namespace Modelling
} // namespace muq

#endif // ifndef _ComponentwiseExpr_h
