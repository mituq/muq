#ifndef GAUSSIANRV_H_
#define GAUSSIANRV_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/GaussianSpecification.h"

namespace muq {
namespace Modelling {
/// Sample a Gaussian random variable
class GaussianRV : public RandVar {
public:

  GaussianRV(std::shared_ptr<GaussianSpecification> const& specification);

#if MUQ_PYTHON == 1
  GaussianRV(boost::python::list const& mu, double const scaledCov);

  GaussianRV(boost::python::list const& mu, boost::python::list const& diag);

  static std::shared_ptr<GaussianRV> PyCreate(boost::python::list const& mu, boost::python::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode);

  static std::shared_ptr<GaussianRV> PyCreateConditional(unsigned int const dim, boost::python::list const& mu, boost::python::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode);
#endif

  template<typename ... Args>
  GaussianRV(Args ... args) : GaussianRV(std::make_shared<GaussianSpecification>(args ...)) {}

  virtual Eigen::MatrixXd Sample(std::vector<Eigen::VectorXd> const& input, int NumSamps = 1) override;
  virtual Eigen::MatrixXd Sample(int NumSamps = 1) override;

#if MUQ_PYTHON == 1
  boost::python::list PySampleOneSamp();

  boost::python::list PySampleOneSampInputs(boost::python::list const& inputs);

  boost::python::list PySample(unsigned int const N);

  boost::python::list PySampleInputs(boost::python::list const& inputs, unsigned int const N);
#endif // MUQ_PYTHON == 1

  virtual Eigen::VectorXd TransformStandardNormalSample(
    Eigen::VectorXd const             & vec,
    std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  /// A specification that knows about the density
  const std::shared_ptr<GaussianSpecification> specification;
};
} // namespace Modelling
} // namespace muq

#endif // ifndef GAUSSIANRV_H_
