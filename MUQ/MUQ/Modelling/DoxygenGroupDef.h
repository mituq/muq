/** @defgroup Modelling
 *  @brief A collection of tools for constructing models -- especially models composed of many small components.
 *
 *   <h2>Module Overview:</h2>
 *
 *   The Modelling module attempts to provide an interface for easily defining, altering, and using forward models,
 *      probability densities, and random variables.  The idea is to define your problem using tools from this module,
 *      then pass the assembled problem to other modules such as Inference, Optimization, or Approximation.
 *
 *   Most of the problems MUQ is concerned with depend on a forward simulation of some sort.  You may be trying to
 *      propagate uncertainties in the input parameters to the simulation output or you may be trying to characterize
 * the
 *      input parameters based on observations of the output.  The Modelling module provides an easy way to combine
 * small
 *      pieces of the simulation (or any general input->output relationship) and run the forward simulation.  All of the small pieces (whether they be densities, models, or random variables) are represented as children of the muq::Modelling::ModPiece.  When multiple muq::Modelling::ModPiece instances need to be combined to form a larger model, the instances are placed on a graph (implemented via the muq::Modelling::ModGraph class).  Thus, setting up a model involves defining an appropriate graph.
 *
 *   In addition to forward models, the graph can contain probability densities and random variables.  The base classes for these objects are muq::Modelling::Density and muq::Modelling::RandVar respectively.  Because of the link between random variables and probability densities, the implementation of these objects is done in pairs.  Random variables and probability densities are both defined by a single instance of muq::Modelling::Specification.
 *
 *   @see Additional information on models can be found in the muq::Modelling::ModPiece class, muq::Modelling::ModGraphPiece
 *      class, and muq::Modelling::ModGraph class.
 *   @see Additional information on probability densities can be found in the muq::Modelling::Density class, muq::Modelling::GaussianDensity
 *      class, and muq::Modelling::UniformDensity class.
 *   @see Additional information on random variables can be found in the muq::Modelling::RandVar class, muq::Modelling::EmpiricalRandVar
 *      class, and muq::Modelling::ComposedRandVar class.
 *
 * <h2>Example: Creating a basic model</h2>
 *
 * In this simple example, we will create a ModPiece that takes a vector \f$x\in\mathbb{R}^2\f$ as an input, and returns a vector \f$f(x) = \left[x(0), x(1)^2, x(0)*x(1)^2\right]\f$
 *
 * First, we need to include ModPiece, ModGraph, and ModGraphPiece headers, as well specify the appropriate namespace.
 * \code{.cpp}
 * #include <memory>                            // needed for shared_ptr
 * #include <MUQ/Modelling/ModPieceTemplates.h> // needed for OneInputNoDerivModPiece
 *
 * using namespace muq:Modelling;
 * using namespace std;
 * \endcode
 * Now we can define the ModPiece implementing \f$f(x)\f$.  In general, ModPiece classes take \f$N\f$ inputs, each of which can contain different sizes.  Here, we only have one input, so we can define our model as a child of muq::Modelling::OneInputNoDerivModPiece instead of inheriting from muq::Modelling::ModPiece directly.  The EvaluateImpl function is where we implement the model \f$f(x)\f$itself.
 * \code{.cpp}
 * class fModel : public OneInputNoDerivModPiece{
 *
 * public:
 *
 *   fModel() : OneInputNoDerivModPiece(2,3){};
 *
 * private:
 *
 *   virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& x) override
 *   {
 *     Eigen::VectorXd output(3);
 *     output(0) = x(0);
 *     output(1) = pow(x(1),2);
 *     output(2) = x(0)*pow(x(1),2);
 *   }
 *
 * };
 * \endcode
 * Now that the fModel class has been defined, we can create an instance of this class and evaluate the model.
 * \code{.cpp}
 * int main()
 * {
 *   shared_ptr<ModPiece> f = make_shared<fModel>();
 *   
 *   Eigen::VectorXd x(2);
 *   x(0) = 2.0;
 *   x(1) = 4.0;
 *   
 *   Eigen::VectorXd y = f->Evaluate(x); // notice that we call Evaluate and not EvaluateImpl
 *
 *   return 0;
 * }
 * \endcode
 *
 * <h2>Example: Combining model components</h2>
 * Assume we have defined the fModel class as above and we have created another class gModel that implements some model \f$g(y):\mathbb{R}^3\rightarrow\mathbb{R}^2\f$.  Out goal in this example is to create a model that implements the composition \f$g(f(x))\f$.  To do this, we will use the muq::Modeling::ModGraph and muq::Modelling::ModGraphPiece classes.  Our workflow will be to:
 * <ol>
 * <li>Create and instance of fModel and an instance of gModel</li>
 * <li>Add both instances to a graph</li>
 * <li>Create an edge from \f$f\f$ to \f$g\f$</li>
 * <li>Create a ModPiece from the graph</li>
 * <li>Evaluate the ModPiece.</li>
 * </ol>
 * The code for this is:
 \code{.cpp}
 * #include <memory>                            // needed for shared_ptr
 * #include <MUQ/Modelling/ModPieceTemplates.h> // needed for OneInputNoDerivModPiece
 * #include <MUQ/Modelling/ModGraph.h>          // needed for ModGraph
 * #include <MUQ/Modelling/ModGraphPiece.h>     // needed to turn Graph into ModPiece
 *
 * using namespace muq:Modelling;
 * using namespace std;
 *
 * // NOTE: The definitions of fModel and gModel would go here, or be included from another file.
 *
 * int main()
 * {
 *   // Step 1 -- Create and instance of fModel and an instance of gModel
 *   shared_ptr<ModPiece> f = make_shared<fModel>();
 *   shared_ptr<ModPiece> g = make_shared<fModel>();
 *
 *   // Step 2 -- Add both instances to a graph
 *   shared_ptr<ModGraph> graph;
 *   graph->AddNode(f,"f_name");
 *   graph->AddNode(g,"g_name");
 *   graph->AddEdge("f_name","g_name");
 *
 *   // Step 3 -- Create a ModPiece from the graph
 *   shared_ptr<ModPiece> gof = ModGraphPiece::Create(graph,"g_name");
 *
 *   // Step 4 -- Evaluate the ModPiece.
 *   Eigen::VectorXd x(2);
 *   x(0) = 2.0;
 *   x(1) = 4.0;
 *   
 *   Eigen::VectorXd z = gof->Evaluate(x);
 *
 *   return 0;
 * }
 * \endcode
 */

