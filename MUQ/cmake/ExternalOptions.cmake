option(MUQ_USE_SACADO "Automatic Differentiation capability in Modelling module using Sacado" OFF)
option(MUQ_USE_LIBMESH "PDE solving capability in PDE module (requires Sacado" OFF)
option(MUQ_USE_GLOG "Enable logging through google-glog" OFF)
option(MUQ_USE_GTEST "Enable unit testing using GTEST" OFF)
option(MUQ_USE_CUDA "Enable GPU computing with CUDA" OFF)

option(MUQ_USE_NLOPT "Include NLOPT in the optimization module" OFF)

option(MUQ_USE_OPENMPI "Use OpenMPI" OFF)
option(MUQ_USE_OPENMP "Use OpenMP (non OS X)" OFF)

option(MUQ_USE_PYTHON "Compile Python library" OFF)

option(MUQ_USE_MKL "Use the Eigen wrapper around the Intel MKL" OFF)

