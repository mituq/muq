#Inference Library
option(Inference_build "Inference libraries" ON)
option(Inference_tests "Inference tests" ON)

# Optimization library
option(Optimization_build "Optimization libraries" ON)
option(Optimization_tests "Optimization tests" ON)

# Geostatistic library
option(Geostats_build "Geostats library" ON)
option(Geostats_tests "Geostats tests" ON)

# General Utilities and Modelling libraries
option(UtilitiesAndModelling_build "Utilities and Modelling libraries" ON)
option(UtilitiesAndModelling_tests "Utilities and Modelling tests" ON)

# Approximation library
option(Approximation_build "Approximation library" ON)
option(Approximation_tests "Approximation tests" ON)

# PDE library
option(PDE_build "PDE libraries" ON)
option(PDE_tests "PDE tests" ON)

