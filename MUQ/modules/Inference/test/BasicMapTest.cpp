
#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/MAP/MAPbase.h"

#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/DensityProduct.h"

using namespace muq::Modelling;
using namespace muq::Inference;
using namespace std;

class Inference_MAP_AllDefTest : public::testing::Test {
public:

  // define the system on a graph
  std::shared_ptr<ModGraph> graph;

  // define the inference problem
  std::shared_ptr<InferenceProblem> prob;

  // optimization parameter tree
  boost::property_tree::ptree params;

  // MAP solver
  std::shared_ptr<MAPbase> TestSolver;


  Inference_MAP_AllDefTest()
  {
	   graph = make_shared<ModGraph>();
    // define the prior mean and covariance
    Eigen::VectorXd PriorMean = Eigen::VectorXd::Zero(2);
    Eigen::MatrixXd PriorCov(2, 2);

    PriorCov << 1, 0.7, 0.7, 2;

    // define the "data"
    Eigen::VectorXd DataMean = 7 * Eigen::VectorXd::Ones(2);
    Eigen::MatrixXd DataCov(2, 2);
    DataCov << 2, 0, 0, 2;

    // the parameter we want to infer (or fit in this case)
    graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");

    // the data model
    graph->AddNode(std::make_shared<GaussianDensity>(DataMean, DataCov), "likelihood");

    // the prior
    graph->AddNode(std::make_shared<GaussianDensity>(PriorMean, PriorCov), "prior");

    // the product of prior and likelihood -- the posterior
    graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");

    // the forward model, an exponential in this case
    graph->AddNode(std::make_shared<ExpModel>(2), "forwardModel");

    // make the inferenceTarget parameter the input to the forward model
    graph->AddEdge("inferenceTarget", "forwardModel", 0);

    // take the output of the forward model as the input to the error model
    graph->AddEdge("forwardModel", "likelihood", 0);

    // use the inferenceTarget parameter as the input to the prior
    graph->AddEdge("inferenceTarget", "prior", 0);

    // set the prior and likelihood as inputs to the posterior
    graph->AddEdge("likelihood", "posterior", 0);
    graph->AddEdge("prior", "posterior", 1);


    // create a MAP solver instance
    params.put("Opt.xtol", 1e-12);
    params.put("Opt.gtol", 1e-12);
    params.put("Opt.ftol", 1e-8);
    params.put("Opt.verbosity", 0);

    prob = make_shared<InferenceProblem>(graph);
  }

  void TearDown()
  {
    // sample the distribution
    Eigen::VectorXd x0   = Eigen::VectorXd::Zero(2);
    Eigen::VectorXd xMAP = TestSolver->Solve(x0);

    EXPECT_NEAR(1.8737144117906237, xMAP[0], 5e-5);
    EXPECT_NEAR(1.9287933176757821, xMAP[1], 5e-5);
  }
};


class Inference_MAP_PriorDefTest : public::testing::Test {
public:

  // define the system on a graph
  std::shared_ptr<ModGraph> graph;

  // define the inference problem
  std::shared_ptr<InferenceProblem> prob;

  // optimization parameter tree
  boost::property_tree::ptree params;

  // MAP solver
  std::shared_ptr<MAPbase> TestSolver;


  Inference_MAP_PriorDefTest()
  {
	  graph = make_shared<ModGraph>();
    // define the prior mean and covariance
    Eigen::VectorXd PriorMean = Eigen::VectorXd::Zero(2);
    Eigen::MatrixXd PriorCov(2, 2);

    PriorCov << 1, 0.7, 0.7, 2;

    // define the "data"
    Eigen::VectorXd DataMean = 7 * Eigen::VectorXd::Ones(2);
    Eigen::MatrixXd DataCov(2, 2);
    DataCov << 2, 0, 0, 2;

    // the parameter we want to infer (or fit in this case)
    graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");

    // the data model
    graph->AddNode(std::make_shared<GaussianDensity>(DataMean, DataCov), "likelihood");

    // the prior
    graph->AddNode(std::make_shared<GaussianDensity>(PriorMean, PriorCov), "prior");

    // the product of prior and likelihood -- the posterior
    graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");

    // the forward model, an exponential in this case
    graph->AddNode(std::make_shared<ExpModel>(2), "testerModel");

    // make the inferenceTarget parameter the input to the forward model
    graph->AddEdge("inferenceTarget", "testerModel", 0);

    // take the output of the forward model as the input to the error model
    graph->AddEdge("testerModel", "likelihood", 0);

    // use the inferenceTarget parameter as the input to the prior
    graph->AddEdge("inferenceTarget", "prior", 0);

    // set the prior and likelihood as inputs to the posterior
    graph->AddEdge("likelihood", "posterior", 0);
    graph->AddEdge("prior", "posterior", 1);


    // create a MAP solver instance
    params.put("Opt.xtol", 1e-12);
    params.put("Opt.gtol", 1e-12);
    params.put("Opt.ftol", 1e-8);
    params.put("Opt.verbosity", 0);

    prob = make_shared<InferenceProblem>(graph);
  }

  void TearDown()
  {
    // sample the distribution
    Eigen::VectorXd x0   = Eigen::VectorXd::Zero(2);
    Eigen::VectorXd xMAP = TestSolver->Solve(x0);

    EXPECT_NEAR(1.8737144117906237, xMAP[0], 5e-5);
    EXPECT_NEAR(1.9287933176757821, xMAP[1], 5e-5);
  }
};


class Inference_MAP_BoxPriorTest : public::testing::Test {
public:

  // define the system on a graph
  std::shared_ptr<ModGraph> graph;

  // define the inference problem
  std::shared_ptr<InferenceProblem> prob;

  // optimization parameter tree
  boost::property_tree::ptree params;

  // MAP solver
  std::shared_ptr<MAPbase> TestSolver;
  
  Inference_MAP_BoxPriorTest()
  {
	  graph = make_shared<ModGraph>();
    
    // define the likelihood mean and covariance
    Eigen::VectorXd LikelyMean = Eigen::VectorXd::Ones(2);
    Eigen::MatrixXd LikelyCov(2, 2);

    LikelyCov << 1, 0.7, 0.7, 2;

    // define the box constraints on the prior
    Eigen::VectorXd lb(2);
    lb << 0.0, 2.0;
    Eigen::VectorXd ub(2);
    ub << 0.8, 4.0;

    // the parameter we want to infer (or fit in this case)
    graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");

    // the data model
    graph->AddNode(std::make_shared<GaussianDensity>(LikelyMean, LikelyCov), "likelihood");

    // the prior
    graph->AddNode(std::make_shared<UniformDensity>(make_shared<UniformBox>(lb,ub)), "prior");

    // the product of prior and likelihood -- the posterior
    graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");

    // take the output of the forward model as the input to the error model
    graph->AddEdge("inferenceTarget", "likelihood", 0);

    // use the inferenceTarget parameter as the input to the prior
    graph->AddEdge("inferenceTarget", "prior", 0);

    // set the prior and likelihood as inputs to the posterior
    graph->AddEdge("likelihood", "posterior", 0);
    graph->AddEdge("prior", "posterior", 1);


    // create a MAP solver instance
    params.put("Opt.xtol", 1e-8);
    params.put("Opt.gtol", 1e-8);
    params.put("Opt.ftol", 0);
    params.get("Opt.ctol", 1e-8);
    params.put("Opt.verbosity", 0);

    prob = make_shared<InferenceProblem>(graph);
  }

  void TearDown()
  {
    TestSolver = MAPbase::Create(prob, params);
    
    // sample the distribution
    Eigen::VectorXd x0(2);
    x0 << 1.0, 2.0;
    Eigen::VectorXd xMAP = TestSolver->Solve(x0);

    EXPECT_NEAR(0.8, xMAP[0], 5e-2);
    EXPECT_NEAR( 2.0, xMAP[1], 5e-2);
  }
};



class Inference_MAP_NonlinearBoxPriorTest : public::testing::Test {
public:

  // define the system on a graph
  std::shared_ptr<ModGraph> graph;

  // define the inference problem
  std::shared_ptr<InferenceProblem> prob;

  // optimization parameter tree
  boost::property_tree::ptree params;

  // MAP solver
  std::shared_ptr<MAPbase> TestSolver;
  
  Inference_MAP_NonlinearBoxPriorTest()
  {
	  graph = make_shared<ModGraph>();
    
    // define the likelihood mean and covariance
    Eigen::VectorXd ErrorMean = Eigen::VectorXd::Ones(2);
    Eigen::MatrixXd ErrorCov(2, 2);

    ErrorCov << 1, 0.7, 0.7, 2;

    // define the box constraints on the prior
    Eigen::VectorXd lb(2);
    lb << 0.0, 2.0;
    Eigen::VectorXd ub(2);
    ub << 0.8, 4.0;

    // the parameter we want to infer (or fit in this case)
    graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");

    // add an exponential model
    graph->AddNode(std::make_shared<ExpModel>(2), "expTheta");

    // the data model
    graph->AddNode(std::make_shared<GaussianDensity>(ErrorMean, ErrorCov), "likelihood");

    // the prior
    graph->AddNode(std::make_shared<UniformDensity>(make_shared<UniformBox>(lb,ub)), "prior");

    // the product of prior and likelihood -- the posterior
    graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");

    // take the output of the forward model as the input to the error model
    graph->AddEdge("inferenceTarget", "expTheta", 0);
    graph->AddEdge("expTheta", "likelihood", 0);

    // use the inferenceTarget parameter as the input to the prior
    graph->AddEdge("inferenceTarget", "prior", 0);

    // set the prior and likelihood as inputs to the posterior
    graph->AddEdge("likelihood", "posterior", 0);
    graph->AddEdge("prior", "posterior", 1);


    // create a MAP solver instance
    params.put("Opt.xtol", 0);
    params.put("Opt.gtol", 1e-5);
    params.put("Opt.ftol", 0);
    params.get("Opt.ctol", 1e-9);
    params.put("Opt.verbosity", 0);

    prob = make_shared<InferenceProblem>(graph);
  }

  void TearDown()
  {
    TestSolver = MAPbase::Create(prob, params);
    
    // sample the distribution
    Eigen::VectorXd x0(2);
    x0 << 1.5, 2.2;
    Eigen::VectorXd xMAP = TestSolver->Solve(x0);

    EXPECT_NEAR(0.70101291616038997, xMAP[0], 2e-2);
    EXPECT_NEAR(1.3621754266237471, xMAP[1], 2e-2);
  }
};



TEST(InferenceMAPTest, BasicTest)
{
  // create a mean vector of dimension 2, filled with ones
  Eigen::VectorXd DensMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd DensCov  = Eigen::MatrixXd::Identity(2, 2);

  DensCov << 1.0, 0.8, 0.8, 0.9;
  Eigen::VectorXd xc = Eigen::VectorXd::Zero(2);

  // create a Density pointer to a gaussian
  auto GaussTest = make_shared<GaussianDensity>(DensMean, DensCov);

  // create a MAP solver instance
  boost::property_tree::ptree params;
  params.put("Opt.Method", "BFGS_Line");
  params.put("Opt.xtol", 0);
  params.put("Opt.gtol", 1e-12);
  params.put("Opt.ftol", 0);
  params.put("Opt.verbosity", 0);

  auto prob       = make_shared<SamplingProblem>(GaussTest);
  auto TestSolver = MAPbase::Create(prob, params);

  // sample the distribution
  Eigen::VectorXd xMAP = TestSolver->Solve(xc);

  EXPECT_NEAR(xMAP[0], 1, 1e-6);
  EXPECT_NEAR(xMAP[1], 1, 1e-6);
}

TEST(InferenceMAPTest, BasicNewtonTest)
{
  // create a mean vector of dimension 2, filled with ones
  Eigen::VectorXd DensMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd DensCov  = Eigen::MatrixXd::Identity(2, 2);

  DensCov << 1.0, 0.8, 0.8, 0.9;
  Eigen::VectorXd xc = Eigen::VectorXd::Zero(2);

  // create a Density pointer to a gaussian
  auto GaussTest = make_shared<GaussianDensity>(DensMean, DensCov);

  // create a MAP solver instance
  boost::property_tree::ptree params;
  params.put("Opt.Method", "Newton");
  params.put("Opt.xtol", 0);
  params.put("Opt.gtol", 1e-12);
  params.put("Opt.ftol", 0);
  params.put("Opt.verbosity", 0);

  auto prob       = make_shared<SamplingProblem>(GaussTest);
  auto TestSolver = MAPbase::Create(prob, params);

  // sample the distribution
  Eigen::VectorXd xMAP = TestSolver->Solve(xc);

  EXPECT_NEAR(xMAP[0], 1, 1e-6);
  EXPECT_NEAR(xMAP[1], 1, 1e-6);
}

TEST(InferenceMAPTest, BasicStructuredBFGSTest)
{
  // create a mean vector of dimension 2, filled with ones
  Eigen::VectorXd DensMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd DensCov  = Eigen::MatrixXd::Identity(2, 2);

  DensCov << 1.0, 0.8, 0.8, 0.9;
  Eigen::VectorXd xc = Eigen::VectorXd::Zero(2);

  // create a Density pointer to a gaussian
  auto GaussTest = make_shared<GaussianDensity>(DensMean, DensCov);

  // create a MAP solver instance
  boost::property_tree::ptree params;
  params.put("Opt.Method", "StructuredBFGS");
  params.put("Opt.xtol", 0);
  params.put("Opt.gtol", 1e-12);
  params.put("Opt.ftol", 0);
  params.put("Opt.verbosity", 0);

  auto prob       = make_shared<SamplingProblem>(GaussTest);
  auto TestSolver = MAPbase::Create(prob, params);

  // sample the distribution
  Eigen::VectorXd xMAP = TestSolver->Solve(xc);

  EXPECT_NEAR(xMAP[0], 1, 1e-6);
  EXPECT_NEAR(xMAP[1], 1, 1e-6);
}

TEST_F(Inference_MAP_AllDefTest, Bfgs)
{
  params.put("Opt.Method", "BFGS_Line");
  TestSolver = MAPbase::Create(prob, params);
}

TEST_F(Inference_MAP_AllDefTest, GaussNewton)
{
  params.put("Opt.Method", "Newton");
  TestSolver = MAPbase::Create(prob, params);
}

TEST_F(Inference_MAP_AllDefTest, StructuredBFGS)
{
  params.put("Opt.Method", "StructuredBFGS");
  TestSolver = MAPbase::Create(prob, params);
}

TEST_F(Inference_MAP_PriorDefTest, Bfgs)
{
  params.put("Opt.Method", "BFGS_Line");
  TestSolver = MAPbase::Create(prob, params);
}

TEST_F(Inference_MAP_PriorDefTest, GaussNewton)
{
  params.put("Opt.Method", "Newton");
  TestSolver = MAPbase::Create(prob, params);
}

TEST_F(Inference_MAP_PriorDefTest, StructuredBFGS)
{
  params.put("Opt.Method", "StructuredBFGS");
  TestSolver = MAPbase::Create(prob, params);
}

///
TEST_F(Inference_MAP_BoxPriorTest, Bfgs)
{
  params.put("Opt.Method", "BFGS_Line");
}

TEST_F(Inference_MAP_BoxPriorTest, GaussNewton)
{
  params.put("Opt.Method", "Newton");
}

TEST_F(Inference_MAP_BoxPriorTest, StructuredBFGS)
{
  params.put("Opt.Method", "StructuredBFGS");
}


///
TEST_F(Inference_MAP_NonlinearBoxPriorTest, Bfgs)
{
  params.put("Opt.Method", "BFGS_Line");
}

TEST_F(Inference_MAP_NonlinearBoxPriorTest, GaussNewton)
{
  params.put("Opt.Method", "Newton");
}

TEST_F(Inference_MAP_NonlinearBoxPriorTest, StructuredBFGS)
{
  params.put("Opt.Method", "StructuredBFGS");
}



TEST(InferenceMAPTest, GaussNewtonHessian)
{
  // define the prior mean and covariance
  Eigen::VectorXd PriorMean = Eigen::VectorXd::Zero(2);
  Eigen::MatrixXd PriorCov(2, 2);

  PriorCov << 1, 0.7, 0.7, 2;

  // define the "data"
  Eigen::VectorXd DataMean = 7 * Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd DataCov(2, 2);
  DataCov << 2, 0, 0, 2;

  // define the system on a graph
  auto graph = make_shared<ModGraph>();

  // the parameter we want to infer (or fit in this case)
  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");

  // the data model
  graph->AddNode(std::make_shared<GaussianDensity>(DataMean, DataCov), "likelihood");

  // the prior
  graph->AddNode(std::make_shared<GaussianDensity>(PriorMean, PriorCov), "prior");

  // the product of prior and likelihood -- the posterior
  graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");

  // the forward model, an exponential in this case
  graph->AddNode(std::make_shared<ExpModel>(2), "forwardModel");

  // make the inferenceTarget parameter the input to the forward model
  graph->AddEdge("inferenceTarget", "forwardModel", 0);

  // take the output of the forward model as the input to the error model
  graph->AddEdge("forwardModel", "likelihood", 0);

  // use the inferenceTarget parameter as the input to the prior
  graph->AddEdge("inferenceTarget", "prior", 0);

  // set the prior and likelihood as inputs to the posterior
  graph->AddEdge("likelihood", "posterior", 0);
  graph->AddEdge("prior", "posterior", 1);


  // create a MAP solver instance
  boost::property_tree::ptree params;

  auto prob       = make_shared<InferenceProblem>(graph);
  auto TestSolver = MAPbase::Create(prob, params);
  Eigen::VectorXd x0(2);
  x0 << 1, 0;
  Eigen::VectorXd x1(2);
  x1 << 0, 1;

  Eigen::VectorXd xc = Eigen::VectorXd::Zero(2);

  Eigen::VectorXd f0 = TestSolver->OptSolver->OptProbPtr->applyInvHess(xc, x0);
  Eigen::VectorXd f1 = TestSolver->OptSolver->OptProbPtr->applyInvHess(xc, x1);

  // now get the truth
  Eigen::MatrixXd jac         = graph->GetNodeModel("forwardModel")->Jacobian(xc);
  Eigen::MatrixXd trueHessInv = PriorCov - PriorCov * jac.transpose() *
                                (DataCov + jac * PriorCov * jac.transpose()).ldlt().solve(jac * PriorCov);
  EXPECT_DOUBLE_EQ(trueHessInv(0, 0), f0(0));
  EXPECT_DOUBLE_EQ(trueHessInv(1, 1), f1(1));
  EXPECT_DOUBLE_EQ(trueHessInv(0, 1), f1(0));
  EXPECT_DOUBLE_EQ(trueHessInv(1, 0), f0(1));
}

