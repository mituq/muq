#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/MapFactory.h"

// #include "MUQ/Geostats/PowerKernel.h"
//#include "MUQ/Inference/MCMC/pCN.h"
 #include "MUQ/Inference/MCMC/SingleChainMCMC.h"
#include "MUQ/Inference/MCMC/TransportMapKernel.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"

#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/DensityProduct.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/GaussianPair.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"


using namespace muq::Modelling;
using namespace muq::Inference;
using namespace muq::Utilities;

// using namespace muq::geostats;
using namespace std;
using namespace Eigen;

class BananaDens : public muq::Modelling::Density {
public:

  BananaDens() : Density(2 * Eigen::VectorXi::Ones(1), true, false, false, false) {}

  const double a = 1.0;
  const double b = 4.0;
  int callCount  = 0.0;

private:

  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    // first, do the transformation to the Gaussian domain
    double x1 = input.at(0) (0) / a;
    double x2 = a * input.at(0) (1) - a * b * (pow(input.at(0) (0), 2.0) + a * a);

    return -0.5 * (x1 * x1 + x2 * x2);
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sens,
                                       int                                 inputDimWrt) override
  {
    Eigen::VectorXd output(2);
    const double    y1 = input.at(0) (0);
    const double    y2 = input.at(0) (1);

    output(0) = -y1 / a + 2 * a * a * y2 * b * y1 - 2 *a *a *b *b *pow(y1, 3.0) - 2 * pow(a, 4.0) * b * b * y1;
    output(1) = -a * a * y2 + a * a * b * (y1 * y1 + a * a);
    return output;
  }
};


class InferenceMCMC_TransportTestBanana : public::testing::Test {
protected:

  virtual void SetUp()
  {
    // create a sampling problem around a banana density
    nannerDens    = make_shared<BananaDens>();
    nannerProblem = make_shared<SamplingProblem>(nannerDens);

    // add the constant and linear terms for each output
    auto multis = MultiIndexFactory::CreateTriTotalOrder(2, 1);
    multis.at(1) += MultiIndexFactory::CreateSingleTerm(2, 0, 2);

    // build the map
    initialMap = MapFactory::CreateIdentity(multis);

    // create an MCMC instance
    params.put("MCMC.Method", "SingleChainMCMC");
    params.put("MCMC.Steps", 110000);
    params.put("MCMC.BurnIn", 10000);
    params.put("MCMC.TransportMap.AdaptStart", 500);
    params.put("MCMC.TransportMap.AdaptStop", 200000);
    params.put("MCMC.TransportMap.AdaptGap", 500);
    params.put("MCMC.TransportMap.AdaptScale", 10000);
    params.put("MCMC.TransportMap.MonitorConvergence", true);

    params.put("Verbose", 0);
    params.put("MCMC.TransportMap.SubMethod.Verbose", 0);
  }

  virtual void TearDown()
  {
    auto TestKernel = make_shared<TransportMapKernel>(nannerProblem, params, initialMap);

    auto TestSolver = make_shared<SingleChainMCMC>(TestKernel, params);

    // sample the distribution
    EmpRvPtr TestPost = TestSolver->Sample(Eigen::VectorXd::Zero(2));

    // compute mean and covariance of samples
    Eigen::VectorXd mean = TestPost->getMean();

    // check to make sure results are close to what we want
    Eigen::VectorXd trueMean(2);

    trueMean << 0.0, nannerDens->b * (1 + nannerDens->a * nannerDens->a);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueMean, mean, 1e-2);
  }

  std::shared_ptr<TransportMap> initialMap;
  std::shared_ptr<SamplingProblem> nannerProblem;
  std::shared_ptr<BananaDens> nannerDens;
  boost::property_tree::ptree params;
};


TEST_F(InferenceMCMC_TransportTestBanana, MH)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Proposal", "MHProposal");
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "MHKernel");
  params.put("MCMC.TransportMap.SubMethod.MCMC.MHProposal.PropSize", 0.5);
}

TEST_F(InferenceMCMC_TransportTestBanana, MALA)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Proposal", "PreMALA");
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "MHKernel");
  params.put("MCMC.TransportMap.SubMethod.MCMC.PreMALA.StepLength", 0.5);
}

TEST_F(InferenceMCMC_TransportTestBanana, IndependentDR)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "DR");
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.ProposalSpecification", "Proposal_0,Proposal_1");
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_0.Proposal", "GaussianIndependence");
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_1.Proposal", "MHProposal");

  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_0.GaussianIndependence.Var", 1.0);
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_1.MHProposal.PropSize", 0.5);
}

TEST_F(InferenceMCMC_TransportTestBanana, GoofyDR)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "DR");
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.ProposalSpecification", "Proposal_0,Proposal_1");
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_0.Proposal", "MHProposal");
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_1.Proposal", "PreMALA");

  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_0.MHProposal.PropSize", 0.5);
  params.put("MCMC.TransportMap.SubMethod.MCMC.DR.Proposal_1.PreMALA.StepLength", 0.5);
}

TEST_F(InferenceMCMC_TransportTestBanana, Mixture)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "MHKernel");
  params.put("MCMC.TransportMap.SubMethod.MCMC.Proposal", "MixtureProposal");

  params.put("MCMC.TransportMap.SubMethod.MCMC.MixtureProposal.Proposal_0.Proposal", "GaussianIndependence");
  params.put("MCMC.TransportMap.SubMethod.MCMC.MixtureProposal.Proposal_1.Proposal", "MHProposal");

  params.put("MCMC.TransportMap.SubMethod.MCMC.MixtureProposal.Proposal_0.GaussianIndependence.Var", 1.0);
  params.put("MCMC.TransportMap.SubMethod.MCMC.MixtureProposal.Proposal_0.Weight", 0.0);

  params.put("MCMC.TransportMap.SubMethod.MCMC.MixtureProposal.Proposal_1.MHProposal.PropSize", 0.5);
  params.put("MCMC.TransportMap.SubMethod.MCMC.MixtureProposal.Proposal_1.Weight", 1.0);

  params.put("MCMC.TransportMap.IndependenceScale", 0.7);
  params.put("MCMC.TransportMap.MaxIndependencProb", 1.0);
}


TEST_F(InferenceMCMC_TransportTestBanana, StanHMC)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "StanHMC");
  params.put("MCMC.TransportMap.SubMethod.MCMC.StanHMC.Leaps", 3);
}

TEST_F(InferenceMCMC_TransportTestBanana, StanNUTS)
{
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "StanNUTS");
}

TEST(InferenceMCMC_TransportTestGauss, Independent)
{
  // create a mean vector of dimension 10, filled with ones
  Eigen::VectorXd trueMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 1, 1, 1, 4;

  auto GaussTest = make_shared<SamplingProblem>(make_shared<GaussianDensity>(trueMean, trueCov));

  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Steps", 110000);
  params.put("MCMC.BurnIn", 10000);
  params.put("MCMC.Kernel", "");

  params.put("MCMC.TransportMap.AdaptStart", 10000);
  params.put("MCMC.TransportMap.AdaptStop", 200000);
  params.put("MCMC.TransportMap.AdaptGap", 50);
  params.put("MCMC.TransportMap.AdaptScale", 10000);

  params.put("MCMC.TransportMap.SubMethod.MCMC.Proposal", "GaussianIndependence");
  params.put("MCMC.TransportMap.SubMethod.MCMC.Kernel", "MHKernel");
  params.put("MCMC.TransportMap.SubMethod.MCMC.GaussianIndependence.Var", 1.0);
  params.put("Verbose", 0);

  auto multis = MultiIndexFactory::CreateTriTotalOrder(2, 1);
  auto initialMap = MapFactory::CreateIdentity(multis);

  auto TestKernel = make_shared<TransportMapKernel>(GaussTest, params, initialMap);
  auto TestSolver = make_shared<SingleChainMCMC>(TestKernel, params);

  // sample the distribution
  EmpRvPtr TestPost = TestSolver->Sample(Eigen::VectorXd::Zero(2));
  TestSolver->WriteAttributes();

  // compute mean and covariance of samples
  Eigen::VectorXd mean = TestPost->getMean();
  Eigen::MatrixXd cov  = TestPost->getCov();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueMean, mean, 1e-2);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueCov,  cov,  1e-1);

}
