
// self include
#include "MUQ/Inference/TransportMaps/PolynomialMap.h"


#include <Eigen/Eigenvalues>

using namespace muq::Inference;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;

PolynomialMap::PolynomialMap(int inputDim,
                            const vector<shared_ptr<MultiIndexSet>>&               multisIn,
                            const vector<Eigen::VectorXd>&                         coeffsIn) : TransportMap(inputDim,coeffsIn),
                                                                                               allMultis(multisIn),
                                                                                               maxOrders(Eigen::VectorXu::Zero(inputDim))
{
  assert(allMultis.size() == outputSize);

  // make sure the coefficients are the right size for the number of bases
  const shared_ptr<MultiIndexPool> testPool = multisIn.at(0)->GetPool();
  for (int i = 0; i < outputSize; ++i) {
    assert(coeffs.at(i).size() == multisIn.at(i)->size());
    assert(multisIn.at(i)->GetMultiIndexLength()<=inputSizes(0));
    assert(testPool==allMultis.at(i)->GetPool());
    assert(i==allMultis.at(i)->GetPoolId());
  }
  
  // check if the map is lower triangular
  isLowerTriangular = true;
  for(int d=0; d<outputSize-1; ++d){
    if(multisIn.at(d)->GetMaxOrders().tail(outputSize-d-1).maxCoeff()!=0){
      isLowerTriangular = false;
      break;
    }
  }
  
  // compute the maximum order over each output dimension
  for(int d=0; d<outputSize; ++d)
    maxOrders = maxOrders.array().max(allMultis.at(d)->GetMaxOrders().array()).matrix();
}
