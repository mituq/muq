
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"

#include "MUQ/Inference/MCMC/SingleChainMCMC.h"
#include "MUQ/Modelling/ModGraphPiece.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Inference;

AbstractSamplingProblem::AbstractSamplingProblem(int const                                       samplingDim,
                                                 std::shared_ptr<muq::Modelling::ModGraph>                        posteriorGraph,
                                                 std::shared_ptr<muq::Inference::AbstractMetric> metricObject) :
  samplingDim(samplingDim),
  posteriorGraph(
    posteriorGraph),
  metricObject(
    metricObject)
{}

void AbstractSamplingProblem::SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph)
{
  SetGraphImpl(graph);
}

void AbstractSamplingProblem::SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph)
{
  this->posteriorGraph = graph;
}

SamplingProblem::SamplingProblem(std::shared_ptr<muq::Modelling::Density>        density,
                                 std::shared_ptr<muq::Inference::AbstractMetric> metricObject) : AbstractSamplingProblem(density->inputSizes(
                                                                                                                           0),
                                                                                                                        std::make_shared<muq::Modelling::ModGraph>(),
                                                                                                                         metricObject)
{
  posteriorGraph->AddNode(density, "posterior");

  //this indirection is done so we always have a ModGraphDensity
  this->density = muq::Modelling::ModGraphDensity::Create(posteriorGraph, "posterior");
  //cannot sample with this problem if there's more than one input
}

SamplingProblem::SamplingProblem(std::shared_ptr<muq::Modelling::ModGraph>                       graph,
                                 std::shared_ptr<muq::Inference::AbstractMetric> metricObject) : AbstractSamplingProblem(
                                                                                                   graph->inputSizes() (
                                                                                                     0),
                                                                                                   graph,
                                                                                                   metricObject)
{
  SetGraphImpl(graph); //don't call the virtual function
}

void SamplingProblem::SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph)
{
  SetGraphImpl(graph);
}

void SamplingProblem::SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph)
{
  AbstractSamplingProblem::SetGraphImpl(graph);

  assert(posteriorGraph->NodeExists("posterior"));
  density = muq::Modelling::ModGraphDensity::Create(posteriorGraph, "posterior");
  //cannot sample with this problem if there's more than one input
  assert(density->inputSizes.size() == 1);
}

TemperedSamplingProblem::TemperedSamplingProblem(std::shared_ptr<AbstractSamplingProblem> inputProblem,
                                                 double const                             temperature) :
  AbstractSamplingProblem(inputProblem->samplingDim,
                          std::make_shared<muq::Modelling::ModGraph>(),
                          nullptr),
  baseProblem(inputProblem),
  temperature(
    temperature)
{}

TemperedSamplingProblem::~TemperedSamplingProblem() {}

SamplingProblem::~SamplingProblem() {}

void AbstractSamplingProblem::SetStateComputations(bool const likelihoodGrad, bool const priorGrad, bool const metric)
{
  useLikelihoodGrad = useLikelihoodGrad || likelihoodGrad;
  usePriorGrad      = usePriorGrad || priorGrad;
  useMetric         = useMetric || metric;
}

void AbstractSamplingProblem::CopyStateComputations(std::shared_ptr<AbstractSamplingProblem> const otherSamplingProblem)
{
  useLikelihoodGrad = otherSamplingProblem->useLikelihoodGrad;
  usePriorGrad      = otherSamplingProblem->usePriorGrad;
  useMetric         = otherSamplingProblem->useMetric;
}

std::shared_ptr<muq::Modelling::ModGraph> AbstractSamplingProblem::GetGraph()
{
  return posteriorGraph;
}

void TemperedSamplingProblem::SetStateComputations(bool const likelihoodGrad, bool const priorGrad, bool const metric)
{
  AbstractSamplingProblem::SetStateComputations(likelihoodGrad, priorGrad, metric); //copy for yourself

  baseProblem->SetStateComputations(likelihoodGrad, priorGrad, metric);             //and the child
}

void TemperedSamplingProblem::CopyStateComputations(std::shared_ptr<AbstractSamplingProblem> const otherSamplingProblem)
{
  AbstractSamplingProblem::CopyStateComputations(otherSamplingProblem); //copy for yourself

  baseProblem->CopyStateComputations(otherSamplingProblem);             //and the child
}

std::shared_ptr<MCMCState> SamplingProblem::ConstructState(Eigen::VectorXd const& state, int const currIteration, shared_ptr<HDF5LogEntry> logEntry)
{
  //cannot sample with this problem if there's more than one input
  assert(density->inputSizes.size() == 1);

  double logLikelihood = density->LogDensity(state);

  boost::optional<Eigen::VectorXd> likelihoodGrad;
  boost::optional<double> logPrior; //only used for inference problems
  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> priorGrad;


  //if it's not valid, return null
  if (!std::isfinite(logLikelihood)) {
    return nullptr;
  }

  //compute gradient as desired
  if (useLikelihoodGrad) {
    likelihoodGrad = boost::optional<Eigen::VectorXd>(density->Gradient(state, Eigen::VectorXd::Constant(1, 1.0), 0));
  } else {
    likelihoodGrad = boost::optional<Eigen::VectorXd>();
  }

  //compute metric as desired
  if (useMetric) {
    metric = boost::optional<Eigen::MatrixXd>(metricObject->ComputeMetric(state));
  } else {
    metric = boost::optional<Eigen::MatrixXd>();
  }


  auto mcmcState = std::make_shared<MCMCState>(state,
                                               logLikelihood,
                                               likelihoodGrad,
                                               logPrior,
                                               priorGrad,
                                               metric,
                                               boost::optional<Eigen::VectorXd>()); //note there's no forward
                                                                                    // model
  return mcmcState;
}

void SamplingProblem::InvalidateCaches()
{
  density->InvalidateCache();
}

std::shared_ptr<MCMCState> TemperedSamplingProblem::ConstructState(Eigen::VectorXd const& state, int const currIteration, shared_ptr<HDF5LogEntry> logEntry)
{
  auto temperedState = baseProblem->ConstructState(state, currIteration, logEntry);

  //set temperature if it's valid
  if (temperedState) {
    temperedState->SetTemperature(temperature);
  }
  return temperedState;
}

void TemperedSamplingProblem::InvalidateCaches()
{
  baseProblem->InvalidateCaches();
}

void TemperedSamplingProblem::SetGraph(std::shared_ptr<muq::Modelling::ModGraph> graph)
{
  SetGraphImpl(graph);
}

void TemperedSamplingProblem::SetGraphImpl(std::shared_ptr<muq::Modelling::ModGraph> graph)
{
  AbstractSamplingProblem::SetGraphImpl(graph);
  baseProblem->SetGraph(graph);
}

