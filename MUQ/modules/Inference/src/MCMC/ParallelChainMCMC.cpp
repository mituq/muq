
#include "MUQ/Inference/MCMC/ParallelChainMCMC.h"

#include <assert.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Inference/MCMC/SingleChainMCMC.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"


using namespace std;
using namespace Eigen;
using namespace muq::Inference;
using namespace muq::Modelling;

REGISTER_MCMC_DEF_TYPE(ParallelChainMCMC) ParallelChainMCMC::ParallelChainMCMC(std::shared_ptr<muq::Inference::
                                                                                               AbstractSamplingProblem> inferenceProblem,
                                                                               boost::property_tree::ptree            & properties)
   : MCMCBase(inferenceProblem,
              properties)
{
  LOG(INFO) << "Constructing Parallel Chain MCMC";
  if (verbose > 0) {
    cout << "Constructing Parallel Chain MCMC" << endl;
  }

  int numChains = properties.get("MCMC.ParallelChain.NumChains", 5);

  LOG(INFO) << "MCMC.ParallelChain.NumChains" << numChains;
  assert(numChains > 0);

  childrenMCMC.resize(numChains);


  //grab the type for the children, move it to the top level so it gets used.
  properties.put("MCMC.Method", properties.get("MCMC.ParallelChain.ChildMethod", "MH"));
    LOG(INFO) << "Parallel subchains of type: MCMC.ParallelChain.ChildMethod=" << properties.get(
    "MCMC.ParallelChain.ChildSampler",
    "MH");

  boost::optional<string> hdf5Basename = properties.get_optional<string>("MCMC.ChainHDF5Output");
  if (hdf5Basename) {
    LOG(INFO) << "Writing parallel children to HDF5 in variations of " << hdf5Basename;
  }

  //prepare the child chains
  for (int i = 0; i < numChains; ++i) {
    if (hdf5Basename) {
      properties.put("MCMC.ChainHDF5Output", *hdf5Basename + boost::lexical_cast<string>(i));
    }

    auto childChainTemp = MCMCBase::ConstructMCMC(inferenceProblem, properties);

    childrenMCMC.at(i) = dynamic_pointer_cast<SingleChainMCMC>(childChainTemp);
    assert(childrenMCMC.at(i)); //the child MCMC must be a single chain
  }
}

ParallelChainMCMC::ParallelChainMCMC(std::vector < std::shared_ptr < SingleChainMCMC >>       childrenMCMC,
                                     std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
                                     boost::property_tree::ptree                            & properties) : MCMCBase(
                                                                                                              inferenceProblem,
                                                                                                              properties),
                                                                                                            childrenMCMC(
                                                                                                              childrenMCMC)
{}

ParallelChainMCMC::~ParallelChainMCMC() {}

muq::Modelling::EmpRvPtr ParallelChainMCMC::GetResults()
{
  MatrixXd allResults;

  //gather results from all the child chains
  for (auto chain : childrenMCMC) {
    MatrixXd oneChainResults = chain->GetResults()->GetAllSamples();
    allResults.conservativeResize(oneChainResults.rows(), allResults.cols() + oneChainResults.cols());

    allResults.rightCols(oneChainResults.cols()) = oneChainResults;
  }

  return std::make_shared<EmpiricalRandVar>(allResults);
}

void ParallelChainMCMC::SetupStartState(Eigen::VectorXd const& xStart)
{
  for (auto chain : childrenMCMC) {
    chain->SetupStartState(xStart);
  }
}

void ParallelChainMCMC::SampleOnce()
{
  for (auto chain : childrenMCMC) {
    chain->SampleOnce();
  }

  ProposeSwaps();

  ++currIteration;
}

void ParallelChainMCMC::ProposeSwaps()
{
  //by default, no swaps are performed
}

void ParallelChainMCMC::SwapChains(unsigned i, unsigned j)
{
  swap(childrenMCMC.at(i)->currentState, childrenMCMC.at(j)->currentState);

  //if the sample is kept, swap the entry in the chain
  if (IsCurrentIterationSaved()) {
    childrenMCMC.at(i)->posteriorSamples->SwapLastSample(childrenMCMC.at(j)->posteriorSamples);
  }
}

