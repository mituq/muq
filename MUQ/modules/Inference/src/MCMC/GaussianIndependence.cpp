
#include "MUQ/Inference/MCMC/GaussianIndependence.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Modelling/RandVarBase.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;


REGISTER_MCMCPROPOSAL(GaussianIndependence);

GaussianIndependence::GaussianIndependence(std::shared_ptr<AbstractSamplingProblem> inferenceProblem,
                                           boost::property_tree::ptree            & properties) : MCMCProposal(
                                                                                                    inferenceProblem,
                                                                                                    properties)
{
  LOG(INFO) << "Constructing Gaussian Independence Proposal";

  propVar = ReadAndLogParameter(properties, "MCMC.GaussianIndendepence.Var", 1.0);

  int dim = inferenceProblem->samplingDim;
  prop = make_shared<GaussianPair>(Eigen::VectorXd::Zero(dim), propVar);

  samplingProblem->SetStateComputations(false, false, false);
}

/** Function performs one MCMC step using the fixed proposal and simple accept/reject stage through the
 *  Metropolis-Hastings Rule */
std::shared_ptr<muq::Inference::MCMCState> GaussianIndependence::DrawProposal(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
  int const currIteration)
{
  return samplingProblem->ConstructState(prop->rv->Sample(), currIteration, logEntry);
}

double GaussianIndependence::ProposalDensity(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                             std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
  assert(proposedState);
  assert(currentState);
  return prop->density->LogDensity(proposedState->state);
}

void GaussianIndependence::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "Gaussian Independence");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "GI - Variance", propVar);
}
