#include <boost/property_tree/xml_parser.hpp> // this needs to be here to avoid a weird toupper bug on osx with python

#include "MUQ/Inference/MAP/MAPbase.h"

// std library includes
#include <iostream>
#include <fstream>
#include <memory>


// other muq includes
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Utilities/LinearOperators/Operators/SymmetricOpBase.h"

#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/UniformDensity.h"

using namespace muq::Optimization;
using namespace muq::Inference;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;

class GaussNewtonOperator : public SymmetricOpBase {
public:

  GaussNewtonOperator(const Eigen::VectorXd *xcIn, shared_ptr<ModPiece> forwardMod,
                      shared_ptr<GaussianSpecification> errorSpecIn,
                      shared_ptr<GaussianSpecification> priorSpecIn) : SymmetricOpBase(forwardMod->outputSize),
                                                                             xc(xcIn), mod(
                                                                               forwardMod), errorSpec(errorSpecIn),
                                                                             priorSpec(priorSpecIn)
  {}

  virtual Eigen::VectorXd apply(const Eigen::VectorXd& x) override
  {
    return errorSpec->ApplyCovariance(x) + mod->JacobianAction(*xc, priorSpec->ApplyCovariance(mod->Gradient(*xc, x)));
  }

private:

  const Eigen::VectorXd *xc;
  shared_ptr<ModPiece>   mod;
  shared_ptr<GaussianSpecification> errorSpec;
  shared_ptr<GaussianSpecification> priorSpec;
};

// use this when the prior and error model are Gaussian
class Gauss2GaussOpt : public OptProbBase {
public:

  Gauss2GaussOpt(shared_ptr<InferenceProblem> InfProbIn) : OptProbBase(InfProbIn->samplingDim), InfProb(InfProbIn),
                                                           priorDens(std::dynamic_pointer_cast<GaussianDensity>(InfProbIn->
                                                                                                           prior)),
                                                           errorDens(std::dynamic_pointer_cast<GaussianDensity>(InfProbIn->
                                                                                                           errorMod)),
                                                           forwardMod(InfProbIn->forwardMod)
  {
    // make sure the forward model was actually defined in the inference problem
    assert(InfProb->forwardMod);
  }

  virtual double eval(const Eigen::VectorXd& xc) override
  {
    return -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);
  }

  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    double fout = -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);

    gradient = -1.0 * InfProb->prior->Jacobian(xc).row(0).transpose() -
               InfProb->likelihood->Jacobian(xc).row(0).transpose();
    return fout;
  }

  // use a Gauss-Newton like approximation to the hessian and conjugate gradient solver for H^{-1}
  virtual Eigen::VectorXd applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override
  {
    Eigen::VectorXd priorCovApp = priorDens->specification->ApplyCovariance(vecIn);

    // temp1 will hold J*priorCov
    Eigen::VectorXd temp1 = forwardMod->JacobianAction(xc, priorCovApp);

    auto GaussNewtonHess = make_shared<GaussNewtonOperator>(&xc,
                                                            forwardMod,
                                                            errorDens->specification,
                                                            priorDens->specification);

    // compute (errorCov + J*priorCov*J')^{-1}*temp1
    Eigen::VectorXd sol = CG(GaussNewtonHess).solve(temp1);

    // sol now contains (errorCov + J*priorCov*J')^{-1}*temp1, now we need to finish this up to compute priorCov -
    // priorCov*J'*sol
    return priorCovApp - priorDens->specification->ApplyCovariance(forwardMod->Gradient(xc, sol));
  }

  // use a Gauss-Newton like approximation to the hessian
  virtual Eigen::VectorXd applyHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override
  {
    // we want to apply priorCov^{-1} + J^T*DataCov^{-1}*J
    Eigen::VectorXd part1 = priorDens->specification->ApplyInverseCovariance(vecIn);                                     //
                                                                                                                         //
                                                                                                                         //
                                                                                                                         //
                                                                                                                         // priorCov^{-1}*vecIn
    Eigen::VectorXd part2 =
      forwardMod->Gradient(xc, errorDens->specification->ApplyInverseCovariance(forwardMod->JacobianAction(xc, vecIn))); //

    // J^T*DataCov^{-1}*J*vecIn

    return 1.0 * (part1 + part2);
  }

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc) override
  {
    Eigen::MatrixXd J = forwardMod->Jacobian(xc);
    auto temp         = priorDens->specification->GetPrecisionMatrix();

    return priorDens->specification->GetPrecisionMatrix() + J.transpose() *
           errorDens->specification->GetPrecisionMatrix() * J;
  }

  virtual Eigen::MatrixXd getInvHess(const Eigen::VectorXd& xc) override
  {
    Eigen::MatrixXd J        = forwardMod->Jacobian(xc);
    Eigen::MatrixXd priorCov = priorDens->specification->GetCovarianceMatrix();
    Eigen::MatrixXd errorCov = errorDens->specification->GetCovarianceMatrix();

    return priorCov - priorCov.selfadjointView<Eigen::Lower>() * J.transpose() *
           (J * priorCov.selfadjointView<Eigen::Lower>() * J.transpose() +
            errorDens->specification->GetCovarianceMatrix()).selfadjointView<Eigen::Lower>().ldlt().solve(
             J * priorCov.selfadjointView<Eigen::Lower>());
  }

private:

  shared_ptr<InferenceProblem> InfProb;
  shared_ptr<GaussianDensity> priorDens;
  shared_ptr<GaussianDensity> errorDens;
  shared_ptr<ModPiece>   forwardMod;
};

class GaussPriorOpt : public OptProbBase {
public:

  GaussPriorOpt(shared_ptr<InferenceProblem> InfProbIn) : OptProbBase(InfProbIn->samplingDim), InfProb(InfProbIn),
                                                          priorDens(std::dynamic_pointer_cast<GaussianDensity>(InfProbIn->
                                                                                                          prior))
  {}

  virtual double eval(const Eigen::VectorXd& xc) override
  {
    return -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);
  }

  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    double fout = -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);

    gradient = -1.0 * InfProb->prior->Jacobian(xc).row(0).transpose() -
               InfProb->likelihood->Jacobian(xc).row(0).transpose();
    return fout;
  }

  // use a Gauss-Newton like approximation to the hessian and conjugate gradient solver for H^{-1}
  virtual Eigen::VectorXd applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override
  {
    return priorDens->specification->ApplyCovariance(vecIn);
  }

  // use a Gauss-Newton like approximation to the hessian
  virtual Eigen::VectorXd applyHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override
  {
    return priorDens->specification->ApplyInverseCovariance(vecIn); // priorCov^{-1}*vecIn
  }

private:

  shared_ptr<InferenceProblem> InfProb;
  shared_ptr<GaussianDensity> priorDens;
};


class Inference2Opt : public OptProbBase {
public:

  Inference2Opt(shared_ptr<InferenceProblem> InfProbIn, bool usePriorIn=true) : OptProbBase(InfProbIn->samplingDim), InfProb(InfProbIn), usePrior(usePriorIn)
  {
    assert(InfProb->forwardMod);
  }

  virtual double eval(const Eigen::VectorXd& xc) override
  {
    if(usePrior){
      return -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);
    }else{
       return -1.0*InfProb->likelihood->LogDensity(xc); 
    }
    
  }

  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    double fout;
    if(usePrior){
      fout = -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);
      gradient = -1.0 * InfProb->prior->Jacobian(xc).row(0).transpose() - InfProb->likelihood->Jacobian(xc).row(0).transpose();
    }else{
      fout = -1.0 * InfProb->likelihood->LogDensity(xc);
      gradient = -1.0 * InfProb->likelihood->Jacobian(xc).row(0).transpose();        
    }
    return fout;
  }

  // use a Gauss-Newton like approximation to the hessian and conjugate gradient solver for H^{-1}
  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc) override
  {
    if(usePrior){
      return -1.0*(InfProb->prior->Hessian(xc,Eigen::VectorXd::Ones(1), 0) + InfProb->likelihood->Hessian(xc,Eigen::VectorXd::Ones(1), 0)) ;  
    }else{
      return -1.0*InfProb->likelihood->Hessian(xc,Eigen::VectorXd::Ones(1), 0);
    }
    
  }
  
private:

  shared_ptr<InferenceProblem> InfProb;
  bool usePrior;
};


class GaussError2Opt : public OptProbBase {
public:

  GaussError2Opt(shared_ptr<InferenceProblem> InfProbIn, bool usePriorIn=true) : OptProbBase(InfProbIn->samplingDim), InfProb(InfProbIn), usePrior(usePriorIn=true)
  {
    assert(InfProb->forwardMod);
  }

  virtual double eval(const Eigen::VectorXd& xc) override
  {
    if(usePrior){
      return -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);
    }else{
      return -1.0 * InfProb->likelihood->LogDensity(xc); 
    }
    
  }

  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    double fout;
    if(usePrior){
      fout = -1.0 * InfProb->prior->LogDensity(xc) - InfProb->likelihood->LogDensity(xc);
      gradient = -1.0 * InfProb->prior->Jacobian(xc).row(0).transpose() - InfProb->likelihood->Jacobian(xc).row(0).transpose();
    }else{
      fout = -1.0 * InfProb->likelihood->LogDensity(xc);
      gradient = -1.0 * InfProb->likelihood->Jacobian(xc).row(0).transpose();
    }
    return fout;
  }

  // use a Gauss-Newton like approximation to the hessian and conjugate gradient solver for H^{-1}
  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc) override
  {
    if(usePrior){
      Eigen::MatrixXd jac = InfProb->forwardMod->Jacobian(xc,0);
      auto likelyHess = jac.transpose()*std::dynamic_pointer_cast<GaussianDensity>(InfProb->errorMod)->specification->ApplyInverseCovariance(jac);
      return -1.0*InfProb->prior->Hessian(xc,Eigen::VectorXd::Ones(1), 0) + likelyHess ;
    }else{
      Eigen::MatrixXd jac = InfProb->forwardMod->Jacobian(xc,0);
      auto likelyHess = jac.transpose()*std::dynamic_pointer_cast<GaussianDensity>(InfProb->errorMod)->specification->ApplyInverseCovariance(jac);
      return likelyHess ;
    }
    
  }
  
private:

  shared_ptr<InferenceProblem> InfProb;
  bool usePrior;
};



class muq::Inference::Sampling2Opt : public OptProbBase {
public:

  Sampling2Opt(shared_ptr<SamplingProblem> InfProbIn) : OptProbBase(InfProbIn->samplingDim), InfProb(InfProbIn) {}

  virtual double eval(const Eigen::VectorXd& xc) override
  {
    return -1.0 * InfProb->density->LogDensity(xc);
  }

  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    double fc = -InfProb->density->LogDensity(xc);

    gradient = -1.0 * InfProb->density->Jacobian(xc).row(0).transpose();
    return fc;
  }

  // use a Gauss-Newton like approximation to the hessian and conjugate gradient solver for H^{-1}
  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc) override
  {
    return -1.0*InfProb->density->Hessian(xc,Eigen::VectorXd::Ones(1), 0);
  }

  
private:

  shared_ptr<SamplingProblem> InfProb;
};


/** Create a MAP solver using the problem defined in the density and the optimization method, etc... defined in the
 * properties ptree.*/
shared_ptr<MAPbase> MAPbase::Create(shared_ptr<AbstractSamplingProblem> InfProb,
                                    boost::property_tree::ptree       & properties)
{
  return make_shared<MAPbase>(InfProb, properties);
}

/** Create a MAP solver using the problem defined in the density and the optimization method, etc... that are defined in
 * the xml file. */
shared_ptr<MAPbase> MAPbase::Create(shared_ptr<AbstractSamplingProblem> InfProb, string xmlFile)
{
  // create the ptree from the xml file and pass this off to the other create function
  std::ifstream ifile(xmlFile.c_str());
  boost::property_tree::ptree pt;

  if (ifile.good()) {
    ifile.close();

    // read the file
    boost::property_tree::read_xml(xmlFile, pt);
  } else {
    std::cerr << "\nWARNING: Not able to open  " << xmlFile << "  please make sure the file exists.  This may cause the program to crash.\n\n";
  }

  return Create(InfProb, pt);
}

void MAPbase::AddUniformConstraints(const std::shared_ptr<UniformDensity> uniformCast, std::shared_ptr<OptProbBase> optProb){
  
  auto boxCast = std::dynamic_pointer_cast<UniformBox>(uniformCast->specification);
  if(boxCast){
    for(int d=0; d<boxCast->lb.size();++d){
      optProb->AddConstraint(make_shared<muq::Optimization::BoundConstraint>(boxCast->lb.size(),d,boxCast->lb(d),true),false); // lower bound
      optProb->AddConstraint(make_shared<muq::Optimization::BoundConstraint>(boxCast->lb.size(),d,boxCast->ub(d),false),false); // upper bound
    }
  }else{
      /// @todo Add predicate of density as constraint function here
  }
}

/** Construct the map solver from the problem defined in the density, and the optimization parameters stored in the
 * ptree. */
MAPbase::MAPbase(std::shared_ptr<AbstractSamplingProblem> InfProb, boost::property_tree::ptree& properties)
{
  shared_ptr<OptProbBase> optProb;

  // first, decide what characteristics the AbstractSamplingProblem has
  auto structuredInfProb = std::dynamic_pointer_cast<InferenceProblem>(InfProb);
  if (structuredInfProb) {
	  
    // check to see if the forward model is defined separately
    if (structuredInfProb->forwardMod) {
      
	  // check to see if we have a gaussian prior
      if (std::dynamic_pointer_cast<GaussianDensity>(structuredInfProb->prior)) {
        
		// check to see if we also have a gaussian error model
        if ((std::dynamic_pointer_cast<GaussianDensity>(structuredInfProb->errorMod))) {
          optProb = make_shared<Gauss2GaussOpt>(structuredInfProb);
        } else {
          // gaussian prior but not error model
          optProb = make_shared<GaussPriorOpt>(structuredInfProb);
        }
      
	    } else {
      
        auto uniformCast = std::dynamic_pointer_cast<UniformDensity>(structuredInfProb->prior);
       
        if ((std::dynamic_pointer_cast<GaussianDensity>(structuredInfProb->errorMod))) {
          optProb = make_shared<GaussError2Opt>(structuredInfProb, uniformCast!=nullptr);
        } else {
          // gaussian prior but not error model
          optProb = make_shared<Inference2Opt>(structuredInfProb, uniformCast!=nullptr);
        }
        
        // check to see if the prior is uniform.  If so, we can explicitly add the constraints
        
        if(uniformCast)
          AddUniformConstraints(uniformCast,optProb);
      }
    
	  } else {
     
     
	  // check to see if we at least have a gaussian prior
      if (std::dynamic_pointer_cast<GaussianDensity>(structuredInfProb->prior)) {
      
        optProb = make_shared<GaussPriorOpt>(structuredInfProb);
      
      } else {
        
        auto uniformCast = std::dynamic_pointer_cast<UniformDensity>(structuredInfProb->prior);
     
        auto sampProblem = make_shared<SamplingProblem>(structuredInfProb->likelihood);
        optProb = make_shared<Sampling2Opt>(sampProblem);
    
        // check to see if the prior is uniform.  If so, we can explicitly add the constraints
        if(uniformCast)
          AddUniformConstraints(uniformCast,optProb);
      }
    }

    // if we couldn't find any structure, just return a basic scalar optimization problem
  } else {
    optProb = make_shared<Sampling2Opt>(std::dynamic_pointer_cast<SamplingProblem>(InfProb));
  }
  assert(optProb);
  OptSolver = OptAlgBase::Create(optProb, properties);
}

/** Compute the maximum of the InferenceProblem starting from the point xc. */
Eigen::VectorXd MAPbase::Solve(const Eigen::VectorXd& xc)
{
  return OptSolver->solve(xc);
}

#if MUQ_PYTHON == 1
shared_ptr<MAPbase> MAPbase::PyCreateFromDict(shared_ptr<AbstractSamplingProblem> InfProb,
                                              boost::python::dict const         & dict)
{
  boost::property_tree::ptree pt = PythonDictToPtree(dict);

  return Create(InfProb, pt);
}

shared_ptr<MAPbase> MAPbase::PyCreateFromString(shared_ptr<AbstractSamplingProblem> InfProb,
                                                string                              xmlFile)
{
  return Create(InfProb, xmlFile);
}

boost::python::list MAPbase::PySolve(boost::python::list const& xc)
{
  return GetPythonVector<Eigen::VectorXd>(Solve(GetEigenVector<Eigen::VectorXd>(xc)));
}

#endif // if MUQ_PYTHON == 1
