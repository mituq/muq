
#include "MUQ/Modelling/ModelOptConstraint.h"

using namespace muq::Modelling;
using namespace muq::Optimization;

/** The usual constructor, user must specify whether the constraint will be an equality or inequality, what Model should
 *  be used as input, and which Model should be used as output.  Remember that all the Models live on a graph, so when
 *  you call InMod.Update(), all models "downstream" will also be updated, which means that the output can be found with
 *  OutMod.Eval().
 */
ModelOptConstraint::ModelOptConstraint(bool isEquality, const Model& MIn, const Model& MOut,
                                       const Eigen::VectorXd cval) : OptConstraintBase(MIn.GetInDim(),
                                                                                       MOut.GetOutDim(), isEquality,
                                                                                       false), ModIn(MIn), ModOut(MOut),
                                                                     ConstVal(cval)
{}


/** Evaluate the constraint. */
Eigen::VectorXd ModelOptConstraint::eval(const Eigen::VectorXd& xc)
{
  Eigen::VectorXd output(GetOutDim());

  // update the input model
  ModIn.Update(xc);

  // get the output value
  ModOut.Eval(output);


  return output - ConstVal;
}

/** Get the gradient of the constraint at a point xc, given the sensitivity of some objective to the output of the
 *  constraint. */
Eigen::VectorXd ModelOptConstraint::grad(const Eigen::VectorXd& xc, const Eigen::VectorXd& SensIn,
                                         Eigen::VectorXd& grad)
{
  Eigen::VectorXd output(GetOutDim());

  // update the input model
  ModIn.Update(xc);

  // get the output value
  ModOut.Eval(output);

  // set the gradient to zero
  grad = Eigen::VectorXd::Zero(GetInDim());

  // use the Models to get the gradient
  ModOut.Grad(ModIn, SensIn, grad);

  return output - ConstVal;
}

