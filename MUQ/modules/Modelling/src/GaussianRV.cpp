#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Modelling/GaussianRV.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

GaussianRV::GaussianRV(shared_ptr<GaussianSpecification> const& specification) :
  RandVar(specification->inputSizes, specification->dim.sum(), false, false, false,
          false), specification(specification) {}

Eigen::MatrixXd GaussianRV::Sample(int NumSamps)
{
  assert(specification->inputSizes.size() == 0);

  Eigen::MatrixXd stdRandom = RandomGenerator::GetNormalRandomMatrix(specification->dim.sum(), NumSamps);

  return specification->ApplyCovarianceSqrt(stdRandom).colwise() + specification->Mean();
}

Eigen::MatrixXd GaussianRV::Sample(vector<Eigen::VectorXd> const& input, int NumSamps)
{
  Eigen::MatrixXd stdRandom = RandomGenerator::GetNormalRandomMatrix(specification->dim.sum(), NumSamps);

  return specification->ApplyCovarianceSqrt(stdRandom, input).colwise() + specification->Mean(input);
}

Eigen::VectorXd GaussianRV::EvaluateImpl(vector<Eigen::VectorXd> const& input)
{
  Eigen::VectorXd stdRandom = RandomGenerator::GetNormalRandomVector(specification->dim.sum());

  return specification->Mean(input) + specification->ApplyCovarianceSqrt(stdRandom, input);
}

Eigen::VectorXd GaussianRV::TransformStandardNormalSample(Eigen::VectorXd const        & vec,
                                                          vector<Eigen::VectorXd> const& inputs)
{
  return specification->ApplyCovarianceSqrt(vec, inputs).colwise() + specification->Mean(inputs);
}

