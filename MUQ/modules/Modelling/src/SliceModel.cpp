
#include "MUQ/Modelling/SliceModel.h"

#include <assert.h>

using namespace muq::Modelling;

/** Construct from a start index, end index, and skip interval.  The start index and end index are inclusive.
 */
SliceModel::SliceModel(int InputDim, int StartIndIn, int EndIndIn, int SkipIn) : OneInputFullModPiece(InputDim,
                                                                                                      floor((SkipIn +
                                                                                                             EndIndIn -
                                                                                                             StartIndIn)
/ SkipIn))
{
  Indices.clear();
  for (int i = StartIndIn; i <= EndIndIn; i += SkipIn) {
    assert(i < InputDim);
    Indices.push_back(i);
  }
}

/** Construct from a vector of the the indices to keep. */
SliceModel::SliceModel(int InputDim, const std::vector<unsigned int>& IndicesIn) : OneInputFullModPiece(InputDim,
                                                                                                        IndicesIn.size()),
                                                                                   Indices(IndicesIn)
{}


Eigen::VectorXd SliceModel::EvaluateImpl(Eigen::VectorXd const& input)
{
  Eigen::VectorXd output(outputSize);

  for (unsigned int i = 0; i < Indices.size(); ++i) {
    output[i] = input[Indices[i]];
  }

  return output;
}

Eigen::VectorXd SliceModel::GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
{
  Eigen::VectorXd GradVec = Eigen::VectorXd::Zero(inputSizes[0]);

  for (unsigned int i = 0; i < Indices.size(); ++i) {
    GradVec[Indices[i]] += sensitivity[i];
  }
  return GradVec;
}

Eigen::MatrixXd SliceModel::JacobianImpl(Eigen::VectorXd const& input)
{
  Eigen::MatrixXd jac = Eigen::MatrixXd::Zero(outputSize, inputSizes[0]);

  for (unsigned int i = 0; i < Indices.size(); ++i) {
    jac(i, Indices[i]) = 1;
  }

  return jac;
}

Eigen::VectorXd SliceModel::JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target)
{
  return Evaluate(target);
}

Eigen::MatrixXd SliceModel::HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
{
  return Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);
}

