
#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"

using namespace muq::Modelling;

Model muq::Modelling::exp(const Model& ModIn)
{
  Model output = std::make_shared<ExpModel>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);

  return output;
}

Model muq::Modelling::sin(const Model& ModIn)
{
  Model output = std::make_shared<SinModel>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);
  return output;
}

Model muq::Modelling::cos(const Model& ModIn)
{
  Model output = std::make_shared<CosModel>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);
  return output;
}

Model muq::Modelling::pow10(const Model& ModIn)
{
  Model output = std::make_shared<Pow10Model>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);
  return output;
}

Model muq::Modelling::abs(const Model& ModIn)
{
  Model output = std::make_shared<AbsModel>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);
  return output;
}

Model muq::Modelling::pow2(const Model& ModIn)
{
  Model output = std::make_shared<Pow2Model>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);
  return output;
}

Model muq::Modelling::log(const Model& ModIn)
{
  Model output = std::make_shared<LogModel>(ModIn.GetOutDim());

  output.SetInput(ModIn, 0);

  return output;
}

