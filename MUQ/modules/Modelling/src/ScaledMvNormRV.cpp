#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Modelling/ScaledMvNormRV.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

Eigen::MatrixXd ScaledMvNormRV::Sample(vector<Eigen::VectorXd> const& input, int NumSamps)
{
  const double sigma = input[0](0);

  Eigen::MatrixXd stdRandom = RandomGenerator::GetNormalRandomMatrix(specification->dim, NumSamps);

  Eigen::MatrixXd unscaled = specification->ApplyCovarianceSqrt(stdRandom);

  return (unscaled * sqrt(sigma)).colwise() + specification->Mean;
}

Eigen::VectorXd ScaledMvNormRV::EvaluateImpl(vector<Eigen::VectorXd> const& input)
{
  return Sample(input, 1);
}

Eigen::VectorXd ScaledMvNormRV::TransformStandardNormalSample(Eigen::VectorXd const& input, double const sigma)
{
  Eigen::VectorXd unscaled = specification->ApplyCovarianceSqrt(input);

  return unscaled * sqrt(sigma) + specification->Mean;
}

