
#include "../../../MUQ/Modelling/GammaDensityRV.h"


using namespace muq::Modelling;
using namespace Eigen;

GammaSpecification::GammaSpecification(double const alpha, double const beta,
                                       int length) : length(length),  alpha(alpha), beta(beta)
{
  assert(alpha > 0);
  assert(beta > 0);
  assert(length > 0);
}

VectorXd GammaSpecification::Sample() const
{
  VectorXd result(length);

  for (int i = 0; i < length; ++i) {
    result(i) = muq::Utilities::RandomGenerator::GetGamma(alpha, beta);
  }
  return result;
}

double GammaSpecification::LogDensity(Eigen::VectorXd const& input)
{
  //check the support first
  if (!(input.array() > 0).all()) {
    return -std::numeric_limits<double>::infinity();
  }

  //else compute the density
  VectorXd temp = (alpha - 1.0) * input.array().log() - 1.0 / beta * input.array();
  return temp.sum();
}

GammaDensity::GammaDensity(std::shared_ptr<GammaSpecification> spec) : Density(VectorXi::Constant(1,
                                                                                                  spec->length), false, false, false,
                                                                               false),
                                                                       spec(spec)
{}

double GammaDensity::LogDensityImpl(std::vector<Eigen::VectorXd> const& input)
{
  return spec->LogDensity(input.at(0));
}

GammaRV::GammaRV(std::shared_ptr<GammaSpecification> spec) : RandVar(
                                                               VectorXi(), spec->length, false, false, false,
                                                               false),
                                                             spec(spec)
{}

Eigen::VectorXd GammaRV::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  return spec->Sample();
}

