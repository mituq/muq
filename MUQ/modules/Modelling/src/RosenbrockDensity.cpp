#include "MUQ/Modelling/RosenbrockDensity.h"

using namespace std;
using namespace muq::Modelling;

RosenbrockDensity::RosenbrockDensity(shared_ptr<RosenbrockSpecification> const& specification) :
  Density(2 * Eigen::VectorXi::Ones(1), false, true, false, false), specification(specification) {}

double RosenbrockDensity::LogDensityImpl(vector<Eigen::VectorXd> const& input)
{
  assert(specification);
  
  // compute Rosenbrock parameters
  const double a = specification->a(input);
  const double b = specification->b(input);
  
  // first, do the transformation to the Gaussian domain
  double x1 = input.at(0) (0) / a;
  double x2 = a * input.at(0) (1) - a * b * (pow(input.at(0) (0), 2.0) + a * a);
  
  return -0.5 * (x1 * x1 + x2 * x2) - log(2.0*M_PI);
}

Eigen::MatrixXd RosenbrockDensity::JacobianImpl(vector<Eigen::VectorXd> const& input, int inputDimWrt) 
{
  assert(specification);
  assert(inputDimWrt==0);
  
  // compute Rosenbrock parameters
  const double a = specification->a(input);
  const double b = specification->b(input);

  Eigen::VectorXd output(2);

  const double y1 = input.at(0) (0);
  const double y2 = input.at(0) (1);
  
  output(0) = -y1 / a + 2 * a * a * y2 * b * y1 - 2 *a *a *b *b *pow(y1, 3.0) - 2 * pow(a, 4.0) * b * b * y1;
  output(1) = -a * a * y2 + a * a * b * (y1 * y1 + a * a);

  return output.transpose();
}

Eigen::MatrixXd RosenbrockDensity::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                               Eigen::VectorXd const             & sensitvity,
                                               int const                           inputDimWrt)
{
  assert(specification);
  assert(inputDimWrt==0);
  
  // compute Rosenbrock parameters
  const double a = specification->a(input);
  const double b = specification->b(input);
  
  const double a2 = pow(a,2);
  
  const double y1 = input.at(0) (0);
  const double y2 = input.at(0) (1);
  
  Eigen::MatrixXd output(2,2);
  output(0,0) = (-2*pow(a,6)*pow(b,2)-6*pow(y1,2)*pow(b,2)*pow(a,4)+2*pow(a,4)*b*y2 -1)/a2;
  output(0,1) = 2*a2*y1*b;
  output(1,0) = output(0,1);
  output(1,1) = -a2;
  
  return sensitvity(0)*output;
}

