
##########################################
#  Objects to build


# files containing google tests
set(Test_Modelling_Sources

    Modelling/test/ModelTest.cpp
    Modelling/test/ModGraphTest.cpp
    
#     Modelling/test/ModPieceBaseTest.cpp
#     Modelling/test/ModParameterTest.cpp
#     Modelling/test/SymbolicModTest.cpp
    Modelling/test/RandVarTest.cpp
    Modelling/test/AnalyticFunctionTest.cpp
	Modelling/test/MultiplicationModPieceTest.cpp
#     Modelling/test/PceModelTest.cpp
    Modelling/test/LinearModelTest.cpp
#     #Modelling/test/ModelOptConstraintTest.cpp
     Modelling/test/UniformDensTest.cpp
#     Modelling/test/AssignmentTests.cpp
#     Modelling/test/OptimalMapTests.cpp
#     Modelling/test/InferenceMapTest.cpp
  Modelling/test/CachedModPiece.cpp
  Modelling/test/PointCacheTest.cpp
  Modelling/test/OdeTests.cpp
  Modelling/test/SolverTests.cpp

  Modelling/test/SumModelTest.cpp
  Modelling/test/ModGraphOperationsTest.cpp

  Modelling/test/GaussianTest.cpp

  Modelling/test/RosenbrockTest.cpp
)

if(MUQ_USE_SACADO)
    set(Test_Modelling_Sources
        ${Test_Modelling_Sources}
        Modelling/test/sacadoTest.cpp
        
        PARENT_SCOPE
    )
else()
    set(Test_Modelling_Sources
        ${Test_Modelling_Sources}
        
        PARENT_SCOPE
    )
endif()
#if(MUQ_USE_CUDA)
#    set(Test_Modelling_Sources
#        ${Test_Modelling_Sources}
#        Modelling/test/CudaDistanceTest.cpp
#        
#        PARENT_SCOPE
#    )
#endif(MUQ_USE_CUDA)



