
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Modelling/SumModel.h"
#include "MUQ/Modelling/DifferenceModel.h"
#include "MUQ/Modelling/ProductModel.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace Eigen;

TEST(SumModel, BasicTest)
{
  auto mod = make_shared<SumModel>(3, 5);

  vector<Eigen::VectorXd> inputs(3);
  for (unsigned int i = 0; i < 3; ++i) {
    inputs[i] = (double)i * Eigen::VectorXd::Ones(5);
  }

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Evaluate(inputs),    3.0 * Eigen::VectorXd::Ones(5),  1.0e-9);

  for (int i = 0; i < 3; ++i) {
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Jacobian(inputs, i), Eigen::MatrixXd::Identity(5, 5), 1.0e-9);

    Eigen::VectorXd target = ((double)i + 1.0) * Eigen::VectorXd::Ones(5);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->JacobianAction(inputs, target, i), target, 1.0e-9);

    Eigen::VectorXd sens = ((double)i - 1.0) * Eigen::VectorXd::Ones(5);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Gradient(inputs, sens, i), sens,                        1.0e-9);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Hessian(inputs, sens, i),  Eigen::MatrixXd::Zero(5, 5), 1.0e-9);
  }
}

TEST(DifferenceModel, BasicTest)
{
  auto mod = make_shared<DifferenceModel>(5);

  vector<Eigen::VectorXd> inputs(2);
  for (unsigned int i = 0; i < 2; ++i) {
    inputs[i] = (double)i * Eigen::VectorXd::Ones(5);
  }

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Evaluate(inputs),    -1.0 * Eigen::VectorXd::Ones(
                        5),                                                                         1.0e-9);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Jacobian(inputs, 0), Eigen::MatrixXd::Identity(5,
                                                                                             5),    1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Jacobian(inputs, 1), -Eigen::MatrixXd::Identity(5,
                                                                                              5),   1.0e-9);

  Eigen::VectorXd target = Eigen::VectorXd::Ones(5);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->JacobianAction(inputs,
                                                             target,
                                                             0), target,                            1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->JacobianAction(inputs,
                                                             target,
                                                             1), -target,                           1.0e-9);

  Eigen::VectorXd sens =  Eigen::VectorXd::Ones(5);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Gradient(inputs,
                                                       sens,
                                                       0),         sens,                            1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Gradient(inputs,
                                                       sens,
                                                       1),         -sens,                           1.0e-9);

  for (int i = 0; i < 2; ++i) {
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Hessian(inputs, sens, i), Eigen::MatrixXd::Zero(5,
                                                                                                5), 1.0e-9);
  }
}


TEST(ProductModel, BasicTest)
{
  auto mod = make_shared<ProductModel>(3, 5);

  vector<Eigen::VectorXd> inputs(3);
  for (unsigned int i = 0; i < 3; ++i) {
    inputs[i] = ((double)i + 1.0) * Eigen::VectorXd::Ones(5);
  }

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Evaluate(inputs), 6.0 * Eigen::VectorXd::Ones(5), 1.0e-9);

  Eigen::VectorXd target(5);
  target << 1, 2, 3, 4, 5;
  Eigen::VectorXd sens(5);
  sens << 5, 4, 3, 2, -1;

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Jacobian(inputs, 0), 6.0 * Eigen::MatrixXd::Identity(5,
                                                                                                   5), 1.0e-9);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->JacobianAction(inputs,
                                                             target,
                                                             0), 6 * target,                           1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->JacobianAction(inputs,
                                                             target,
                                                             1), 3 * target,                           1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->JacobianAction(inputs,
                                                             target,
                                                             2), 2 * target,                           1.0e-9);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Gradient(inputs,
                                                       sens,
                                                       0),         6 * sens,                           1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Gradient(inputs,
                                                       sens,
                                                       1),         3 * sens,                           1.0e-9);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Gradient(inputs,
                                                       sens,
                                                       2),         2 * sens,                           1.0e-9);

  for (int i = 0; i < 3; ++i) {
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, mod->Hessian(inputs, sens, i), Eigen::MatrixXd::Zero(5,
                                                                                                5),    1.0e-9);
  }
}
