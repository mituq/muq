import unittest
from numpy import *

import libmuqModelling

class sinSumMod(libmuqModelling.ModPiece):
    def __init__(self,dim,numIn=2):
        super(sinSumMod,self).__init__([dim]*numIn,dim,True,True,True,True,False)

    def EvaluateImpl(self,x):
        output = [0.0]*self.outputSize
        for i in range(len(x)):
            output += sin(x[i])
        return output.tolist()

    def GradientImpl(self,x,sens,dim):
        return (cos(x[dim])*array(sens)).tolist()

    def JacobianImpl(self,x,dim):
        output = zeros((self.outputSize,self.outputSize))
        for i in range(self.outputSize):
            output[i][i] = cos(x[dim][i])
        return output.tolist()

    def JacobianActionImpl(self,x,targ,dim):
        return dot(array(self.Jacobian(x,dim)),array(targ)).tolist()

    def HessianImpl(self,x,sens,dim):
        output = zeros((self.outputSize,self.outputSize))
        for i in range(self.outputSize):
            output[i][i] -= sens[i]*sin(x[dim][i])
        return output.tolist()

class squareMod(libmuqModelling.OneInputFullModPiece):
    def __init__(self,inDim):
        super(squareMod,self).__init__(inDim,inDim)

    def EvaluateImpl(self,x):
        return (array(x)**2).tolist()

    def GradientImpl(self,x,sens,dim):
        assert(dim==0)
        return (2.0*array(x)*array(sens)).tolist()

    def JacobianImpl(self,x,dim):
        assert(dim==0)
        output = zeros((self.outputSize,self.outputSize))
        for i in range(self.outputSize):
            output[i][i] = 2.0*x[i]
        return output.tolist()

    def JacboianActionImpl(self,x,targ,dim):
        assert(dim==0)
        return dot(array(Jacobian(x,dim)),array(targ)).tolist()

    def HessianImpl(self,x,sens,dim):
        assert(dim==0)
        hess = zeros((self.outputSize,self.outputSize))
        for i in range(self.outputSize):
            hess[i][i] = 2.0*sens[i]
        return hess.tolist()

class sumSquareMod(libmuqModelling.OneInputFullModPiece):
    def __init__(self,inDim,outDim=1):
        super(sumSquareMod,self).__init__(inDim,outDim)

    def EvaluateImpl(self,x):
        return [(array(x)**2).sum()]*self.outputSize

    def GradientImpl(self,x,sens,dim):
        assert(dim==0)
        output = zeros((self.inputSizes[0]))
        for i in range(self.outputSize):
            output += 2.0*array(x)*sens[i]
        return output.tolist()
        
    def JacobianImpl(self,x,dim):
        assert(dim==0)
        output = zeros((self.outputSize,self.inputSizes[0]))
        for i in range(self.outputSize):
            output[i,:] = 2.0*array(x)
        return output.tolist()

    def JacobianActionImpl(self,x,targ,dim):
        return dot(array(self.Jacobian(x)),array(targ)).tolist()

    def HessianImpl(self,x,sens,dim):
        output = zeros((self.inputSizes[0],self.inputSizes[0]))
        for j in range(self.outputSize):
            for i in range(self.inputSizes[0]):
                output[i,i] += sens[j]*2.0
        return output.tolist()

class ModGraphPieceTest(unittest.TestCase):
    def testBasicTest(self):
        myGraph = libmuqModelling.ModGraph()

        # create some models
        f2 = sinSumMod(2)
        f1 = sinSumMod(2)
        x  = squareMod(2)
        y1 = squareMod(2)
        y2 = squareMod(2)

        # add some nodes
        myGraph.AddNode(f2,"f2")
        myGraph.AddNode(f1,"f1")
        myGraph.AddNode(x,"x")
        myGraph.AddNode(y1,"y1")
        myGraph.AddNode(y2,"y2")

        # connect them
        myGraph.AddEdge("x","f2",0)
        myGraph.AddEdge("y1","f1",0)
        myGraph.AddEdge("y2","f1",1)
        myGraph.AddEdge("f1","f2",1)
        myGraph.writeGraphViz("results/tests/GraphViz/BasicTestPython.pdf")

        # bind some nodes
        myGraph.BindNode("y1",[0.1]*2)
        myGraph.BindNode("y2",[0.2]*2)

        # create the ModGraphPiece model
        graphMod = libmuqModelling.ModGraphPiece(myGraph, "f2")
        graphMod2 = myGraph.ConstructModPiece("f2")

        self.assertTrue(len(graphMod.inputSizes)==1)
        self.assertTrue(graphMod.inputSizes[0]==2)
        self.assertTrue(graphMod.outputSize==2)
        
        # evaluate test
        inVec = [1.0]*2
        outVec = graphMod.Evaluate([inVec])
        outVec2 = graphMod2.Evaluate([inVec])

        self.assertEqual(outVec2[0], outVec[0])
        self.assertEqual(outVec2[1], outVec[1])

        self.assertTrue(sin(inVec[0]*inVec[0])+sin(sin(0.1)+sin(0.2))==outVec[0])
        self.assertTrue(sin(inVec[1]*inVec[1])+sin(sin(0.1)+sin(0.2))==outVec[0])

        # gradient test
        grad = graphMod.Gradient([inVec],[1.0]*2,0)

        self.assertTrue(2.0*inVec[0]*cos(inVec[0]*inVec[0])==grad[0])
        self.assertTrue(2.0*inVec[1]*cos(inVec[1]*inVec[1])==grad[1])

        # jacobian action test
        inVec2 = (array(inVec)+array([random.random(), random.random()])).tolist()
        linOut = graphMod.JacobianAction([inVec],inVec2,0)

        self.assertTrue(2.0*inVec[0]*cos(inVec[0]*inVec[0])*inVec2[0]==linOut[0])
        self.assertTrue(2.0*inVec[1]*cos(inVec[1]*inVec[1])*inVec2[1]==linOut[1])

        # jacobian testing
        jacOut = graphMod.Jacobian([inVec],0)
        self.assertTrue(2.0*inVec[0]*cos(inVec[0]*inVec[0])==jacOut[0][0])
        self.assertTrue(0.0==jacOut[1][0])
        self.assertTrue(0.0==jacOut[0][1])
        self.assertTrue(2.0*inVec[1]*cos(inVec[1]*inVec[1])==jacOut[1][1])

        # hessian testing
        hessOut = graphMod.Hessian([inVec],[1.0]*2,0)
        self.assertTrue(-4.0*inVec[0]*inVec[0]*sin(inVec[0]*inVec[0])+2.0*cos(inVec[0]*inVec[0])==hessOut[0][0])
        self.assertTrue(0.0==hessOut[1][0])
        self.assertTrue(0.0==hessOut[0][1])
        self.assertTrue(-4.0*inVec[1]*inVec[1]*sin(inVec[1]*inVec[1])+2.0*cos(inVec[1]*inVec[1])==hessOut[1][1])

    def testHessianDimensionChange(self):
        inDim = 2

        myGraph = libmuqModelling.ModGraph()

        # create some models
        snm = sinSumMod(1)
        sq1 = sumSquareMod(inDim)
        sq2 = sumSquareMod(inDim)

        # add nodes
        myGraph.AddNode(snm,"sin")
        myGraph.AddNode(sq1,"square1")
        myGraph.AddNode(sq2,"square2")

        # add edges
        myGraph.AddEdge("square1","sin",0)
        myGraph.AddEdge("square2","sin",1)

        # create the model 
        graphMod = libmuqModelling.ModGraphPiece(myGraph, "sin")

        # inputs
        inVec1 = [1.0]*inDim
        inVec2 = [1.0]*inDim

        # compute Hessian
        hess = graphMod.Hessian([inVec1,inVec2],[1.0],0)
        
        diag0 = -4.0*inVec1[0]*inVec1[0]*sin((array(inVec1)*array(inVec1)).sum())+2.0*cos((array(inVec1)*array(inVec1)).sum())
        diag1 = -4.0*inVec1[1]*inVec1[1]*sin((array(inVec1)*array(inVec1)).sum())+2.0*cos((array(inVec1)*array(inVec1)).sum())
        offdiag = -4.0*inVec1[0]*inVec1[1]*sin((array(inVec1)*array(inVec1)).sum())

        self.assertEqual(hess[0][0],diag0)
        self.assertEqual(hess[1][1],diag1)
        self.assertEqual(hess[1][0],offdiag)
        self.assertEqual(hess[0][1],offdiag)

    def testHessianComplexGraph(self):
        inDim1 = 3
        inDim2 = 4
        outDim = 2

        myGraph = libmuqModelling.ModGraph()

        # create some models
        f1 = sinSumMod(outDim,4)
        x1 = sumSquareMod(inDim1,outDim)
        x2 = sumSquareMod(inDim2,outDim)
        y1 = squareMod(outDim)
        y2 = squareMod(outDim)
        z1 = squareMod(outDim)
        z2 = squareMod(outDim)

        # add nodes
        myGraph.AddNode(f1,"f1")
        myGraph.AddNode(x1,"x1")
        myGraph.AddNode(x2,"x2")
        myGraph.AddNode(y1,"y1")
        myGraph.AddNode(y2,"y2")
        myGraph.AddNode(z1,"z1")
        myGraph.AddNode(z2,"z2")

        # add edges
        myGraph.AddEdge("x1", "y1", 0);
        myGraph.AddEdge("x1", "y2", 0);
        myGraph.AddEdge("y1", "f1", 0);
        myGraph.AddEdge("y2", "f1", 1);
        myGraph.AddEdge("x2", "z1", 0);
        myGraph.AddEdge("x2", "z2", 0);
        myGraph.AddEdge("z1", "f1", 2);
        myGraph.AddEdge("z2", "f1", 3);

        # create the model
        graphMod = libmuqModelling.ModGraphPiece(myGraph, "f1")
        graphMod.writeGraphViz("results/tests/GraphViz/HessianTestPython.pdf")

        inVec0 = [0.97]*3
        inVec1 = [0.156]*4
        sens = [0.4213]*2

        for p in range(outDim):
            hess = graphMod.Hessian([inVec0,inVec1],sens,p)
            hessFD = graphMod.HessianByFD([inVec0,inVec1],sens,p)
            for i in range(len(hess)):
                for j in range(len(hess)):
                    self.assertAlmostEqual(hess[i][j],hessFD[i][j],1)

    def testDiamond(self):
        myGraph = libmuqModelling.ModGraph()

        # create models
        f1 = sinSumMod(2)
        x  = squareMod(2)
        y1 = squareMod(2)
        y2 = squareMod(2)

        # add nodes
        myGraph.AddNode(f1,"f1")
        myGraph.AddNode(x,"x")
        myGraph.AddNode(y1,"y1")
        myGraph.AddNode(y2,"y2")

        # add connectivity 
        myGraph.AddEdge("x","y1",0)
        myGraph.AddEdge("x","y2",0)
        myGraph.AddEdge("y1","f1",0)
        myGraph.AddEdge("y2","f1",1)
        myGraph.writeGraphViz("results/tests/GraphViz/DiamondTestPython.pdf")
        graphMod = libmuqModelling.ModGraphPiece(myGraph,"f1")
        graphMod.writeGraphViz("results/tests/GraphViz/DiamondPieceTestPython.pdf")

        # make sure this is what we expect
        self.assertEqual(1,len(graphMod.inputSizes))
        self.assertEqual(2,graphMod.inputSizes[0])
        self.assertEqual(2,graphMod.outputSize)

        # evaulation testing
        inVec = [0.5]*2
        output = graphMod.Evaluate([inVec])

        self.assertEqual(2.0*sin(pow(inVec[0],4.0)),output[0])
        self.assertEqual(2.0*sin(pow(inVec[1],4.0)),output[1])

        # gradient testing
        grad = graphMod.Gradient([inVec],[1.0]*2,0)

        self.assertEqual(8.0*pow(inVec[0],3.0)*cos(pow(inVec[0],4.0)),grad[0])
        self.assertEqual(8.0*pow(inVec[1],3.0)*cos(pow(inVec[1],4.0)),grad[1])

        # jacobian action testing
        inVec2 = [random.random()+inVec[0],random.random()+inVec[1]]
        linOut = graphMod.JacobianAction([inVec],inVec2,0)

        self.assertEqual(8.0*pow(inVec[0],3.0)*cos(pow(inVec[0],4.0))*inVec2[0],linOut[0])
        self.assertEqual(8.0*pow(inVec[1],3.0)*cos(pow(inVec[1],4.0))*inVec2[1],linOut[1])
        
        # jacobian testing 
        jacOut = graphMod.Jacobian([inVec],0)
        self.assertEqual(8.0*pow(inVec[0],3)*cos(pow(inVec[0],4.0)),jacOut[0][0])
        self.assertEqual(0.0,jacOut[1][0])
        self.assertEqual(0.0,jacOut[0][1])
        self.assertEqual(8.0*pow(inVec[1],3)*cos(pow(inVec[1],4.0)),jacOut[1][1])

        # hessian testing 
        hessOut = graphMod.Hessian([inVec],[1.0]*2,0)
        diag = -32.0*pow(inVec[0],6.0)*sin(pow(inVec[0],4.0))+24.0*pow(inVec[0],2.0)*cos(pow(inVec[0],4.0))

        self.assertAlmostEqual(diag,hessOut[0][0],14)
        self.assertAlmostEqual(diag,hessOut[1][1],14)
        self.assertEqual(0.0,hessOut[0][1])
        self.assertEqual(0.0,hessOut[1][0])
        
