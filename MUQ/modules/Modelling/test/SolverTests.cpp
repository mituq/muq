// include the google testing header
#include "gtest/gtest.h"
#include <boost/property_tree/ptree.hpp>

// include the ODE ModPiece header
#include "MUQ/Modelling/SolverModPiece.h"

// include other MUQ models
#include "MUQ/Modelling/LinearModel.h"

using namespace std;
using namespace muq::Modelling;


class ModellingSolverTest_Linear : public::testing::Test {
protected:

  // set up the mesh that we will use, as well as an empty covariance matrix
  virtual void SetUp() override
  {
	  Eigen::MatrixXd A(5,5);
	  A << 15.2710,    4.4067,    1.6644,   -0.6991,   -3.7098,
    	   4.4067,    9.5481,   -1.9777,    3.3411,   -6.2778,
   		   1.6644,   -1.9777,   15.3218,   -0.3454,    3.6397,
   		   -0.6991,    3.3411,   -0.3454,   10.5004,   -3.8877,
   		   -3.7098,   -6.2778,    3.6397,   -3.8877,    8.7315;
		   
	   Eigen::VectorXd b(5);
	   b << -0.1332, -0.7145, 1.3514, -0.2248, -0.5890;
	   
	   f = make_shared<LinearModel>(-b,A);
		   
  }

  virtual void TearDown() override
  {
	  auto fInv = make_shared<SolverModPiece>(f, params);
	  
	  std::vector<Eigen::VectorXd> input(1);
	  input.at(0) = Eigen::VectorXd::Ones(5);
	  
	  Eigen::VectorXd modSol = fInv->Evaluate(input);
	  
	  Eigen::VectorXd trueSol(5);
	  trueSol << -0.046657757763242, -0.219999065967568, 0.143669574832940, -0.075243876658131, -0.338846802598012;
	  
	  for(int i=0; i<5; ++i)
		  EXPECT_NEAR(trueSol(i),modSol(i),1e-8);
  }
  
  std::shared_ptr<ModPiece>   f;
  Eigen::VectorXd sol;
  
  boost::property_tree::ptree params;
};




class NonlinearModel : public ModPiece {
public:

  NonlinearModel() : ModPiece(Eigen::VectorXi::Constant(2,2), 2, true, true, true, false,false) {};
  virtual ~NonlinearModel() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    
	  // return the gradient of the rosenbrock function f = (1-x)^2 + 100*(y-x^2)^2
	  double x = inputs.at(0)(0);
	  double y = inputs.at(0)(1);
	  double a = inputs.at(1)(0);
	  double b = inputs.at(1)(1);
	  
	  Eigen::VectorXd grad(2);
	  grad(0) = a*a*b-2*(1-x) - 400*x*(y-x*x);
	  grad(1) = b*b*a+200*(y-x*x);
      
	return grad;
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs, const Eigen::VectorXd &sens, int inputDimWrt) override
  {
	  return Jacobian(inputs,inputDimWrt).transpose()*sens;
  }
  
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int inputDimWrt) override
  {
	  double x = inputs.at(0)(0);
	  double y = inputs.at(0)(1);
	  double a = inputs.at(1)(0);
	  double b = inputs.at(1)(1);
	  
	  if(inputDimWrt==0){
	  	
		  Eigen::MatrixXd hess(2,2);
		  hess(0,0) = 2 + 1200*x*x -400*y;
		  hess(1,0) = hess(0,1) = -400*x;
		  hess(1,1) = 200;
		  return hess;
	  }else{
		  Eigen::MatrixXd hess = Eigen::MatrixXd::Zero(2,2);
		  hess(0,0) = 2*a*b;
		  hess(0,1) = a*a;
		  hess(1,0) = b*b;
		  hess(1,1) = 2*a*b;
		  return hess;
	  }
	  
  }
  
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& inputs, const Eigen::VectorXd &target, int inputDimWrt) override
  {
	 return Jacobian(inputs,inputDimWrt)*target;
  }
  
};


class ModellingSolverTest_NonLinear : public::testing::Test {
protected:

  // set up the mesh that we will use, as well as an empty covariance matrix
  virtual void SetUp() override
  {   
	   f = make_shared<NonlinearModel>();		   
  }

  virtual void TearDown() override
  {
	  
	  auto fInv = make_shared<SolverModPiece>(f, params);
	  
	  std::vector<Eigen::VectorXd> input(2);
	  input.at(0) = Eigen::VectorXd::Constant(2,0.5);
	  double a = 0.1;
	  double b = 0.2;
	  Eigen::VectorXd input2(2);
	  input2 << a, b;
	  input.at(1) = input2;
	  
	  Eigen::VectorXd modSol = fInv->Evaluate(input);
	  EXPECT_NEAR(-(b*a*a - 2)/(2*a*b*b + 2),modSol(0),1e-7);
	  EXPECT_NEAR(-(- 50*pow(a,4.0)*pow(b,2.0) + pow(a,3.0)*pow(b,6.0) + 2*pow(a,2.0)*pow(b,4.0) + 200*pow(a,2.0)*b + a*pow(b,2.0) - 200)/(200*pow(a*pow(b,2.0) + 1,2.0)),modSol(1),1e-7);
	  
	  
	  Eigen::VectorXd vec = Eigen::VectorXd::Ones(2);
	  Eigen::MatrixXd trueJac(2,2);
	  trueJac(0,0) = -(b*(a*a*b*b + 2*a + 2*b))/(2*pow(a*b*b + 1,2.0));
	  trueJac(0,1) = -(a*(- a*a*b*b + a + 4*b))/(2*pow(a*b*b + 1,2.0));
	  trueJac(1,0) = -(b*(- 100*pow(a,4.0)*pow(b,3.0) + pow(a,3.0)*pow(b,7.0) - 200*pow(a,3.0)*b + 3*pow(a,2.0)*pow(b,5.0) + 3*a*pow(b,3.0) + 400*a + 401*b))/(200*pow(a*pow(b,2.0) + 1,3.0));
	  trueJac(1,1) = -(a*(50*pow(a,4.0)*pow(b,3.0) + pow(a,3.0)*pow(b,7.0) - 50*pow(a,3.0)*b + 3*pow(a,2.0)*pow(b,5.0) - 300*pow(a,2.0)*pow(b,2.0) + 3*a*pow(b,3.0) + 100*a + 401*b))/(100*pow(a*pow(b,2.0) + 1,3.0));

	  Eigen::VectorXd trueGrad = trueJac.transpose()*vec;
	  Eigen::VectorXd trueJacAct = trueJac*vec;
	  
	  Eigen::VectorXd modGrad = fInv->Gradient(input,vec,1);
	  EXPECT_NEAR(trueGrad(0),modGrad(0),1e-7);
	  EXPECT_NEAR(trueGrad(1),modGrad(1),1e-7);
	  
	  Eigen::VectorXd modAct = fInv->JacobianAction(input,vec,1);
	  EXPECT_NEAR(trueJacAct(0),modAct(0),1e-7);
	  EXPECT_NEAR(trueJacAct(1),modAct(1),1e-7);
	  
	  Eigen::MatrixXd modJac = fInv->Jacobian(input,1);
	  EXPECT_NEAR(trueJac(0,0),modJac(0,0),1e-7);
	  EXPECT_NEAR(trueJac(0,1),modJac(0,1),1e-7);
	  EXPECT_NEAR(trueJac(1,0),modJac(1,0),1e-7);
	  EXPECT_NEAR(trueJac(1,1),modJac(1,1),1e-7);
	  
  }
  
  std::shared_ptr<ModPiece>   f;
  
  boost::property_tree::ptree params;
};





TEST_F(ModellingSolverTest_Linear, dense)
{
	params.put("KINSOL.LinearSolver", "Dense");	
}


TEST_F(ModellingSolverTest_Linear, spgmr)
{
	params.put("KINSOL.LinearSolver", "SPGMR");
}

TEST_F(ModellingSolverTest_Linear,spbcg)
{
	params.put("KINSOL.LinearSolver", "SPBCG");
}

TEST_F(ModellingSolverTest_Linear, sptfqmr)
{
	params.put("KINSOL.LinearSolver", "SPTFQMR");
}



TEST_F(ModellingSolverTest_NonLinear, dense)
{
	params.put("KINSOL.LinearSolver", "Dense");	
}


TEST_F(ModellingSolverTest_NonLinear, spgmr)
{
	params.put("KINSOL.LinearSolver", "SPGMR");
}

TEST_F(ModellingSolverTest_NonLinear,spbcg)
{
	params.put("KINSOL.LinearSolver", "SPBCG");
}

TEST_F(ModellingSolverTest_NonLinear, sptfqmr)
{
	params.put("KINSOL.LinearSolver", "SPTFQMR");
}





