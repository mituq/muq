
#include <iostream>
#include <fstream>

#include "gtest/gtest.h"

#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/Density.h"

using namespace muq::Modelling;
using namespace std;


double isPositive(const Eigen::VectorXd& xc)
{
  return xc.minCoeff();
}

// test the bounded uniform density
TEST(ModellingUniformDensTest, Bounded)
{
  // define the lower bounds
  Eigen::VectorXd lb(2);

  lb << 0, 0;

  // define the upper bounds
  Eigen::VectorXd ub(2);
  ub << 1, 1;

  // create the uniform density on [0,1]^2
  UniformDensity theDens(make_shared<UniformBox>(lb, ub));

  // try evaluating the log density on the inside and outside
  Eigen::VectorXd testOut = 2.0 * Eigen::VectorXd::Ones(2);
  Eigen::VectorXd testIn  = 0.5 * Eigen::VectorXd::Ones(2);
  EXPECT_LT(exp(theDens.LogDensity(testOut)), 1e-12);
  EXPECT_EQ(1.0, theDens.LogDensity(testIn));
}


// test using an arbitrary predicate
TEST(ModellingUniformDensTest, Positive)
{
  // create the uniform density on [0,1]^2
  UniformDensity theDens(make_shared<UniformUserPredicate>(isPositive, 2));

  // try evaluating the log density on the inside and outside
  Eigen::VectorXd testOut = -2.0 * Eigen::VectorXd::Ones(2);
  Eigen::VectorXd testIn  = 0.5 * Eigen::VectorXd::Ones(2);

  EXPECT_LT(exp(theDens.LogDensity(testOut)), 1e-12);
  EXPECT_EQ(1.0, theDens.LogDensity(testIn));
}
