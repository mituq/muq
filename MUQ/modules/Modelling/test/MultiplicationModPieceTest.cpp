#include "MUQ/Modelling/MultiplicationModPiece.h"

#include "gtest/gtest.h"

#include <vector>

using namespace muq::Modelling;


TEST(ModellingMultiplicationTest, ScalarVector){
	
	std::vector<Eigen::VectorXd> inputs;
	inputs.push_back(0.1*Eigen::VectorXd::Ones(1));
	inputs.push_back(Eigen::VectorXd::Random(10));
	
	auto multiplier = std::make_shared<MultiplicationModPiece>(Eigen::Vector2i{1,10});
	
	Eigen::VectorXd output = multiplier->Evaluate(inputs);
	multiplier->Jacobian(inputs,0);
	multiplier->Jacobian(inputs,1);
	multiplier->Gradient(inputs,Eigen::VectorXd::Ones(10),0);
	multiplier->Gradient(inputs,Eigen::VectorXd::Ones(10),1);
	
	for(int d=0; d<10; ++d)
	  EXPECT_DOUBLE_EQ(0.1*inputs.at(1)(d),output(d));
}


TEST(ModellingMultiplicationTest, VectorScalar){
	
	
	std::vector<Eigen::VectorXd> inputs;
	inputs.push_back(Eigen::VectorXd::Random(10));
	inputs.push_back(0.1*Eigen::VectorXd::Ones(1));
	
	auto multiplier = std::make_shared<MultiplicationModPiece>(Eigen::Vector2i{10,1});
	
	Eigen::VectorXd output = multiplier->Evaluate(inputs);
	multiplier->Jacobian(inputs,0);
	multiplier->Jacobian(inputs,1);
	multiplier->Gradient(inputs,Eigen::VectorXd::Ones(10),0);
	multiplier->Gradient(inputs,Eigen::VectorXd::Ones(10),1);
	
	for(int d=0; d<10; ++d)
	  EXPECT_DOUBLE_EQ(0.1*inputs.at(0)(d),output(d));
	
}


TEST(ModellingMultiplicationTest, VectorVector){
	
	
	std::vector<Eigen::VectorXd> inputs;
	inputs.push_back(Eigen::VectorXd::Random(10));
	inputs.push_back(Eigen::VectorXd::Random(10));
	
	auto multiplier = std::make_shared<MultiplicationModPiece>(Eigen::Vector2i{10,10});
	
	Eigen::VectorXd output = multiplier->Evaluate(inputs);
	multiplier->Jacobian(inputs,0);
	multiplier->Jacobian(inputs,1);
	multiplier->Gradient(inputs,Eigen::VectorXd::Ones(10),0);
	multiplier->Gradient(inputs,Eigen::VectorXd::Ones(10),1);
	
	for(int d=0; d<10; ++d)
	  EXPECT_DOUBLE_EQ(inputs.at(1)(d)*inputs.at(0)(d),output(d));
	
}