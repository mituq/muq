#include <boost/property_tree/ptree.hpp> // needed here to avoid weird osx "toupper" bug when compiling MUQ with python

#include "gtest/gtest.h"

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <limits>

#include "MUQ/Modelling/CachedModPiece.h"

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Modelling/ModPieceTemplates.h"

using namespace std;
using namespace Eigen;
using namespace muq::Modelling;
using namespace muq::Utilities;

class InfModel : public OneInputNoDerivModPiece {
public:

  InfModel() : OneInputNoDerivModPiece(3, 1) {}

  virtual ~InfModel() = default;

private:

  VectorXd EvaluateImpl(VectorXd const& input) override
  {
    if (input(0) < 15) {
      return VectorXd::Constant(1, input(0));
    } else {
      return VectorXd::Constant(1, std::numeric_limits<double>::infinity());
    }
  }
};

TEST(Modelling, CachedNeighborFunctions)
{
  auto rawModel = make_shared<InfModel>();
  auto fn       = make_shared<CachedModPiece>(rawModel);

  MatrixXd points(3, 4);

  points << 1, 2, 3, 4, 2, 6, 7, 8, 3, 10, 11, 12;

  for (int i = 0; i < points.cols(); ++i) {
    fn->Evaluate(points.col(i)); //add to cache
  }


  VectorXd testPoint(3);
  testPoint << 2, 2, 2;

  MatrixXd twoNearest(3, 2);
  twoNearest <<  1,  2, 2,  6, 3, 10;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, twoNearest, fn->FindKNearestNeighbors(testPoint, 2), 1e-14);

  MatrixXd newPoints(3, 2);
  newPoints << 2, 3, 2, 3, 3, 9;
  for (int i = 0; i < newPoints.cols(); ++i) {
    fn->Evaluate(newPoints.col(i)); //add to cache
  }

  MatrixXd radiusNearest(3, 2);
  radiusNearest << 2, 1, 2, 2, 3, 3;
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, radiusNearest, fn->FindNeighborsWithinRadius(testPoint, 3.0), 1e-14);

  MatrixXd failing(3, 2);
  failing << 18, 9, 2, 2, 5, 7;

  for (int i = 0; i < failing.cols(); ++i) {
    EXPECT_TRUE(!isfinite(fn->Evaluate(failing.col(i)) (0)) || i); //add to cache
  }


  MatrixXd finalPoints(3, 7);
  finalPoints << 2,  1, 3,  9, 2, 3, 4, 2, 2, 3, 2, 6, 7, 8, 3, 3, 9, 7, 10, 11, 12;
  EXPECT_PRED_FORMAT2(MatrixEqual, finalPoints, fn->FindNeighborsWithinRadius(testPoint, 30.0));

  //       EXPECT_PRED_FORMAT3(MatrixApproxEqual,radiusNearest,,1e-14);
}


