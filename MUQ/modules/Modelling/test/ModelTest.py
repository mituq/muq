import unittest
from numpy import * # math stuff in python

import libmuqModelling

# one input
class SinModNoDeriv(libmuqModelling.OneInputNoDerivModPiece):
    def __init__(self,dim):
        libmuqModelling.OneInputNoDerivModPiece.__init__(self,dim,dim)
    
    def EvaluateImpl(self,x):
        return sin(x).tolist()

# adjoint only
class SinModAdjoint(libmuqModelling.OneInputAdjointModPiece):
    def __init__(self,dim):
        libmuqModelling.OneInputAdjointModPiece.__init__(self,dim,dim)
    
    def EvaluateImpl(self,x):
        return sin(x).tolist()

    def GradientImpl(self,x,sens,inputDimWrt):
        assert(self.hasDirectGradient and inputDimWrt==0)
        return cos(x).tolist()

# jacobian only
class SinModJacobian(libmuqModelling.OneInputJacobianModPiece):
    def __init__(self,dim):
        libmuqModelling.OneInputJacobianModPiece.__init__(self,dim,dim)
    
    def EvaluateImpl(self,x):
        return sin(x).tolist()

    def JacobianImpl(self,x,inputDimWrt):
        assert(self.hasDirectJacobian and inputDimWrt==0)
        jac = [[0.0]*self.inputSizes[0] for i in range(self.inputSizes[0])]
        for i in range(self.inputSizes[0]):
            jac[i][i] = cos(x[i])
        return jac

# adjoint and jacobian
class SinModAdjointJacobian(libmuqModelling.OneInputAdjointJacobianModPiece):
    def __init__(self,dim):
        libmuqModelling.OneInputAdjointJacobianModPiece.__init__(self,dim,dim)
    
    def EvaluateImpl(self,x):
        return sin(x).tolist()

    def GradientImpl(self,x,sens,inputDimWrt):
        assert(self.hasDirectGradient and inputDimWrt==0)
        return cos(x).tolist()

    def JacobianImpl(self,x,inputDimWrt):
        assert(self.hasDirectJacobian and inputDimWrt==0)
        jac = [[0.0]*self.inputSizes[0] for i in range(self.inputSizes[0])]
        for i in range(self.inputSizes[0]):
            jac[i][i] = cos(x[i])
        return jac

    def JacobianActionImpl(self,x,target,inputDimWrt):
        assert(self.hasDirectJacobianAction and inputDimWrt==0)
        jacAction = cos(x)*target
        return jacAction.tolist()

# full derivative information
class SinModFull(libmuqModelling.OneInputFullModPiece):
    def __init__(self,dim):
        libmuqModelling.OneInputFullModPiece.__init__(self,dim,dim)
    
    def EvaluateImpl(self,x):
        return sin(x).tolist()

    def GradientImpl(self,x,sens,inputDimWrt):
        assert(self.hasDirectGradient and inputDimWrt==0)
        return cos(x).tolist()

    def JacobianImpl(self,x,inputDimWrt):
        assert(self.hasDirectJacobian and inputDimWrt==0)
        jac = [[0.0]*self.inputSizes[0] for i in range(self.inputSizes[0])]
        for i in range(self.inputSizes[0]):
            jac[i][i] = cos(x[i])
        return jac

    def JacobianActionImpl(self,x,target,inputDimWrt):
        assert(self.hasDirectJacobianAction and inputDimWrt==0)
        jacAction = cos(x)*target
        return jacAction.tolist()

    def HessianImpl(self,x,sens,inputDimWrt):
        assert(self.hasDirectHessian and inputDimWrt==0)
        hess = [[0.0]*self.inputSizes[0] for i in range(self.inputSizes[0])]
        for i in range(self.inputSizes[0]):
            hess[i][i] = -sum(sens)*sin(x[i])
        return hess

# another random mod piece 
class FooMod(libmuqModelling.ModPiece):
    def __init__(self, dim):
        libmuqModelling.ModPiece.__init__(self, [dim], dim, False, False, False, False, False, "RandomName")

    def EvaluateImpl(self,x):
        return x[0]

# test the model implementation
class ModelTest(unittest.TestCase):
    def testNaming(self):
        dim = 10
        
        fooMod = FooMod(dim)

        self.assertEqual(fooMod.GetName(), "RandomName")

        fooMod.SetName("AnotherName")

        self.assertEqual(fooMod.GetName(), "AnotherName")

    def testFiniteDifference(self):
        dim = 25

        vecIn = [1]*dim
        sensIn = [1]*dim

        testSinMod = SinModAdjointJacobian(dim)

        grad = testSinMod.GradientByFD(vecIn,sensIn,0)
        hess = testSinMod.HessianByFD(vecIn,sensIn,0)

        for i in range(dim):
            self.assertAlmostEqual(grad[i],cos(vecIn[i]),4)
            self.assertAlmostEqual(hess[i][i],-dim*sin(vecIn[i]),2)
            for j in range(dim):
                if( j!=i ):
                    self.assertTrue(hess[i][j]==0.0)

    def testSingleEval(self):
        dim = 25
        vecIn = [1]*dim

        testSinMod = SinModNoDeriv(dim)
        result = testSinMod.Evaluate(vecIn)

        # make sure they match
        self.assertTrue(result==sin(vecIn).tolist())

        # try it again
        result = testSinMod.Evaluate(vecIn)
        self.assertTrue(result==sin(vecIn).tolist())

        # and at a different point
        vecIn = [2]*dim
        result = testSinMod.Evaluate(vecIn)
        self.assertTrue(result==sin(vecIn).tolist())

    def testSingleGrad(self):
        dim = 25
        vecIn = [1]*dim
        sensIn = [1]*dim

        testSinMod = SinModAdjoint(dim)

        # make sure gradient evaluates to the correct thing
        gradVec = testSinMod.Gradient(vecIn,sensIn)
        self.assertTrue(gradVec==cos(vecIn).tolist())

        # try it again
        gradVec = testSinMod.Gradient(vecIn,sensIn)
        self.assertTrue(gradVec==cos(vecIn).tolist())

        # try it at a different point
        vecIn = [2]*dim
        gradVec = testSinMod.Gradient(vecIn,sensIn)
        self.assertTrue(gradVec==cos(vecIn).tolist())

    def testSingleJacobian(self):
        dim = 25
        vecIn = [1]*dim

        testSinMod = SinModJacobian(dim)

        # make sure it is correct
        jac = testSinMod.Jacobian(vecIn)
        trueJac = [[0.0]*dim for i in range(dim)]
        for i in range(dim):
            trueJac[i][i] = cos(vecIn[i])
        self.assertTrue(jac==trueJac)

        # try again
        jac = testSinMod.Jacobian(vecIn)
        self.assertTrue(jac==trueJac)

        # try it at a different point
        vecIn = [2]*dim
        jac = testSinMod.Jacobian(vecIn)
        trueJac = [[0.0]*dim for i in range(dim)]
        for i in range(dim):
            trueJac[i][i] = cos(vecIn[i])
        self.assertTrue(jac==trueJac)
        
    def testSingleJacobianAction(self):
        dim = 25;
        vecIn = [1]*dim
        target = [2]*dim

        testSinMod = SinModAdjointJacobian(dim)

        # make sure it is correct
        jacAction = testSinMod.JacobianAction(vecIn,target)
        self.assertTrue(jacAction==(cos(vecIn)*target).tolist())

        # try again
        jacAction = testSinMod.JacobianAction(vecIn,target)
        self.assertTrue(jacAction==(cos(vecIn)*target).tolist())
        
        # and at at different point        
        vecIn = [2]*dim
        jacAction = testSinMod.JacobianAction(vecIn,target)
        self.assertTrue(jacAction==(cos(vecIn)*target).tolist())
        
    def testSingleHessian(self):
        dim = 25;
        vecIn = [1]*dim
        sens = [1]*dim
        
        testSinMod = SinModFull(dim)
        
        # make sure it is correct
        hess = testSinMod.Hessian(vecIn,sens)
        trueHess = [[0.0]*dim for i in range(dim)]
        for i in range(dim):
            trueHess[i][i] = -dim*sin(vecIn[i])
        self.assertTrue(hess==trueHess)

        # try again
        hess = testSinMod.Hessian(vecIn,sens)
        self.assertTrue(hess==trueHess)

        # and at a different point
        vecIn = [2]*dim
        hess = testSinMod.Hessian(vecIn,sens)
        trueHess = [[0.0]*dim for i in range(dim)]
        for i in range(dim):
            trueHess[i][i] = -dim*sin(vecIn[i])
        self.assertTrue(hess==trueHess)

    def testVectorPassthroughModel(self):
        dim = 20
        vecIn = [1.0]*dim
        sens = [2.0]*dim
        target = [3.0]*dim

        mod = libmuqModelling.VectorPassthroughModel(dim)

        result = mod.Evaluate(vecIn)
        grad = mod.Gradient(vecIn,sens)
        jac = mod.Jacobian(vecIn)
        jacAct = mod.JacobianAction(vecIn,target)
        hess = mod.Hessian(vecIn,sens)

        self.assertTrue(grad==sens)
        self.assertTrue(result==vecIn)
        for i in range(dim):
            evec = [0.0]*dim
            evec[i] = 1.0
            self.assertTrue(evec==jac[i])
        self.assertTrue(jacAct==target)
        for i in range(dim):
            evec = [0.0]*dim
            self.assertTrue(evec==hess[i])

