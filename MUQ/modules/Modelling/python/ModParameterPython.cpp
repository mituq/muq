#include "MUQ/Modelling/python/ModParameterPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

void muq::Modelling::ExportModParameter() 
{
  py::class_<ModParameter, shared_ptr<ModParameter>, py::bases<ModPiece>, boost::noncopyable> exportMP("ModParameter", py::init<int, double>());

  exportMP.def(py::init<py::list const&>());

  py::implicitly_convertible<shared_ptr<ModParameter>, shared_ptr<ModPiece> >();
}
