
#include "MUQ/Modelling/python/DensityPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

DensityPython::DensityPython(Eigen::VectorXi const& inputSizes,
                             bool const             hasDirectGradient,
                             bool const             hasDirectJacobian,
                             bool const             hasDirectJacobianAction,
                             bool const             hasDirectHessian) : Density(inputSizes,
                                                                                                             hasDirectGradient,
                                                                                                             hasDirectJacobian,
                                                                                                             hasDirectJacobianAction,
                                                                                                             hasDirectHessian)
{}

double DensityPython::PyLogDensityImpl(boost::python::list const& inputs)
{
  return this->get_override("LogDensity") (inputs);
}

double DensityPython::LogDensityImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  boost::python::list inputPython = VectorToPythonList(inputs);

  return PyLogDensityImpl(inputPython);
}

void muq::Modelling::ExportDensity()
{
  // create class
  boost::python::class_<DensityPython, std::shared_ptr<DensityPython>, boost::python::bases<ModPiece>,
                        boost::noncopyable> exportDensity("Density",
                                                          boost::python::init<Eigen::VectorXi const&, bool const,
                                                                              bool const, bool const, bool const>());

  // allow user to implement LogDensity
  exportDensity.def("LogDensityImpl", boost::python::pure_virtual(&DensityPython::PyLogDensityImpl));

  // allow user to call LogDensity
  exportDensity.def("LogDensity", &Density::PyLogDensity);

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<DensityPython>, std::shared_ptr<Density> >();
}
