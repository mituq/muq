import sys
#sys.path.append('lib') # for MUQ libraries
sys.path.append('modules/Utilities/test')
sys.path.append('modules/Modelling/test')
sys.path.append('modules/Geostatistics/test')
sys.path.append('modules/Inference/test')
sys.path.append('modules/Optimization/test')
sys.path.append('modules/Pde/test')
sys.path.append('modules/Approximation/test')

import unittest
import xmlrunner # needed to create XML output for Jenkins

import libmuqUtilities
import libmuqModelling

try:
  import libmuqPde
  testPde = True
except ImportError:
  testPde= False

from HDF5WrapperTest import *
from RandomGeneratorTest import *
from PolynomialTest import *
from QuadratureTest import *

from ModelTest import *
from OdeModPieceTest import *
from ComponentwiseModelTest import *
from ModGraphTest import *
from ModGraphPieceTest import *
from GaussianTest import *
from LinearModelTest import *
from UniformDensTest import *
from CachedModPieceTest import *
from SumModelTest import *
from ModGraphOperationsTest import *

from CovKernelTest import *

from MetricTests import *
#from BasicMCMCtest import *
from BasicMapTest import *
#from ImportanceSamplerTest import *
#from TransportMapTest import *

from NloptTest import *
from ConstraintTest import *

from RegressionTest import *
from PolynomialApproximatorTest import *
from SmolyakPCEFactoryTest import *

if(testPde):
    from KernelModelTest import *
    from MeshKernelModelTest import *
    from SensorTest import *
    from MeshParameterTest import *
#    from FieldIntegrator1DTest import *

if __name__ == '__main__':
    if(testPde):
      libmeshInit = libmuqPde.LibMeshInit(sys.argv)

    #libmuqUtilities.InitializeLogging(["RunPythonTests"])

    # create MUQGeneralUtilities test suite
    hdf5WrapperTests = unittest.TestLoader().loadTestsFromTestCase(HDF5WrapperTest)
    randomGeneratorTests = unittest.TestLoader().loadTestsFromTestCase(RandomGeneratorTest)
    polynomialTests = unittest.TestLoader().loadTestsFromTestCase(PolynomialTest)
    quadratureTests = unittest.TestLoader().loadTestsFromTestCase(QuadratureTest)
    MUQGeneralUtilitiesTests = [ # list the tests 
                                hdf5WrapperTests,
                                randomGeneratorTests,
                                polynomialTests,
                                quadratureTests
                                ]

    generalUtilitiesTests = unittest.TestSuite(MUQGeneralUtilitiesTests)
    

    # create MUQModelling test suite
    modelTests          = unittest.TestLoader().loadTestsFromTestCase(ModelTest)
    odeTests            = unittest.TestLoader().loadTestsFromTestCase(OdeModPieceTest)
    componentwiseTest   = unittest.TestLoader().loadTestsFromTestCase(ComponentwiseModelTest)
    modGraphTests       = unittest.TestLoader().loadTestsFromTestCase(ModGraphTest)
    modGraphPieceTests  = unittest.TestLoader().loadTestsFromTestCase(ModGraphPieceTest)
    gaussianTests       = unittest.TestLoader().loadTestsFromTestCase(GaussianTest)
    linearModelTests    = unittest.TestLoader().loadTestsFromTestCase(LinearModelTest)
    UniformDensTests    = unittest.TestLoader().loadTestsFromTestCase(UniformDensTest)
    CachedModPieceTests = unittest.TestLoader().loadTestsFromTestCase(CachedModPieceTest)
    SumModelTests       = unittest.TestLoader().loadTestsFromTestCase(SumModelTest)
    ModGraphOperationsTest = unittest.TestLoader().loadTestsFromTestCase(ModGraphOperationsTest)
    
    MUQModellingTests = [ # list the tests
                         modelTests,
                         odeTests,
                         componentwiseTest,
                         modGraphTests,
                         modGraphPieceTests,
                         gaussianTests,
                         linearModelTests,
                         UniformDensTests,
                         CachedModPieceTests,
                         SumModelTests,
                         ModGraphOperationsTest
                         ]

    modellingTests = unittest.TestSuite(MUQModellingTests)

    # create MUQGeostats test suite
    covKernelTests = unittest.TestLoader().loadTestsFromTestCase(CovKernelTest)

    MUQGeostatsTests = [ # list the tests
                        covKernelTests
                       ]

    geostatsTests = unittest.TestSuite(MUQGeostatsTests)

    # create the MUQ Approximation test suite
    regressionTests = unittest.TestLoader().loadTestsFromTestCase(RegressorTest)
    polyApproximatorTests = unittest.TestLoader().loadTestsFromTestCase(PolynomialApproximatorTest)
    lowOrderRegressionTests = unittest.TestLoader().loadTestsFromTestCase(LowOrderRegressorTest)
    smolyakPCEFactoryTests = unittest.TestLoader().loadTestsFromTestCase(SmolyakPCEFactoryTest)
    MUQApproximationTests = [ # list the tests
                              regressionTests,
                              lowOrderRegressionTests,
                              polyApproximatorTests,
                              smolyakPCEFactoryTests
                              ]

    approximationTests = unittest.TestSuite(MUQApproximationTests)

    # create MUQOptimization test suite
    nloptTests      = unittest.TestLoader().loadTestsFromTestCase(NloptTest)
    constraintTests = unittest.TestLoader().loadTestsFromTestCase(ConstraintTest)

    MUQOptimizationTests = [ # list the tests
                            nloptTests,
                            constraintTests
                            ]

    optimizationTests = unittest.TestSuite(MUQOptimizationTests)

    # create MUQInference test suite
    metricTests            = unittest.TestLoader().loadTestsFromTestCase(MetricTest)
    basicMAPTests          = unittest.TestLoader().loadTestsFromTestCase(BasicMAPTest)
    allDefMAPTests         = unittest.TestLoader().loadTestsFromTestCase(AllDefMAPTest)
    #basicMCMCTests         = unittest.TestLoader().loadTestsFromTestCase(BasicMCMCTest)
    #importanceSamplerTests = unittest.TestLoader().loadTestsFromTestCase(ImportanceSamplerTest)
    #transportMapTests      = unittest.TestLoader().loadTestsFromTestCase(TransportMapTest)
    importanceSamplerTests = unittest.TestLoader().loadTestsFromTestCase(ImportanceSamplerTest)

    MUQInferenceTests = [ # list the tests
                         #metricTests,
                         basicMAPTests,
                         allDefMAPTests,
                         #basicMCMCTests,
                         #transportMapTests,
                         #importanceSamplerTests
                         ]

    inferenceTests = unittest.TestSuite(MUQInferenceTests)

    if(testPde):
        # create MUQpde test suite
        kernelModelTests       = unittest.TestLoader().loadTestsFromTestCase(KernelModelTest)
        meshKernelModelTests   = unittest.TestLoader().loadTestsFromTestCase(MeshKernelModelTest)
        sensorTests            = unittest.TestLoader().loadTestsFromTestCase(SensorTest)
        fieldIntegrator1DTests = unittest.TestLoader().loadTestsFromTestCase(FieldIntegrator1DTest)
        MPTests                = unittest.TestLoader().loadTestsFromTestCase(MeshParameterTest)
        
        MUQpdeTests = [ # list the tests
                       kernelModelTests,
                       meshKernelModelTests,
                       sensorTests,
                       fieldIntegrator1DTests,
                       MPTests
                      ]
        
        pdeTests = unittest.TestSuite(MUQpdeTests)

    # run the tests of each module
    unittest.TextTestRunner(verbosity=2).run(generalUtilitiesTests)
    unittest.TextTestRunner(verbosity=2).run(modellingTests)
    unittest.TextTestRunner(verbosity=2).run(geostatsTests)
    #unittest.TextTestRunner(verbosity=2).run(optimizationTests)
    unittest.TextTestRunner(verbosity=2).run(inferenceTests)
    unittest.TextTestRunner(verbosity=2).run(approximationTests)
    if(testPde):    
        unittest.TextTestRunner(verbosity=2).run(pdeTests)





