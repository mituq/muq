function WriteSamples(filename, samps)
%
% USAGE: samps = ReadSamples(filename)
%
% This function provides a way to read samples written by the MUQ
% EmpiricalRandVar into matlab.
%
% INPUT:
%   filename : the name of the binary file written by MUQ
%
% OUTPUT:
%   samps : a matrix containing the samples, each row contains one sample
%

% open the file
fid = fopen(filename,'w');

[NumSamps dim] = size(samps);
% read in the number of samples
 fwrite(fid,NumSamps,'int');

% read in the sample dimension
 fwrite(fid,dim,'int');

% read in the samples
fwrite(fid,samps','double');
fclose(fid);

