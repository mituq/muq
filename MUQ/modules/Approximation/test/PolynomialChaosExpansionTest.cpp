#include "MUQ/Approximation/pce/PolynomialChaosExpansionTest.h"

#include <iostream>
#include <fstream>

#include <Eigen/Dense>
#include <Eigen/LU>
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussChebyshevQuadrature1D.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/ProbabilistHermite.h"

#include "MUQ/Utilities/VariableCollection.h"

#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

VectorXd pceTestConstantine1(VectorXd const& val)
{
  double   epsilon = 0.4;
  Matrix2d opMat;

  opMat << 1 + epsilon, val(0), val(0), 1;

  Vector2d rhs;
  rhs << 2, 1;
  return opMat.colPivHouseholderQr().solve(rhs);
}

VectorXd pceTestConstantine2(VectorXd const& val)
{
  double   epsilon1 = 0.2;
  double   epsilon2 = 2;
  Matrix2d opMat;

  opMat << 1 + epsilon1, val(0), val(0), 1;

  Matrix2d opMat2;
  opMat2 << 1 + epsilon2, val(1), val(1), 1;
  Vector2d rhs;
  rhs << 1, 1;
  Matrix2d fullMat = opMat * opMat2;
  return fullMat.colPivHouseholderQr().solve(rhs);
}

VectorXd pceTestConstantine3(VectorXd const& val)
{
  double   epsilon = 0.01;
  Matrix2d opMat;

  opMat << 2 + val(0), epsilon, epsilon, 2 + val(1);

  Vector2d rhs;
  rhs << 1, 1;
  return opMat.colPivHouseholderQr().solve(rhs);
}

VectorXd pceTestConstantine4(VectorXd const& val)
{
  Matrix2d opMat;

  opMat << 4, val(1) + val(0) * val(1), val(0) + val(0) * val(1), 1;

  Vector2d rhs;
  rhs << 1, 1;
  return opMat.colPivHouseholderQr().solve(rhs);
}


TEST(ApproximationPolynomialChaosExpansion, WeightedSum)
{
  const int outDim = 1;
  const int inputDim = 2;
  const int order = 3;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<HermitePolynomials1DRecursive>();
  
  vector<shared_ptr<PolynomialChaosExpansion>> expansions(3);
  
  // create an expansion with all coefficients equal to 1
  expansions.at(0) = make_shared<PolynomialChaosExpansion>(Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  // create an expansion with all coefficients equal to 2
  expansions.at(1) = make_shared<PolynomialChaosExpansion>(2.0*Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  // create an expansion with all coefficients equal to 3
  expansions.at(2) = make_shared<PolynomialChaosExpansion>(3.0*Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  Eigen::VectorXd weights(3);
  weights << 1.0, 1.0, 0.0;
  auto sumExpansion = PolynomialChaosExpansion::ComputeWeightedSum(expansions,weights);
  
  Eigen::VectorXd testPt = Eigen::VectorXd::Random(inputDim);
  
  Eigen::VectorXd trueEval = weights(0)*expansions.at(0)->Evaluate(testPt) + weights(1)*expansions.at(1)->Evaluate(testPt);
  EXPECT_DOUBLE_EQ(trueEval(0),sumExpansion->Evaluate(testPt)(0));
  
  weights << 0.5, 0.25, 1.0;
  sumExpansion = PolynomialChaosExpansion::ComputeWeightedSum(expansions,weights);
  trueEval = weights(0)*expansions.at(0)->Evaluate(testPt) + weights(1)*expansions.at(1)->Evaluate(testPt) + weights(2)*expansions.at(2)->Evaluate(testPt);
  EXPECT_DOUBLE_EQ(trueEval(0),sumExpansion->Evaluate(testPt)(0));
}
