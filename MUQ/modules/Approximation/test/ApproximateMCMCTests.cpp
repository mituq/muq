
#include <iostream>
#include <utility>

#include <boost/make_shared.hpp>
#include <Eigen/Eigenvalues>
#include <gtest/gtest.h>
#include <boost/property_tree/ptree.hpp>


#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/LogConfig.h"
#include <MUQ/Utilities/RandomGenerator.h>
#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"

 #include "MUQ/Inference/MCMC/MCMCBase.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"


using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;
using namespace boost::property_tree;

#define MCMCTOL 1e-9

class QuarticDensity : public Density {
public:

  QuarticDensity() : Density(Eigen::VectorXi::Constant(1, 2), false, false, false, false) {}

  virtual ~QuarticDensity() = default;

  double LogDensityImpl(std::vector<Eigen::VectorXd> const& input)
  {
    double x = input.at(0) (0);
    double y = input.at(0) (1);

    double result = -3 * x * x - 1.5 * (y - x * x) * (y - x * x);

    return result;
  }
};


TEST(IncrementalApproximation, QuarticDensityLinearApprox)
{
  RandomGeneratorTemporarySetSeed seed(95585523);

  auto graph= std::make_shared<muq::Modelling::ModGraph>();

  auto density = make_shared<QuarticDensity>();

  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");
  graph->AddNode(density, "posterior");
  graph->AddEdge("inferenceTarget", "posterior", 0);

  auto infProblem = make_shared<SamplingProblem>(graph);


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "LocalApproximationKernel");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Steps", 500);
  params.put("MCMC.BurnIn", 5);
  params.put("MCMC.LocalApproximationKernel.approximationCutStart", "inferenceTarget");
  params.put("MCMC.LocalApproximationKernel.approximationCutEnd", "posterior");
  params.put("NearestNeighborApproximation.FitType", "linear");
  params.put("MCMC.MHProposal.PropSize", 2);
  params.put("MCMC.LocalApproximationKernel.initialSampleRadius", 1);
  params.put("Approximator.kN", 6);

  params.put("Verbose", 0);
  auto mcmc = MCMCBase::ConstructMCMC(infProblem, params);

  // sample the distribution

  VectorXd start(2);
  start << 1, 1;
  EmpRvPtr TestPost = mcmc->Sample(start);

  // compute mean and covariance of samples
  Eigen::VectorXd mean = TestPost->getMean();
  Eigen::MatrixXd cov  = TestPost->getCov();

  // check to make sure results are close to what we want.
  //These are from a short chain, tested to high precision against a regression value.
  Eigen::VectorXd recordedMean(2);
  recordedMean << -0.038368675125919204,
0.48434397708672339;

  Eigen::MatrixXd recordedCov(2, 2);
  recordedCov << 0.41307650092305337, 0.087831070898137395,
0.087831070898137395, 0.87470041891631722;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, recordedMean, mean, MCMCTOL);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, recordedCov,  cov,  MCMCTOL);
}

TEST(IncrementalApproximation, QuarticDensityQuadraticApprox)
{
  RandomGeneratorTemporarySetSeed seed(72536779);

  auto graph= std::make_shared<muq::Modelling::ModGraph>();

  auto density = make_shared<QuarticDensity>();

  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");
  graph->AddNode(density, "posterior");
  graph->AddEdge("inferenceTarget", "posterior", 0);

  auto infProblem = make_shared<SamplingProblem>(graph);


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "LocalApproximationKernel");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Steps", 500);
  params.put("MCMC.BurnIn", 5);
  params.put("MCMC.LocalApproximationKernel.approximationCutStart", "inferenceTarget");
  params.put("MCMC.LocalApproximationKernel.approximationCutEnd", "posterior");
  params.put("NearestNeighborApproximation.FitType", "quadratic");
  params.put("MCMC.MHProposal.PropSize", 2);
  params.put("MCMC.LocalApproximationKernel.initialSampleRadius", 1);
  params.put("Approximator.kN", 12);

  params.put("Verbose", 0);
  auto mcmc = MCMCBase::ConstructMCMC(infProblem, params);

  // sample the distribution

  VectorXd start(2);
  start << 1, 1;
  EmpRvPtr TestPost = mcmc->Sample(start);

  // compute mean and covariance of samples
  Eigen::VectorXd mean = TestPost->getMean();
  Eigen::MatrixXd cov  = TestPost->getCov();

  // check to make sure results are close to what we want.
  //These are from a short chain, tested to high precision against a regression value.
  Eigen::VectorXd recordedMean(2);
  recordedMean << -0.057943756058860388,
0.35980557521979445;

  Eigen::MatrixXd recordedCov(2, 2);
  recordedCov << 0.20206994229861944, -0.014390894343360553,
-0.014390894343360553, 0.48709429814207955;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, recordedMean, mean, MCMCTOL);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, recordedCov,  cov,  MCMCTOL);
}

TEST(IncrementalApproximation, QuarticDensityGPApprox)
{
  RandomGeneratorTemporarySetSeed seed(24721313);

    auto graph= std::make_shared<muq::Modelling::ModGraph>();

  auto density = make_shared<QuarticDensity>();

  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");
  graph->AddNode(density, "posterior");
  graph->AddEdge("inferenceTarget", "posterior", 0);

  auto infProblem = make_shared<SamplingProblem>(graph);


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "LocalApproximationKernel");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Steps", 500);
  params.put("MCMC.BurnIn", 5);
  params.put("MCMC.LocalApproximationKernel.approximationCutStart", "inferenceTarget");
  params.put("MCMC.LocalApproximationKernel.approximationCutEnd", "posterior");
  params.put("MCMC.MHProposal.PropSize", 2);
  params.put("MCMC.LocalApproximationKernel.initialSampleRadius", 1);
  params.put("MCMC.LocalApproximationKernel.useNuggetRefinement", false);
  params.put("LocalGPApproximation.numRefineStages", 2);
  params.put("MCMC.LocalApproximationKernel.ApproximationType", "gp");

  params.put("LocalGPApproximation.Regressor.nuggetScale", 1e-6);
  params.put("Verbose", 0);
  params.put("Approximator.kN", 12);

  auto mcmc = MCMCBase::ConstructMCMC(infProblem, params);

  // sample the distribution

  VectorXd start(2);
  start << 1, 1;
  EmpRvPtr TestPost = mcmc->Sample(start);

  // compute mean and covariance of samples
  Eigen::VectorXd mean = TestPost->getMean();
  Eigen::MatrixXd cov  = TestPost->getCov();

  // check to make sure results are close to what we want.
  //These are from a short chain, tested to high precision against a regression value.
  Eigen::VectorXd recordedMean(2);
  recordedMean << 0.072566995795700584,
0.17431439393723849;

  Eigen::MatrixXd recordedCov(2, 2);
  recordedCov << 0.18750586928627372, 0.054272381533669763,
0.054272381533669763, 0.32630867552332921;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, recordedMean, mean, MCMCTOL);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, recordedCov,  cov,  MCMCTOL);
}