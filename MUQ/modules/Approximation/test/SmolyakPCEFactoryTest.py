import unittest
from numpy import * # math stuff in python
import libmuqModelling
from libmuqUtilities import *
from libmuqApproximation import *

# one input
class SinModNoDeriv(libmuqModelling.OneInputNoDerivModPiece):
     def __init__(self,dim):
         libmuqModelling.OneInputNoDerivModPiece.__init__(self,dim,dim)

     def EvaluateImpl(self,x):
         return sin(x).tolist()

class SmolyakPCEFactoryTest(unittest.TestCase):

  def tearDown(self):
    varCollection = VariableCollection()
    varCollection.PushVariable("x",self.polys,self.quad)
    varCollection.PushVariable("y",self.polys,self.quad)
    rawFn = SinModNoDeriv(2)
    fn = libmuqModelling.CachedModPiece(rawFn);
  
    factory = SmolyakPCEFactory(varCollection,fn)
    pce = factory.StartAdaptiveToTolerance(3,1e-9)
  
    testPt = [0.1,0.4]
    trueOutput = rawFn.Evaluate(testPt)
    pceOutput = pce.Evaluate(testPt)
    self.assertAlmostEqual(trueOutput[0], pceOutput[0], 5)
    self.assertAlmostEqual(trueOutput[1], pceOutput[1], 5)
    
  def testLegendrePce1(self):
    self.quad = GaussPattersonQuadrature1D()
    self.polys = LegendrePolynomials1DRecursive()
  
  def testLegendrePce2(self):
    self.quad = GaussLegendreQuadrature1D()
    self.polys = LegendrePolynomials1DRecursive()



        
    
