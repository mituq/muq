#include "MUQ/Approximation/python/PolynomialRegressorPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Approximation;

// python constructor
PolynomialRegressor::PolynomialRegressor(unsigned int const inDim, unsigned int const outDim, py::dict const& para) :
  PolynomialRegressor(inDim, outDim, PythonDictToPtree(para)) {}

py::list PolynomialRegressor::PyCrossValidationEvaluate(py::list const& inputs) const
{
  return GetPythonMatrix<double>(AllCrossValidationEvaluate(GetEigenVector<Eigen::VectorXd>(inputs)).transpose());
}

void muq::Approximation::ExportPolynomialRegressor() 
{
  py::class_<PolynomialRegressor, shared_ptr<PolynomialRegressor>,
	     py::bases<Regressor>, boost::noncopyable> exportPolyReg("PolynomialRegressor", py::init<unsigned int const, unsigned int const, py::dict const&>());

  exportPolyReg.def("NumPointsForInterpolation", &PolynomialRegressor::NumPointsForInterpolation);
  exportPolyReg.def("CubicWeightingOn", &PolynomialRegressor::CubicWeightingOn);
  exportPolyReg.def("CubicWeightingOff", &PolynomialRegressor::CubicWeightingOff);
  exportPolyReg.def("CrossValidationOn", &PolynomialRegressor::CrossValidationOn);
  exportPolyReg.def("CrossValidationOff", &PolynomialRegressor::CrossValidationOff);
  exportPolyReg.def("SetMaxCVSamples", &PolynomialRegressor::SetMaxCVSamples);
  exportPolyReg.def("CrossValidationEvaluate", &PolynomialRegressor::PyCrossValidationEvaluate);

  // convert to parents
  py::implicitly_convertible<shared_ptr<PolynomialRegressor>, shared_ptr<Regressor> >();
}
