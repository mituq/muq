#include "MUQ/Approximation/Incremental/PolynomialApproximator.h"

#include <boost/math/special_functions.hpp>

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

#include "MUQ/Approximation/Incremental/SpaceFillOptimization.h"
#include "MUQ/Approximation/BallConstraint.h"
#include "MUQ/Utilities/HDF5Logging.h"


using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Optimization;
using namespace muq::Approximation;

PolynomialApproximator::PolynomialApproximator(shared_ptr<ModPiece> const& fnIn,
                                               boost::property_tree::ptree para) : Approximator(fnIn, para)
{
  // the approximated function must only have one input
  assert(fn->inputSizes.size() == 1);

  // must be at most quadratic polynomials
  assert(para.get<int>("PolynomialRegressor.Order", 0) < 3);

  // make the regressor
  regressor = make_shared<PolynomialRegressor>(fn->inputSizes(0), fn->outputSize, para);

  // number of nearest neighbors to use - default is the number for interpolation, padded by sqrt(d)
  kN =
    ReadAndLogParameter<int>(para, "PolynomialApproximator.kN",
                             static_cast<int>(regressor->NumPointsForInterpolation() *
                                              sqrt(static_cast<int>(fnIn->inputSizes(0)))));

  // the number of nearest neighbors must at least interpolate
  if (kN < regressor->NumPointsForInterpolation()) {
    cerr << endl << endl << "ERROR: Regressor requires " << regressor->NumPointsForInterpolation() << " but only " <<
      kN << " nearest neighbors are used for local regression." << endl << endl;
    assert(kN >= regressor->NumPointsForInterpolation());
  }

  // the upper bound on the (p+1) derivative of the approximated function
  derivativeBound = ReadAndLogParameter<double>(para, "PolynomialApproximator.DerivativeBound", 1.0);
    assert(derivativeBound > 0);

  // the variance of the function to be approximated
  fnVar = ReadAndLogParameter<double>(para, "PolynomialApproximator.FunctionVariance", 0.0);
}

void PolynomialApproximator::ComputeRegression(Eigen::VectorXd const& point)
{
  // don't recompute if the point is the current center and the cache is up-to-date
  if (usedCurrentCache && MatrixEqual(point, center)) {
    return;
  }

  // inputs and outputs of nearest neighbors
  Eigen::MatrixXd inputs;
  Eigen::MatrixXd outputs;

  // find the nearest neighbors
  NearestNeighbors(inputs, outputs, point);

  // fit the regressor
  regressor->Fit(inputs, outputs, point);

  // the regression used the most up-to-date cache
  usedCurrentCache = true;

  // set the center
  center = point;
}

Eigen::VectorXd PolynomialApproximator::EvaluateImpl(Eigen::VectorXd const& input)
{
  // do the local regression
  ComputeRegression(input);

  //return the regression nomial evaluation
  return regressor->Evaluate(input);
}

Eigen::MatrixXd PolynomialApproximator::JacobianImpl(Eigen::VectorXd const& input)
{
  // do the local regression
  ComputeRegression(input);

  //return the regression nomial jacobian
  return regressor->Jacobian(input);
}

bool PolynomialApproximator::ResetNumNearestNeighbors(unsigned int const newKN)
{
  assert(newKN > 0);

  kN = newKN;

  assert(kN >= regressor->NumPointsForInterpolation());

  return (fn->GetNumOfEvals() >= kN);
}

unsigned int PolynomialApproximator::NumNearestNeighbors() const
{
  return kN;
}

unsigned int PolynomialApproximator::NumAvailableNearestNeighbors() const
{
  return fn->GetNumOfEvals();
}

double PolynomialApproximator::LocalRadius(Eigen::VectorXd const& point) const
{
  // find the nearest neighbors
  Eigen::MatrixXd inputs;
  Eigen::MatrixXd outputs; // these aren't acutally necessary

  NearestNeighbors(inputs, outputs, point);

  return (inputs.colwise() - point).colwise().norm().maxCoeff();
}

double PolynomialApproximator::ErrorIndicator(Eigen::VectorXd const& point) const
{
  // find the nearest neighbors
  Eigen::MatrixXd inputs;
  Eigen::MatrixXd outputs; // these aren't acutally necessary

  NearestNeighbors(inputs, outputs, point);

  // order of the regression
  const unsigned int p = regressor->RegressionOrder();
  assert(p == 0 || p == 1 || p == 2); // constant, linear, or quadratic

  // compute the raidus of the ball
  const double radius = (inputs.colwise() - point).colwise().norm().maxCoeff();

  // compute the lambda poisedness constant
  const double lambda = regressor->PoisednessConstant(inputs, point, optOptions);

  // compute the contribution of the noisy error
  const double noisyErr = regressor->NoisyError(inputs, point, fnVar);

  // return the error indicator
  return noisyErr + sqrt(kN) * radius * lambda * derivativeBound /  boost::math::factorial<double>(p + 1);
}

int PolynomialApproximator::GetNumberOfCrossValidationFits() const
{
  return regressor->GetNumberOfCVFits();
}

void PolynomialApproximatorWithModes::SetMode(Modes newMode)
{
  mode = newMode;
  InvalidateCache();
}

///Select which index is desired, and set to crossValidation mode.
void PolynomialApproximatorWithModes::SetCrossValidationIndex(int const i)
{
  assert(i >= 0);
  assert(i < GetNumberOfCrossValidationFits());
  cvIndex = i;
  mode    = crossValidation;
  InvalidateCache();
}

Eigen::VectorXd PolynomialApproximatorWithModes::EvaluateImpl(Eigen::VectorXd const& input)
{
  switch (mode) {
  case standard:
    // do the local regression
    ComputeRegression(input);

    //return the regression nomial evaluation
    return regressor->Evaluate(input);

    break;

  case crossValidation:
    // do the local regression
    ComputeRegression(input);
    //return the cv evaluation
    return regressor->OneCrossValidationEvaluate(input, cvIndex);


    break;

  case trueDirect:
    //return the actual evaluation
    return fn->Evaluate(input);

    break;
  }

  assert(false); //shouldn't get here
  return Eigen::VectorXd();
}

Eigen::MatrixXd PolynomialApproximatorWithModes::JacobianImpl(Eigen::VectorXd const& input)
{
  switch (mode) {
  case standard:
    // do the local regression
    ComputeRegression(input);

    //return the regression nomial jacobian
    return regressor->Jacobian(input);

    break;

  case crossValidation:
    // do the local regression

    ComputeRegression(input);

    //return the CV jacobian
    return regressor->OneCrossValidationJacobian(input, cvIndex);

    break;

  case trueDirect:
    //return the actual Jacobian
    return fn->Jacobian(input);

    break;
  }

  assert(false); //shouldn't get here
  return Eigen::MatrixXd();
}
