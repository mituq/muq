
add_subdirectory(src)

IF(UtilitiesAndModelling_tests)
  add_subdirectory(test)
  #pass the test sources to the higher level
  set(all_gtest_sources ${all_gtest_sources} ${Test_GeneralUtility_Sources} PARENT_SCOPE)
ENDIF(UtilitiesAndModelling_tests)  