
#include <iostream>

#include "gtest/gtest.h"

#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"
#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"

#include "MUQ/Utilities/EigenUtils.h"

using namespace std;

using namespace Eigen;
using namespace muq::Utilities;

TEST(UtilitiesMultiIndexSet, BasicAddActive)
{
  const int dim = 10;
  Eigen::RowVectorXu base = Eigen::RowVectorXu::Zero(dim);
  
  auto set = make_shared<MultiIndexSet>(dim);
  
  int newInd = set->AddActive(base);
  EXPECT_EQ(0,newInd);
  EXPECT_EQ(1,set->size());
  
  base(0)++;
  newInd = set->AddActive(base);
  EXPECT_EQ(1,newInd);
  EXPECT_EQ(2,set->size());
}

TEST(UtilitiesMultiIndexSet, CombinedPool)
{
  const int dim = 10;
  Eigen::RowVectorXu base = Eigen::RowVectorXu::Zero(dim);
  
  shared_ptr<MultiIndexPool> jointPool = make_shared<MultiIndexPool>();
  auto setA = make_shared<MultiIndexSet>(dim,make_shared<NoLimiter>(),jointPool);
  auto setB = make_shared<MultiIndexSet>(dim,make_shared<NoLimiter>(),jointPool);
 
  int newInd = setA->AddActive(base);
  EXPECT_EQ(0,newInd);
  EXPECT_EQ(1,setA->size());
  
  newInd = setB->AddActive(base);
  EXPECT_EQ(0,newInd);
  EXPECT_EQ(1,setB->size());
  
  base(0) = 1;
  newInd = setB->AddActive(base);
  EXPECT_EQ(1,newInd);
  EXPECT_EQ(2,setB->size());
  
  base(0) = 0;
  base(1) = 1;
  newInd = setA->AddActive(base);
  EXPECT_EQ(1,newInd);
  EXPECT_EQ(2,setA->size());
  
  Eigen::RowVectorXu tempMulti1 = setB->IndexToMulti(0);
  for(int d=0; d<dim; ++d)
    EXPECT_EQ(0,tempMulti1(d));
  
  Eigen::RowVectorXu tempMulti2 = setB->IndexToMulti(1);
  EXPECT_EQ(1,tempMulti2(0));
  for(int d=1; d<dim; ++d)
    EXPECT_EQ(0,tempMulti2(d));
  
  Eigen::RowVectorXu tempMulti3 = setA->IndexToMulti(0);
  for(int d=0; d<dim; ++d)
    EXPECT_EQ(0,tempMulti3(d));
  
  Eigen::RowVectorXu tempMulti4 = setA->IndexToMulti(1);
  EXPECT_EQ(0,tempMulti4(0));
  EXPECT_EQ(1,tempMulti4(1));
  for(int d=2; d<dim; ++d)
    EXPECT_EQ(0,tempMulti4(d));
  
}

///Test the first, last, and a couple of middle multi-indices from the full tensor family.
TEST(UtilitiesMultiIndexSet, FullTensor)
{
  auto indexSet = MultiIndexFactory::CreateFullTensor(5, 5);

  RowVectorXu index0    = indexSet->IndexToMulti(0);
  RowVectorXu index28   = indexSet->IndexToMulti(28);
  RowVectorXu index837  = indexSet->IndexToMulti(837);
  RowVectorXu indexLast = indexSet->IndexToMulti(7775);

  RowVectorXu index0Correct(5);

  index0Correct << 0, 0, 0, 0, 0;
  RowVectorXu index28Correct(5);
  index28Correct << 0, 0, 0, 4, 4;
  RowVectorXu index837Correct(5);
  index837Correct << 0, 3, 5, 1, 3;
  RowVectorXu indexLastCorrect(5);
  indexLastCorrect << 5, 5, 5, 5, 5;

  EXPECT_TRUE(MatrixEqual(index0, index0Correct));
  EXPECT_TRUE(MatrixEqual(index28, index28Correct));
  EXPECT_TRUE(MatrixEqual(index837, index837Correct));
  EXPECT_TRUE(MatrixEqual(indexLast, indexLastCorrect));

  EXPECT_EQ(837u, indexSet->MultiToIndex(index837Correct));
}

///Test the first, last, and a couple of middle multi-indices from the total order family.
TEST(UtilitiesMultiIndexSet, TotalOrder)
{
  //Test the number of a larger family
  auto indexSet = MultiIndexFactory::CreateTotalOrder(7, 6);

  EXPECT_EQ(1716u, indexSet->GetNumberOfIndices());

  RowVectorXu index0    = indexSet->IndexToMulti(0);
  RowVectorXu index49   = indexSet->IndexToMulti(49);
  RowVectorXu index1647 = indexSet->IndexToMulti(1647);
  RowVectorXu indexLast = indexSet->IndexToMulti(1715);

  RowVectorXu index0Correct(7);
  index0Correct << 0, 0, 0, 0, 0, 0, 0;
  RowVectorXu index49Correct(7);
  index49Correct << 0, 0, 0, 0, 2, 0, 0;

  RowVectorXu index1647Correct(7);
  index1647Correct << 3, 0, 2, 0, 0, 0, 1;

  RowVectorXu indexLastCorrect(7);
  indexLastCorrect << 6, 0, 0, 0, 0, 0, 0;


  EXPECT_TRUE(MatrixEqual(index0, index0Correct));
  EXPECT_TRUE(MatrixEqual(index49, index49Correct));
  EXPECT_TRUE(MatrixEqual(index1647, index1647Correct));
  EXPECT_TRUE(MatrixEqual(indexLast, indexLastCorrect));

  EXPECT_EQ(1647u, indexSet->MultiToIndex(index1647));
}


TEST(UtilitiesMultiIndexSet, Serialization)
{
  string filename = "results/tests/MultiIndexArchive.txt";
  
  Eigen::MatrixXu oldMultis;
  {
    shared_ptr<MultiIndexSet> indexSet = MultiIndexFactory::CreateTotalOrder(4, 6);
    oldMultis = indexSet->GetAllMultiIndices();
    
    ofstream ofs(filename.c_str());
    boost::archive::text_oarchive oa(ofs);
    oa << indexSet;
  }

  {
    shared_ptr<MultiIndexSet> indexSet2;
  
    std::ifstream ifs(filename.c_str());
    boost::archive::text_iarchive ia(ifs);
    
    ia >> indexSet2;
    EXPECT_TRUE(MatrixEqual(oldMultis, indexSet2->GetAllMultiIndices()));
  }
}


///Test the first, last, and a couple of middle multi-indices from the total order family.
TEST(UtilitiesMultiIndexSet, TotalOrderWithMin)
{
  //Test the number of a larger family
  auto indexSet = MultiIndexFactory::CreateTotalOrder(7, 8, 4);

  EXPECT_EQ(6315u, indexSet->GetNumberOfIndices());

  RowVectorXu index0    = indexSet->IndexToMulti(0);
  RowVectorXu index53   = indexSet->IndexToMulti(53);
  RowVectorXu index5461 = indexSet->IndexToMulti(5461);
  RowVectorXu indexLast = indexSet->IndexToMulti(6314);


  RowVectorXu index0Correct(7);
  index0Correct << 0, 0, 0, 0, 0, 0, 4;

  RowVectorXu index53Correct(7);
  index53Correct << 0, 0, 0, 0, 1, 3, 3;

  RowVectorXu index5461Correct(7);
  index5461Correct << 2, 3, 0, 1, 0, 0, 1;

  RowVectorXu indexLastCorrect(7);
  indexLastCorrect << 8, 0, 0, 0, 0, 0, 0;


  EXPECT_TRUE(MatrixEqual(index0, index0Correct));
  EXPECT_TRUE(MatrixEqual(index53, index53Correct));
  EXPECT_TRUE(MatrixEqual(index5461, index5461Correct));
  EXPECT_TRUE(MatrixEqual(indexLast, indexLastCorrect));

  EXPECT_EQ(5461u, indexSet->MultiToIndex(index5461Correct));
}


///Test the first, last, and a couple of middle multi-indices from the total order family, non-uniform.
TEST(UtilitiesMultiIndexSet, NonUniformTensor)
{
  //Test the number of a larger family
  RowVectorXu order(5);

  order << 1, 2, 4, 3, 4;
  auto indexSet = MultiIndexFactory::CreateFullTensor(order);
  
  EXPECT_EQ(600u, indexSet->GetNumberOfIndices());

  RowVectorXu index0    = indexSet->IndexToMulti(0);
  RowVectorXu index146  = indexSet->IndexToMulti(146);
  RowVectorXu index316  = indexSet->IndexToMulti(316);
  RowVectorXu indexLast = indexSet->IndexToMulti(599);

  RowVectorXu index0Correct(5);
  index0Correct << 0, 0, 0, 0, 0;
  RowVectorXu index146Correct(5);
  index146Correct << 0, 1, 2, 1, 1;

  RowVectorXu index316Correct(5);
  index316Correct << 1, 0, 0, 3, 1;

  RowVectorXu indexLastCorrect(5);
  indexLastCorrect << 1, 2, 4, 3, 4;


  EXPECT_TRUE(MatrixEqual(index0, index0Correct));
  EXPECT_TRUE(MatrixEqual(index146, index146Correct));
  EXPECT_TRUE(MatrixEqual(index316, index316Correct));
  EXPECT_TRUE(MatrixEqual(indexLast, indexLastCorrect));

  EXPECT_EQ(316u, indexSet->MultiToIndex(index316Correct));
}


///A simple test of a small adaptive multiIndex set
TEST(UtilitiesMultiIndexSet, AdaptiveActivate)
{
  auto adaptiveSet = MultiIndexFactory::CreateTotalOrder(2, 2);
  
  //add a few of the admissible ones to the active set
  RowVectorXu a(2);

  a << 1, 2;
  adaptiveSet->Activate(a);
  
  RowVectorXu b(2);
  b << 0, 3;
  adaptiveSet->Activate(b);
  
  RowVectorXu c(2);
  c << 2, 1;
  adaptiveSet->Activate(c);

  //check one that isn't supposed to work yet
  RowVectorXu d(2);
  d << 2, 3;
  EXPECT_FALSE(adaptiveSet->IsAdmissible(d));

}


///A simple test of a small adaptive multiIndex set
TEST(UtilitiesMultiIndexSet, AdaptiveExpand)
{
  auto adaptiveSet = MultiIndexFactory::CreateTotalOrder(2, 2);
  
  //add a few of the admissible ones to the active set
  RowVectorXu b(2);
  b << 0, 3;
  int newInd = adaptiveSet->AddActive(b);
  
  EXPECT_TRUE(adaptiveSet->IsExpandable(newInd));
  
  adaptiveSet->Expand(newInd);
  Eigen::MatrixXu results(8,2);
  results << 0, 0,
             0, 1,
             0, 2,
             1, 0,
             1, 1,
             2, 0,
             0, 3,
             0, 4;
  EXPECT_TRUE(MatrixEqual(results, adaptiveSet->GetAllMultiIndices()));

}

///A simple test of a small adaptive multiIndex set, testing out the initialization with a simplex.
TEST(UtilitiesMultiIndexSet, AdaptiveMultiIndexForcibleAdding)
{
  auto adaptiveSet = MultiIndexFactory::CreateTotalOrder(2, 1);

  RowVectorXu inadmissibleTerm(2);

  inadmissibleTerm << 2, 2;

  auto added = adaptiveSet->ForciblyActivate(inadmissibleTerm);

  //now expand that same term, which is definitely not expandable
  added = adaptiveSet->ForciblyExpand(adaptiveSet->MultiToIndex(inadmissibleTerm));

  MatrixXu result(15, 2);
  result << 0, 0,
0, 1,
1, 0,
2, 2,
1, 2,
1, 1,
0, 2,
2, 1,
2, 0,
3, 2,
3, 1,
3, 0,
2, 3,
1, 3,
0, 3;

  EXPECT_TRUE(MatrixEqual(result, adaptiveSet->GetAllMultiIndices()));
}
// 
// TEST(UtilitiesMultiIndexFamily, ExpMultiIndexFamily)
// {
//   ExpLimitedMultiIndexFamily::Ptr indexFamily = ExpLimitedMultiIndexFamily::Create(4, 6, 0);
// 
// 
//   EXPECT_EQ(1520u, indexFamily->GetNumberOfIndices());
// 
//   //            cout << "num: " << indexFamily->GetNumberOfIndices() << endl;
//   //            for(unsigned int i =0; i<indexFamily->GetNumberOfIndices(); i++)
//   //                    cout << indexFamily->IndexToMulti(i) << endl;
// 
// 
//   RowVectorXu index0    = indexFamily->IndexToMulti(0);
//   RowVectorXu index356  = indexFamily->IndexToMulti(356);
//   RowVectorXu index974  = indexFamily->IndexToMulti(974);
//   RowVectorXu indexLast = indexFamily->IndexToMulti(1519);
// 
//   RowVectorXu index0Correct(4);
//   index0Correct <<   0, 0, 0, 0;
//   RowVectorXu index356Correct(4);
//   index356Correct <<  0,  1, 20,  0;
//   RowVectorXu index974Correct(4);
//   index974Correct << 2,  0,  0, 14;
//   RowVectorXu indexLastCorrect(4);
//   indexLastCorrect <<  63,  0,  0,  0;
// 
//   EXPECT_TRUE(MatrixEqual(index0, index0Correct));
//   EXPECT_TRUE(MatrixEqual(index356, index356Correct));
//   EXPECT_TRUE(MatrixEqual(index974, index974Correct));
//   EXPECT_TRUE(MatrixEqual(indexLast, indexLastCorrect));
// 
//   EXPECT_EQ(974u, indexFamily->MultiToIndex(index974Correct));
// }
// 
// TEST(UtilitiesMultiIndexFamily, UnstructuredMultiIndexFamily)
// {
//   UnstructuredMultiIndexFamily::Ptr indexFamily = UnstructuredMultiIndexFamily::Create(4, 2);
// 
//   RowVectorXu index1(4);
//   RowVectorXu index2(4);
//   RowVectorXu index3(4);
// 
//   index1 << 1, 2, 3, 4;
//   index2 << 2, 2, 3, 4;
//   index3 << 2, 2, 2, 2;
// 
//   indexFamily->AddMultiIndex(index1);
// 
//   EXPECT_TRUE(MatrixEqual(indexFamily->IndexToMulti(0), index3));
//   EXPECT_FALSE(indexFamily->IsMultiInFamily(index2));
//   EXPECT_EQ(1u, indexFamily->MultiToIndex(index1));
// }
