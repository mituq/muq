import sys
import unittest
import math

from libmuqUtilities import *

class QuadratureTest(unittest.TestCase):
  def testLegendreQuad(self):
    tol = 1e-10
    
    trueWs = [0.066671344308688, 0.14945134915058, 0.21908636251598, 0.26926671931, 0.29552422471475, 0.29552422471475, 0.26926671931, 0.21908636251598, 0.14945134915058, 0.066671344308688]
    trueNodes = [-0.97390652851717, -0.86506336668898, -0.67940956829902, -0.43339539412925, -0.14887433898163, 0.14887433898163, 0.43339539412925, 0.67940956829902, 0.86506336668898, 0.97390652851717]
  
    size = 10
    quad = GaussLegendreQuadrature1D()
    nodes = quad.GetNodes(size)
    weights = quad.GetWeights(size)
    for i in range(size):
      self.assertTrue(math.fabs(trueWs[i]-weights[i])<tol)
      self.assertTrue(math.fabs(trueNodes[i]-nodes[i])<tol)

  def testHermiteQuad(self):
    tol = 1e-10
    
    trueWs = [2.6585516843563e-07, 8.5736870435878e-05, 0.0039053905846291, 0.051607985615884, 0.26049231026416, 0.57013523626248, 0.57013523626248, 0.26049231026416, 0.051607985615884, 0.0039053905846291, 8.5736870435879e-05, 2.6585516843563e-07]
    
    trueNodes = [-3.8897248978698, -3.0206370251209, -2.2795070805011, -1.5976826351526, -0.94778839124016, -0.31424037625436, 0.31424037625436, 0.94778839124016, 1.5976826351526, 2.2795070805011, 3.0206370251209, 3.8897248978698]

    size = 12
    
    quad = GaussHermiteQuadrature1D()
    nodes = quad.GetNodes(size)
    weights = quad.GetWeights(size)
    for i in range(size):
      self.assertTrue(math.fabs(trueWs[i]-weights[i])<tol)
      self.assertTrue(math.fabs(trueNodes[i]-nodes[i])<tol)
