
#include "gtest/gtest.h"

#include <iostream>
#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/Utilities/EigenUtils.h"

using namespace std;
using namespace Eigen;

TEST(Utilities, SimpleFileIO)
{
  
  string   fullString = "results/tests/FileIO.dat";
  
  
  int trueInteger = 123;
  int recoveredInt;
  // save data to archive
  {
    ofstream ofs(fullString.c_str());
    EXPECT_TRUE(ofs.is_open());
    
    ofs << trueInteger;
    ofs.close();
  }
  
  // load data from archive
  {
    ifstream ifs(fullString.c_str());
    EXPECT_TRUE(ifs.is_open());
    ifs >> recoveredInt;
    ifs.close();
  }
  
  EXPECT_EQ(trueInteger,recoveredInt);
  
}

TEST(Utilities, EigenSerializeDouble)
{
  MatrixXd randomMat = MatrixXd::Random(5, 10);

  string   fullString = "results/tests/EigenSerializeTest.dat";
  ofstream ofs(fullString.c_str());

  // save data to archive
  {
    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << randomMat;

    // archive and stream closed when destructors are called
  }


  MatrixXd recoveredMat;
  ifstream ifs(fullString.c_str());

  // load data to archive
  {
    boost::archive::text_iarchive oa(ifs);

    // write class instance to archive
    oa >> recoveredMat;

    // archive and stream closed when destructors are called
  }

  EXPECT_TRUE(muq::Utilities::MatrixApproxEqual(randomMat, recoveredMat, 1e-14));
}

TEST(Utilities, EigenSerializeUnsigned)
{
  MatrixXu testMat(3, 2);

  testMat << 1, 2, 3, 4, 5, 6;

  string   fullString = "results/tests/EigenSerializeUTest.dat";
  ofstream ofs(fullString.c_str());

  // save data to archive
  {
    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << testMat;

    // archive and stream closed when destructors are called
  }


  MatrixXu recoveredMat;
  ifstream ifs(fullString.c_str());

  // load data to archive
  {
    boost::archive::text_iarchive oa(ifs);

    // write class instance to archive
    oa >> recoveredMat;

    // archive and stream closed when destructors are called
  }
  EXPECT_TRUE(muq::Utilities::MatrixEqual(testMat, recoveredMat));
}
