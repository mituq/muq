
#include <gtest/gtest.h>
#include <iostream>
#include <cmath>

#include <Eigen/Core>

void SpaceFillSequence(std::vector<unsigned int>& pts, unsigned int skip, unsigned int max)
{
  if (skip != 0) {
    unsigned int size = pts.size();
    for (unsigned int i = 0; i < size; ++i) {
      unsigned int newpt = pts[i] + skip;
      if (newpt < max) {
        pts.push_back(newpt);
      }
    }
    SpaceFillSequence(pts, floor(skip / 2), max);
  }
}

void BuildMatParts(const Eigen::MatrixXd& InputMat, Eigen::MatrixXd& u, Eigen::MatrixXd& v)
{
  // figure out the size of this block
  unsigned int ncols = InputMat.cols();
  unsigned int nrows = InputMat.rows();

  unsigned int maxK = std::min(ncols, nrows);
  std::vector<unsigned int> allIs(1, 0);

    SpaceFillSequence(allIs, maxK - 1, maxK);
  maxK = std::min(int(allIs.size()), int(maxK));

  // build a space filling sequence of is

  v = Eigen::MatrixXd::Zero(ncols, maxK);
  u = Eigen::MatrixXd::Zero(nrows, maxK);

  // first, grab a row to use an an unscaled v
  for (unsigned int c = 0; c < ncols; ++c) {
    v(c, 0) = InputMat(0, c);
  }


  // now, grab the column corresponding to the absolute maximum entry of v
  Eigen::VectorXd::Index maxInd;
  v.col(0).cwiseAbs().maxCoeff(&maxInd);
  unsigned int j = int(maxInd);

  // scale the v vector
  double gamma = 1.0 / v(0, j);
  v.col(0) *= gamma;

  // grab the column of the covariance
  for (unsigned int r = 0; r < nrows; ++r) {
    u(r, 0) = InputMat(r, j);
  }

  u.col(0).cwiseAbs().maxCoeff(&maxInd);
  unsigned int i = 1; //int(maxInd);


  //double approxNorm = 0;

  // maximum rank
  unsigned int k;
  for (k = 1; k < maxK; ++k) {
    i = allIs[k];

    // first, grab a row of the residual to use an an unscaled v
    for (unsigned int c = 0; c < ncols; ++c) {
      double predict = 0;

      for (unsigned int l = 0; l < k; ++l) {
        predict += u(i, l) * v(c, l);
      }

      v(c, k) = InputMat(i, c) - predict;
    }


    // now, grab the column corresponding to the absolute maximum entry of v
    v.col(k).cwiseAbs().maxCoeff(&maxInd);
    j = int(maxInd);

    // scale the v vector
    if (v.col(k).norm() < 1e-3) {
      break;
    }

    double gamma = 1.0 / v(j, k);
    v.col(k) *= gamma;

    // grab a column of the residual
    for (unsigned int r = 0; r < nrows; ++r) {
      double predict = 0;
      for (unsigned int l = 0; l < k; ++l) {
        predict += u(r, l) * v(j, l);
      }

      u(r, k) = InputMat(r, j) - predict;
    }
  }

  u = u.topLeftCorner(nrows, k).eval();
  v = v.topLeftCorner(nrows, k).eval();
}

TEST(UtilitiesMatApproxTest, CovMat)
{
  int N               = pow(2, 10);
  Eigen::MatrixXd Mat = Eigen::MatrixXd::Identity(N, N);

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      Mat(i, j) = exp(-1.0 * fabs(i - j) / (8 * N));
    }
  }


  Eigen::MatrixXd u, v;
  BuildMatParts(Mat, u, v);

  EXPECT_LT((Mat - u * v.transpose()).norm(), 1e-1);

  //std::cout << Mat-u*v.transpose() << std::endl;
}
