
#include "MUQ/Utilities/EigenUtils.h"

#include <functional>
#include <iostream>

#include <boost/filesystem.hpp>
#include <Eigen/Core>

#include "MUQ/Utilities/LogConfig.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;

Eigen::VectorXu muq::Utilities::FindNonZeroInVector(Eigen::VectorXd const& input)
{
  unsigned int nonZeroCount = 0;

  for (unsigned int i = 0; i < input.rows(); ++i) {
    if (input(i) != 0) {
      ++nonZeroCount;
    }
  }

  VectorXu result(nonZeroCount);

  unsigned int j = 0;
  for (unsigned int i = 0; i < input.rows(); ++i) {
    if (input(i) != 0) {
      result(j) = i;
      ++j;
    }
  }

  return result;
}

Eigen::MatrixXd muq::Utilities::ApplyVectorFunction(std::function<Eigen::VectorXd(
                                                                           Eigen::VectorXd const&)> fn, unsigned const outputDim,
                                                           Eigen::MatrixXd const& inputs)
{
  MatrixXd result(outputDim, inputs.cols()); //set up the return matrix

  for (unsigned i = 0; i < inputs.cols(); ++i) {
    result.col(i) = fn(inputs.col(i));       //computing and equating checks the output dimensionality
  }

  return result;
}

Eigen::MatrixXd muq::Utilities::ApplyExecutableFunction(std::string const    & executable,
                                                               unsigned const         outputDim,
                                                               Eigen::MatrixXd const& inputs)
{
  //construct the paths
  auto pointFile  = boost::filesystem::unique_path("/tmp/polychaosPoints-%%%%-%%%%-%%%%-%%%%");
  auto resultFile = boost::filesystem::unique_path("/tmp/polychaosResults-%%%%-%%%%-%%%%-%%%%");


  ofstream ofs(pointFile.c_str());

  assert(ofs.good());
  for (unsigned i = 0; i < inputs.rows(); ++i) {
    for (unsigned j = 0; j < inputs.cols(); ++j) {
      ofs.precision(15);
      ofs << inputs(i, j) << " ";
    }
    ofs << endl;
  }


  string command = executable + " " + pointFile.native() + " " + resultFile.native();
  LOG(INFO) << "Running external command: " << command;
  assert(!system(command.c_str()));

  ifstream ifs(resultFile.c_str());
  assert(ifs.good());
  string aNumber;

  MatrixXd result(outputDim, inputs.cols());

  for (unsigned int i = 0; i < outputDim; ++i) {
    for (unsigned j = 0; j < inputs.cols(); ++j) {
      ifs >> result(i, j);
    }
  }


  //clean up!
  boost::filesystem::remove(pointFile);
  boost::filesystem::remove(resultFile);

  return result;
}

