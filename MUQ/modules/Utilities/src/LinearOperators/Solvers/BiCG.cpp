
#include "MUQ/Utilities/LinearOperators/Solvers/BiCG.h"

// standard library includes
#include <memory>
#include <iostream>

// other muq includes
#include "MUQ/Utilities/LinearOperators/Operators/LinOpBase.h"
#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Utilities;
using namespace std;

Eigen::VectorXd BiCG::solve(const Eigen::VectorXd& b)
{
  // make sure b is the correct size
  assert(A->cols() == b.rows());
  assert(A->rows() == A->cols());

  // current solution
  Eigen::VectorXd xc = Eigen::VectorXd::Zero(b.rows());

  // current residual
  Eigen::VectorXd rc = b;

  double rSquaredNorm = rc.squaredNorm();
  double bNorm = sqrt(rSquaredNorm);
  double error = 1.0;
  double resid;
  
  double omega = 1.0;
  Eigen::VectorXd rTilde = rc;
  Eigen::VectorXd p, v, s, t;
  
  const unsigned int maxIts = b.rows();
  double beta, rho, rhoOld, alpha;
  for (unsigned int i = 0; i < maxIts; ++i) {
	  
	  rho = rTilde.dot(rc);
	  if(rho==0)
		  break;
	  
	  if(i>0){
		  beta = (rho/rhoOld)*(alpha/omega);
		  p = rc + beta*(p-omega*v);
	  }else{
	  	  p = rc;
	  }
	  
	  v = A->apply(p);
	  alpha = rho/rTilde.dot(v);
	  s = rc - alpha*v;
	  
	  // check for early convergence
	  if(s.norm()<tol){
		  xc += alpha*p;
		  break;
	  }
	  
	  // stabilization
	  t = A->apply(s);
	  omega = t.dot(s)/t.squaredNorm();
	  
	  // update the solution
	  xc += alpha*p + omega*s;
	  
	  rc = s-omega*t;
	  error = rc.norm()/bNorm;
	  
	  // check for convergence
	  if(error<tol)
		  break;
	  
	  if(abs(omega)<1e-13)
		  break;
	  
	  rhoOld = rho;
  }

  return xc;
}

