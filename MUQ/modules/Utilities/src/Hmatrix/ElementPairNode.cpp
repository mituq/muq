

#include "MUQ/Geostatistics/CovKernel.h"

#include "MUQ/Utilities/Hmatrix/ElementPairNode.h"
#include "MUQ/Utilities/Hmatrix/ElementNode.h"

#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>

#include <vector>
#include <algorithm>

using namespace muq::Utilities;
using namespace muq::Geostatistics;


/** Constructor for element pair node, recurses down */
template<unsigned int dim>
ElementPairNode<dim>::ElementPairNode(double                                  eta,
                                      std::shared_ptr < ElementTree < dim >>& Tree,
                                      std::shared_ptr < ElementNode < dim >>& Node1In,
                                      std::shared_ptr < ElementNode < dim >>& Node2In)
{
  // copy the nodes
  Node1 = Node1In;
  Node2 = Node2In;


  isLeaf = false;

  double diam1 = Tree->GetDiam(Node1);
  double diam2 = Tree->GetDiam(Node2);
  double dist  = Tree->GetDist(Node1, Node2);

  //std::cout << "min( " << diam1 << " , " << diam2 << " ) < eta * " << dist << " == ";
  Admiss = (std::min(diam1, diam2) < eta * dist);

  //std::cout << Admiss << std::endl;

  // recurse if possible
  if (Admiss) {
    child11 = std::shared_ptr<ElementPairNode>(nullptr);
    child12 = std::shared_ptr<ElementPairNode>(nullptr);
    child21 = std::shared_ptr<ElementPairNode>(nullptr);
    child22 = std::shared_ptr<ElementPairNode>(nullptr);

    isLeaf = true;
  } else {
    if ((Node1->child1.get() != nullptr) && (Node2->child1.get() != nullptr)) {
      child11 = std::shared_ptr<ElementPairNode>(new ElementPairNode(eta, Tree, Node1->child1, Node2->child1));
    } else {
      isLeaf = true;
    }

    if ((Node1->child1.get() != nullptr) && (Node2->child2.get() != nullptr)) {
      child12 = std::shared_ptr<ElementPairNode>(new ElementPairNode(eta, Tree, Node1->child1, Node2->child2));
    } else {
      isLeaf = true;
    }

    if ((Node1->child2.get() != nullptr) && (Node2->child1.get() != nullptr)) {
      child21 = std::shared_ptr<ElementPairNode>(new ElementPairNode(eta, Tree, Node1->child2, Node2->child1));
    } else {
      isLeaf = true;
    }

    if ((Node1->child2.get() != nullptr) && (Node2->child2.get() != nullptr)) {
      child22 = std::shared_ptr<ElementPairNode>(new ElementPairNode(eta, Tree, Node1->child2, Node2->child2));
    } else {
      isLeaf = true;
    }
  }
}

void SpaceFillSequence(std::vector<unsigned int>& pts, unsigned int skip, unsigned int max)
{
  if (skip != 0) {
    unsigned int size = pts.size();
    for (unsigned int i = 0; i < size; ++i) {
      unsigned int newpt = pts[i] + skip;
      if (newpt < max) {
        pts.push_back(newpt);
      }
    }
    SpaceFillSequence(pts, floor(skip / 2), max);
  }
}

template<unsigned int dim>
void ElementPairNode<dim>::RecurseMatConstruct(std::shared_ptr < ElementTree < dim >>& Tree,
                                               muq::Geostatistics::CovKernelPtr           & CovKernel)
{
  if (Admiss) {
    //std::cout << "rank( " << Node1->StartInd+1 << ":" << Node1->EndInd << " , " << Node2->StartInd+1 << ":" <<
    // Node2->EndInd << " ) = ";

    // if we are admissable, then we can build a low rank approximation to this block of the matrix
    BuildMatParts(Tree, CovKernel);
  } else {
    // if we are not admissable then we should keep recursing unless this is a leaf, then we need to build a dense
    // matrix
    if (isLeaf) {
      // figure out the size of this block
      unsigned int nrows = (Node1->EndInd - Node1->StartInd);
      unsigned int ncols = (Node2->EndInd - Node2->StartInd);

      //std::cout << "rank( " << Node1->StartInd+1 << ":" << Node1->EndInd << " , " << Node2->StartInd+1 << ":" <<
      // Node2->EndInd << " ) = " << std::min(nrows,ncols) << ";\n";

      // allocate the dense matrix to hold this block
      DenseCovPart = Eigen::MatrixXd::Zero(nrows, ncols);

      // fill in the block using the covariance kernel
      for (unsigned int i = 0; i < nrows; ++i) {
        for (unsigned int j = 0; j < ncols; ++j) {
          DenseCovPart(i, j) = CovKernel->Kernel(Tree->ElePos.col(
                                                   Tree->Elements[Node1->StartInd + i]),
                                                 Tree->ElePos.col(Tree->Elements[Node2->StartInd + j]));
        }
      }
    } else {
      // THIS IS NOT A LEAF! Keep going!
      child11->RecurseMatConstruct(Tree, CovKernel);
      child12->RecurseMatConstruct(Tree, CovKernel);
      child21->RecurseMatConstruct(Tree, CovKernel);
      child22->RecurseMatConstruct(Tree, CovKernel);
    }
  }
}

template<unsigned int dim>
void ElementPairNode<dim>::RecurseMatMult(std::shared_ptr < ElementTree < dim >>& ClusterTree,
                                          const Eigen::VectorXd                 & x,
                                          Eigen::VectorXd                       & b) const
{
  // if this is an admissable node, use the low rank approximation
  if (Admiss) {
    unsigned int ColStart = Node2->StartInd;
    unsigned int ColEnd   = Node2->EndInd;
    unsigned int RowStart = Node1->StartInd;
    unsigned int RowEnd   = Node1->EndInd;

    unsigned int size1 = ColEnd - ColStart;
    unsigned int size2 = RowEnd - RowStart;

    // copy the relevant part of x into a new vector
    Eigen::VectorXd tempx(size1);

    for (unsigned int c = 0; c < size1; ++c) {
      tempx[c] = x[ClusterTree->Elements[ColStart + c]];
    }

    // add the result back to b
    Eigen::VectorXd tempb = u * v.transpose() * tempx;
    for (unsigned int r = 0; r < size2; ++r) {
      b[ClusterTree->Elements[RowStart + r]] += tempb[r];
    }
  } else { // this is not an admissable node, so we need to use the dense matrix
           // if we are not admissable then we should keep recursing unless this is a leaf, then we need to build a
           // dense
           // matrix
    if (isLeaf) {
      unsigned int ColStart = Node2->StartInd;
      unsigned int ColEnd   = Node2->EndInd;
      unsigned int RowStart = Node1->StartInd;
      unsigned int RowEnd   = Node1->EndInd;

      unsigned int size1 = ColEnd - ColStart;

      unsigned int size2 = RowEnd - RowStart;

      // copy the relevant part of x into a new vector
      Eigen::VectorXd tempx(size1);
      for (unsigned int c = 0; c < size1; ++c) {
        tempx[c] = x[ClusterTree->Elements[ColStart + c]];
      }

      // add the result back to b
      Eigen::VectorXd tempb = DenseCovPart * tempx;
      for (unsigned int r = 0; r < size2; ++r) {
        b[ClusterTree->Elements[RowStart + r]] += tempb[r];
      }
    } else {
      // THIS IS NOT A LEAF! Keep going!
      child11->RecurseMatMult(ClusterTree, x, b);
      child12->RecurseMatMult(ClusterTree, x, b);
      child21->RecurseMatMult(ClusterTree, x, b);
      child22->RecurseMatMult(ClusterTree, x, b);
    }
  }
}

template<unsigned int dim>
void ElementPairNode<dim>::BuildMatParts(std::shared_ptr < ElementTree < dim >>& Tree,
                                         muq::Geostatistics::CovKernelPtr           & CovKernel)
{
  // figure out the size of this block
  unsigned int nrows = (Node1->EndInd - Node1->StartInd);
  unsigned int ncols = (Node2->EndInd - Node2->StartInd);

  unsigned int maxK = std::min(nrows, ncols);

  v = Eigen::MatrixXd::Zero(ncols, maxK);
  u = Eigen::MatrixXd::Zero(nrows, maxK);

  std::vector<unsigned int> allIs(1, 0);
  SpaceFillSequence(allIs, maxK - 1, maxK);
  maxK = std::min(int(allIs.size()), int(maxK));

  // first, grab a row to use an an unscaled v
  for (unsigned int c = 0; c < ncols; ++c) {
    v(c,
      0) =
      CovKernel->Kernel(Tree->ElePos.col(Tree->Elements[Node1->StartInd]),
                        Tree->ElePos.col(Tree->Elements[Node2->StartInd + c]));
  }

  // now, grab the column corresponding to the absolute maximum entry of v
  Eigen::VectorXd::Index maxInd;
  v.col(0).cwiseAbs().maxCoeff(&maxInd);
  unsigned int j(maxInd);

  // scale the v vector
  double gamma = 1.0 / v(j, 0);
  v.col(0) *= gamma;

  // grab the column of the covariance
  for (unsigned int r = 0; r < nrows; ++r) {
    u(r,
      0) =
      CovKernel->Kernel(Tree->ElePos.col(Tree->Elements[Node1->StartInd + r]),
                        Tree->ElePos.col(Tree->Elements[Node2->StartInd + j]));
  }

  unsigned int i = 1; //int(maxInd);

  // maximum rank
  unsigned int k;
  for (k = 1; k < maxK; ++k) {
    i = allIs[k];

    // first, grab a row of the residual to use an an unscaled v
    for (unsigned int c = 0; c < ncols; ++c) {
      double predict = 0;
      for (unsigned int l = 0; l < k; ++l) {
        predict += u(i, l) * v(c, l);
      }

      v(c,
        k) =
        CovKernel->Kernel(Tree->ElePos.col(Tree->Elements[Node1->StartInd + i]),
                          Tree->ElePos.col(Tree->Elements[Node2->StartInd + c])) - predict;
    }

    // now, grab the column corresponding to the absolute maximum entry of v
    v.col(k).cwiseAbs().maxCoeff(&maxInd);
    j = int(maxInd);

    if (v.col(k).norm() < 1e-10) {
      break;
    }

    // scale the v vector
    if (fabs(v(j, k)) > 1.0e-10) {
      gamma = 1.0 / v(j, k);
    } else {
      break;
    }

    //double gamma = 1.0/v(j,k);
    v.col(k) *= gamma;

    // grab a column of the residual
    for (unsigned int r = 0; r < nrows; ++r) {
      double predict = 0;
      for (unsigned int l = 0; l < k; ++l) {
        predict += u(r, l) * v(j, l);
      }

      u(r,
        k) =
        CovKernel->Kernel(Tree->ElePos.col(Tree->Elements[Node1->StartInd + r]),
                          Tree->ElePos.col(Tree->Elements[Node2->StartInd + j])) - predict;
    }
  }

  u = u.topLeftCorner(nrows, std::min(k, maxK)).eval();
  v = v.topLeftCorner(ncols, std::min(k, maxK)).eval();
}

namespace muq {
namespace Utilities {
template class ElementPairNode<1>;
template class ElementPairNode<2>;
template class ElementPairNode<3>;
}
}


