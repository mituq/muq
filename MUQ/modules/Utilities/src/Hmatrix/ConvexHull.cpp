
#include "MUQ/Utilities/Hmatrix/ConvexHull.h"
#include <iostream>

using namespace muq::Utilities;


/** Check the turning direction, will be negative for ccw, 0 for colinear, and positive for cw */
template<unsigned int dim>
double TurnDirection(const Eigen::Matrix<double, dim, 1>& a,
                     const Eigen::Matrix<double, dim, 1>& b,
                     const Eigen::Matrix<double, dim, 1>& c)
{
  Eigen::Matrix<double, dim, 1> bma = b - a;
  Eigen::Matrix<double, dim, 1> cma = c - a;

  return bma[0] * cma[1] - bma[1] * cma[0];
}

template<unsigned int dim>
struct hull_comparator {
  Eigen::Matrix<double, dim, 1> r; //reference point

  hull_comparator(const Eigen::Matrix<double, dim, 1>& rIn) : r(rIn) {}

  // comparison operator
  bool operator()(const Eigen::Matrix<double, dim, 1>& a, const Eigen::Matrix<double, dim, 1>& b)
  {
    Eigen::Matrix<double, dim, 1> amr = a - r; // a minus r
    Eigen::Matrix<double, dim, 1> bmr = b - r; // b minus r

    double a_angle = std::atan2(amr[1], amr[0]);
    double b_angle = std::atan2(bmr[1], bmr[0]);

    if (a_angle != b_angle) {
      return a_angle > b_angle;
    } else {
      return amr.norm() < bmr.norm();
    }
  }
};

template<unsigned int dim>
bool XDirComp2d(const Eigen::Matrix<double, dim, 1>& x, const Eigen::Matrix<double, dim, 1>& y)
{
  if (x[0] != y[0]) {
    return x[0] < y[0];
  } else {
    return x[1] < y[1];
  }
}

template<unsigned int dim>
bool XDirComp1d(const Eigen::Matrix<double, dim, 1>& x, const Eigen::Matrix<double, dim, 1>& y)
{
  return x[0] < y[0];
}

template<unsigned int dim>
void ConvexHull<dim>::GrahamScan(std::vector < Eigen::Matrix < double, dim, 1 >>& AllPts)
{
  hullpts.clear();

  // assuming the points are already sorted in clockwise order, compute the convex hull using the graham scan and store
  // the results in the hullpts vector

  // start at the furthest left and bottom point
  auto i = AllPts.begin();
  for (; i != AllPts.begin() + 2; ++i) {
    hullpts.push_back(*i);
  }


  for (; i != AllPts.end(); ++i) {
    bool done_removing = false;

    while (!done_removing) {
      Eigen::Matrix<double, dim, 1>& a = *(hullpts.end() - 2), & b = *(hullpts.end() - 1);
      done_removing = (TurnDirection<dim>(a, b, *i) <= 0);

      if (!done_removing) {
        hullpts.pop_back();
      } else {
        hullpts.push_back(*i);
      }
    }
  }
}

/** Build the convex hull from the pts stored in a vector. */
template<unsigned int dim>
void ConvexHull<dim>::BuildHull(std::vector < Eigen::Matrix < double, dim, 1 >>& AllPts)
{
  if (dim == 1) {
    hullpts.clear();

    // get the index of the smallest element
    auto ind = std::min_element(AllPts.begin(), AllPts.end(), XDirComp1d<dim> );

    hullpts.push_back(*ind);
    ind = std::max_element(AllPts.begin(), AllPts.end(), XDirComp1d<dim> );
    hullpts.push_back(*ind);
  } else if (dim == 2) {
    // sort all the points according to their x-coordinate
    std::sort(AllPts.begin(), AllPts.end(), XDirComp2d<dim> );

    Eigen::Matrix<double, dim, 1> lowest_leftmost = *(AllPts.begin());

    std::sort(AllPts.begin() + 1, AllPts.end(), hull_comparator<dim>(lowest_leftmost));

    GrahamScan(AllPts);
  } else if (dim == 3) {
    std::cerr <<
    "ERROR: This ConvexHull class does not currently support 3d.  If you want to implement this please contact Matt at mparno@mit.edu.\n";
    throw("ConvexHull does not support 3d.");
  }

  // at this point, the hullpts are sorted in clockwise order
}

/** Compute and return the diameter of this convex hull.
 *   \todo Speed this up with the "rotating calipers" approach
 */
template<unsigned int dim>
double ConvexHull<dim>::GetDiam() const
{
  if (dim == 1) {
    return hullpts[1][0] - hullpts[0][0];
  } else if (dim == 2) {
    double diam = 0;

    // loop through each pair of points in the hull, returning the maximum distance.
    for (auto i = hullpts.begin(); i < hullpts.end(); ++i) {
      for (auto j = i + 1; j < hullpts.end(); ++j) {
        diam = std::max((*i - *j).norm(), diam);
      }
    }
    return diam;
  }

  return -999;
}

/** Compute and return the minimum distance between points in this hull and another hull*/
template<unsigned int dim>
double ConvexHull<dim>::GetDist(const ConvexHull<dim>& Rhs) const
{
  if (dim == 1) {
    return std::min(std::fabs(Rhs.hullpts[0][0] - hullpts[1][0]), std::fabs(hullpts[0][0] - Rhs.hullpts[1][0]));
  } else if (dim == 2) {
    double dist = 1e100;

    // loop through each pair of points in the hull, returning the maximum distance.
    for (auto i = hullpts.begin(); i < hullpts.end(); ++i) {
      for (auto j = Rhs.hullpts.begin(); j < Rhs.hullpts.end(); ++j) {
        dist = std::min((*i - *j).norm(), dist);
      }
    }
    return dist;
  }

  return -999;
}

/** Add on to this convex hull. */
template<unsigned int dim>
ConvexHull<dim>& ConvexHull<dim>::operator+=(const ConvexHull<dim>& Rhs)
{
  if (dim == 1) {
    hullpts[0][0] = std::min(Rhs.hullpts[0][0], hullpts[0][0]);
    hullpts[1][0] = std::max(Rhs.hullpts[1][0], hullpts[1][0]);
  } else if (dim == 2) {
    // create a vector holding the convex hull points of this and rhs
    std::vector < Eigen::Matrix < double, dim, 1 >> PotentialHull = hullpts;
    PotentialHull.insert(PotentialHull.end(), Rhs.hullpts.begin(), Rhs.hullpts.end());

    // sort the points by their x position
    std::sort(PotentialHull.begin(), PotentialHull.end(), XDirComp2d<dim> );

    Eigen::Matrix<double, dim, 1> lowest_leftmost = *(PotentialHull.begin());

    // sort the hull by angle, putting them in clockwise order
    std::sort(PotentialHull.begin() + 1, PotentialHull.end(), hull_comparator<dim>(lowest_leftmost));

    // perform the Graham scan which will compute the convex hull
    GrahamScan(PotentialHull);
  }

  return *this;
}

/** Merge two convex hulls into one. */
template<unsigned int dim>
ConvexHull<dim> ConvexHull<dim>::operator+(const ConvexHull<dim>& Rhs)
{
  ConvexHull temp(*this);

  temp += Rhs;
  return temp;
}

/** Copy a different convex hull */
template<unsigned int dim>
ConvexHull<dim>& ConvexHull<dim>::operator=(const ConvexHull<dim>& Rhs)
{
  hullpts = Rhs.hullpts;

  return *this;
}

template class muq::Utilities::ConvexHull<1>;
template class muq::Utilities::ConvexHull<2>;
template class muq::Utilities::ConvexHull<3>;
