#include "MUQ/Utilities/python/VariableCollectionPython.h"

#include "MUQ/Utilities/VariableCollection.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;


void (VariableCollection::*pv1)(shared_ptr<Variable>) = &VariableCollection::PushVariable;
void (VariableCollection::*pv2)(string,shared_ptr<RecursivePolynomialFamily1D>, shared_ptr<QuadratureFamily1D> ) = &VariableCollection::PushVariable;
void (VariableCollection::*pv3)(string,shared_ptr<QuadratureFamily1D> ) = &VariableCollection::PushVariable;
void (VariableCollection::*pv4)(string,shared_ptr<RecursivePolynomialFamily1D> ) = &VariableCollection::PushVariable;

void muq::Utilities::ExportVariableCollection(){
  
  py::class_<VariableCollection, shared_ptr<VariableCollection>, boost::noncopyable> exportVar("VariableCollection");
  
  exportVar.def("PushVariable", pv1);
  exportVar.def("PushVariable", pv2);
  exportVar.def("PushVariable", pv3);
  exportVar.def("PushVariable", pv4);
  
  exportVar.def("length", &VariableCollection::length);
  exportVar.def("GetVariable", &VariableCollection::GetVariable);
  
}
