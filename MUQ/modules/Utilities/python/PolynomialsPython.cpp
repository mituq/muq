#include "MUQ/Utilities/python/PolynomialsPython.h"
#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;

void muq::Utilities::ExportRecursive()
{
  py::class_<RecursivePolynomialFamily1D, shared_ptr<RecursivePolynomialFamily1D>, boost::noncopyable> exportPoly("RecursivePolynomialFamily1D", py::no_init);
  
  exportPoly.def("evaluate", &RecursivePolynomialFamily1D::evaluate);
  exportPoly.def("GetNormalization", &RecursivePolynomialFamily1D::GetNormalization);
  exportPoly.def("gradient", &RecursivePolynomialFamily1D::gradient);
  exportPoly.def("secondDerivative", &RecursivePolynomialFamily1D::secondDerivative);
}

BOOST_PYTHON_FUNCTION_OVERLOADS(Roots_Hermite_overloads, Static1DPolynomial<HermitePolynomials1DRecursive>::PyRootsTol, 1, 2)
BOOST_PYTHON_FUNCTION_OVERLOADS(Roots_ProbHermite_overloads, Static1DPolynomial<ProbabilistHermite>::PyRootsTol, 1, 2)
BOOST_PYTHON_FUNCTION_OVERLOADS(Roots_Legendre_overloads, Static1DPolynomial<LegendrePolynomials1DRecursive>::PyRootsTol, 1, 2)
BOOST_PYTHON_FUNCTION_OVERLOADS(Roots_Monomial_overloads, Static1DPolynomial<Monomial>::PyRootsTol, 1, 2)

void muq::Utilities::ExportHermite()
{
  py::class_<Static1DPolynomial<HermitePolynomials1DRecursive>, shared_ptr<Static1DPolynomial<HermitePolynomials1DRecursive>>,
  py::bases<RecursivePolynomialFamily1D>, boost::noncopyable> exportStaticPoly("StaticHermitePolynomial", py::no_init);
  
  exportStaticPoly.def("Roots", &Static1DPolynomial<HermitePolynomials1DRecursive>::PyRootsTol, Roots_Hermite_overloads(py::args("coeffs","tol"),"Find all roots")).staticmethod("Roots");
  
  py::implicitly_convertible<shared_ptr<Static1DPolynomial<HermitePolynomials1DRecursive>>, shared_ptr<RecursivePolynomialFamily1D>>();
  
  py::class_<HermitePolynomials1DRecursive, shared_ptr<HermitePolynomials1DRecursive>,
  py::bases<Static1DPolynomial<HermitePolynomials1DRecursive>>, boost::noncopyable> exportPoly("HermitePolynomials1DRecursive");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<HermitePolynomials1DRecursive>, shared_ptr<RecursivePolynomialFamily1D>>();
}

void muq::Utilities::ExportProbHermite()
{
  py::class_<Static1DPolynomial<ProbabilistHermite>, shared_ptr<Static1DPolynomial<ProbabilistHermite>>,
  py::bases<RecursivePolynomialFamily1D>, boost::noncopyable> exportStaticPoly("StaticProbHermitePolynomial", py::no_init);
  
  exportStaticPoly.def("Roots", &Static1DPolynomial<ProbabilistHermite>::PyRootsTol, Roots_ProbHermite_overloads(py::args("coeffs","tol"),"Find all roots")).staticmethod("Roots");
  
  py::implicitly_convertible<shared_ptr<Static1DPolynomial<ProbabilistHermite>>, shared_ptr<RecursivePolynomialFamily1D>>();
  
  py::class_<ProbabilistHermite, shared_ptr<ProbabilistHermite>,
  py::bases<Static1DPolynomial<ProbabilistHermite>>, boost::noncopyable> exportPoly("ProbabilistHermite");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<ProbabilistHermite>, shared_ptr<RecursivePolynomialFamily1D>>();
}

void muq::Utilities::ExportLegendre()
{
  py::class_<Static1DPolynomial<LegendrePolynomials1DRecursive>, shared_ptr<Static1DPolynomial<LegendrePolynomials1DRecursive>>,
  py::bases<RecursivePolynomialFamily1D>, boost::noncopyable> exportStaticPoly("StaticLegendrePolynomial", py::no_init);
  
  exportStaticPoly.def("Roots", &Static1DPolynomial<LegendrePolynomials1DRecursive>::PyRootsTol, Roots_Legendre_overloads(py::args("coeffs","tol"),"Find all roots")).staticmethod("Roots");
  
  py::implicitly_convertible<shared_ptr<Static1DPolynomial<LegendrePolynomials1DRecursive>>, shared_ptr<RecursivePolynomialFamily1D>>();
  
  py::class_<LegendrePolynomials1DRecursive, shared_ptr<LegendrePolynomials1DRecursive>,
  py::bases<Static1DPolynomial<LegendrePolynomials1DRecursive>>, boost::noncopyable> exportPoly("LegendrePolynomials1DRecursive");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<LegendrePolynomials1DRecursive>, shared_ptr<RecursivePolynomialFamily1D>>();
}

void muq::Utilities::ExportMonomial()
{
  py::class_<Static1DPolynomial<Monomial>, shared_ptr<Static1DPolynomial<Monomial>>,
  py::bases<RecursivePolynomialFamily1D>, boost::noncopyable> exportStaticPoly("StaticMonomial", py::no_init);
  
  exportStaticPoly.def("Roots", &Static1DPolynomial<Monomial>::PyRootsTol, Roots_Monomial_overloads(py::args("coeffs","tol"),"Find all roots")).staticmethod("Roots");
  
  
  py::implicitly_convertible<shared_ptr<Static1DPolynomial<Monomial>>, shared_ptr<RecursivePolynomialFamily1D>>();
  
  py::class_<Monomial, shared_ptr<Monomial>, py::bases<Static1DPolynomial<Monomial>>, boost::noncopyable> exportPoly("Monomial");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<Monomial>, shared_ptr<RecursivePolynomialFamily1D>>();
}