#include "MUQ/Utilities/python/PythonTranslater.h"

using namespace std;

boost::property_tree::ptree muq::Utilities::PythonDictToPtree(boost::python::dict const& dict)
{
  boost::property_tree::ptree pt;

  boost::python::list keys = dict.keys();

  for (int i = 0; i < boost::python::len(keys); ++i) {
    string key = boost::python::extract<std::string>(keys[i]);

    boost::python::object val = dict[key];

           assert(TrySetPtreeVal<std::string>(pt, key,
                                       val) ||
           TrySetPtreeVal<int>(pt, key, val) || TrySetPtreeVal<double>(pt, key, val));
  }

  return pt;
}

boost::python::list  muq::Utilities::VectorToPythonList(std::vector<Eigen::VectorXd> const& input)
{
  boost::python::list inputPython;

  for (unsigned int i = 0; i < input.size(); ++i) {
    boost::python::list in = GetPythonVector<Eigen::VectorXd>(input[i]);
    inputPython.append(in);
  }

  return inputPython;
}

template<typename scalarType>
Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> muq::Utilities::GetEigenMatrix(boost::python::list& pyMatrix)
{
  const int N = boost::python::len(pyMatrix);

  Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> matrix(N, boost::python::len(boost::python::extract<boost::python::list>(pyMatrix[0])));

  for (int i = 0; i < N; ++i) {
    matrix.row(i) = GetEigenVector<Eigen::Matrix<scalarType,Eigen::Dynamic,1>>(boost::python::extract<boost::python::list>(pyMatrix[i]));
  }

  return matrix;
}

template Eigen::MatrixXd muq::Utilities::GetEigenMatrix(boost::python::list& pyMatrix);
template Eigen::MatrixXi muq::Utilities::GetEigenMatrix(boost::python::list& pyMatrix);
template Eigen::Matrix<unsigned,Eigen::Dynamic,Eigen::Dynamic> muq::Utilities::GetEigenMatrix(boost::python::list& pyMatrix);

template<typename scalarType>
Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> muq::Utilities::GetEigenMatrix(boost::python::list const& pyMatrix)
{
  const int N = boost::python::len(pyMatrix);
  
  Eigen::Matrix<scalarType,Eigen::Dynamic,Eigen::Dynamic> matrix(N, boost::python::len(boost::python::extract<boost::python::list>(pyMatrix[0])));
  
  for (int i = 0; i < N; ++i) {
    matrix.row(i) = GetEigenVector<Eigen::Matrix<scalarType,Eigen::Dynamic,1>>(boost::python::extract<boost::python::list>(pyMatrix[i]));
  }
  
  return matrix;
}

template Eigen::MatrixXd muq::Utilities::GetEigenMatrix(boost::python::list const& pyMatrix);
template Eigen::MatrixXi muq::Utilities::GetEigenMatrix(boost::python::list const& pyMatrix);
template Eigen::Matrix<unsigned,Eigen::Dynamic,Eigen::Dynamic> muq::Utilities::GetEigenMatrix(boost::python::list const& pyMatrix);


void muq::Utilities::VectorTranslate(Eigen::VectorXd const& Input, boost::python::list& Output, int size)
{
  for (int i = 0; i < size; ++i) {
    Output.append(Input(i));
  }
}

