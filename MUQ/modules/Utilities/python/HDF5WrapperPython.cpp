#include "MUQ/Utilities/python/HDF5WrapperPython.h"

using namespace std;
using namespace muq::Utilities;

HDF5WrapperPython::HDF5WrapperPython() : HDF5Wrapper() {}

void HDF5WrapperPython::OpenFileDict(boost::python::dict const& dict)
{
  OpenFile(PythonDictToPtree(dict));
}

void HDF5WrapperPython::OpenFileStr(string const& filename)
{
  OpenFile(filename);
}

void HDF5WrapperPython::WriteMatrixPy(string const datasetName, boost::python::list const& pyMatrix)
{
  WriteMatrix(datasetName, GetEigenMatrix(pyMatrix));
}

void HDF5WrapperPython::WriteVectorPy(string const datasetName, boost::python::list const& vector)
{
  WriteMatrix(datasetName, GetEigenVector<Eigen::VectorXd>(vector));
}

boost::python::list HDF5WrapperPython::ReadMatrixPy(string const datasetName)
{
  return GetPythonMatrix(ReadMatrix(datasetName));
}

boost::python::list HDF5WrapperPython::ReadVectorPy(string const datasetName)
{
  return GetPythonVector<Eigen::VectorXd>(ReadMatrix(datasetName));
}

void HDF5WrapperPython::WritePartialMatrixPy(string const               datasetName,
                                             boost::python::list const& matrix,
                                             int                        row,
                                             int                        col)
{
  WritePartialMatrix(datasetName, GetEigenMatrix(matrix), row, col);
}

void muq::Utilities::ExportHDF5Wrapper()
{
  boost::python::class_<HDF5WrapperPython, shared_ptr<HDF5WrapperPython>, boost::noncopyable> exportHDF5Wrapper(
    "HDF5Wrapper",
    boost::python::init<>());

  exportHDF5Wrapper.def("OpenFile", &HDF5WrapperPython::OpenFileDict);
  exportHDF5Wrapper.def("OpenFile", &HDF5WrapperPython::OpenFileStr).staticmethod("OpenFile");
  exportHDF5Wrapper.def("MergeFile", &HDF5Wrapper::MergeFile).staticmethod("MergeFile");
  exportHDF5Wrapper.def("CloseFile", &HDF5Wrapper::CloseFile).staticmethod("CloseFile");
  exportHDF5Wrapper.def("FlushFile", &HDF5Wrapper::FlushFile).staticmethod("FlushFile");
  exportHDF5Wrapper.def("DoesGroupExist", &HDF5Wrapper::DoesGroupExist).staticmethod("DoesGroupExist");
  exportHDF5Wrapper.def("DoesDataSetExist", &HDF5Wrapper::DoesDataSetExist).staticmethod("DoesDataSetExist");
  exportHDF5Wrapper.def("CreateGroup", &HDF5Wrapper::CreateGroup).staticmethod("CreateGroup");
  exportHDF5Wrapper.def("WriteMatrix", &HDF5WrapperPython::WriteMatrixPy).staticmethod("WriteMatrix");
  exportHDF5Wrapper.def("WriteVector", &HDF5WrapperPython::WriteVectorPy).staticmethod("WriteVector");

  exportHDF5Wrapper.def("WriteStringAttribute", &HDF5Wrapper::WriteStringAttribute ).staticmethod("WriteStringAttribute");
  exportHDF5Wrapper.def("WriteScalarAttribute", &HDF5Wrapper::WriteScalarAttribute<double> );
  exportHDF5Wrapper.def("WriteScalarAttribute", &HDF5Wrapper::WriteScalarAttribute<int> ).staticmethod("WriteScalarAttribute");
  exportHDF5Wrapper.def("WriteVectorAttribute", &HDF5WrapperPython::WriteVectorAttributePy<double> );
  exportHDF5Wrapper.def("WriteVectorAttribute", &HDF5WrapperPython::WriteVectorAttributePy<int> ).staticmethod("WriteVectorAttribute");
  
  exportHDF5Wrapper.def("GetVectorAttribute", &HDF5WrapperPython::GetVectorAttributePy<double> );
  exportHDF5Wrapper.def("GetVectorAttribute", &HDF5WrapperPython::GetVectorAttributePy<int> ).staticmethod("GetVectorAttribute");
  exportHDF5Wrapper.def("GetScalarAttribute", &HDF5Wrapper::GetScalarAttribute<double> );
  exportHDF5Wrapper.def("GetScalarAttribute", &HDF5Wrapper::GetScalarAttribute<int> ).staticmethod("GetScalarAttribute");
  exportHDF5Wrapper.def("GetStringAttribute", &HDF5Wrapper::GetStringAttribute ).staticmethod("GetStringAttribute");


  exportHDF5Wrapper.def("ReadMatrix", &HDF5WrapperPython::ReadMatrixPy).staticmethod("ReadMatrix");
  exportHDF5Wrapper.def("ReadVector", &HDF5WrapperPython::ReadVectorPy).staticmethod("ReadVector");
  exportHDF5Wrapper.def("WritePartialMatrix", &HDF5WrapperPython::WritePartialMatrixPy).staticmethod("WritePartialMatrix");

  boost::python::implicitly_convertible<shared_ptr<HDF5WrapperPython>, shared_ptr<HDF5Wrapper> >();
}

