#include "MUQ/Utilities/python/RandomGeneratorPython.h"

using namespace std;
using namespace muq::Utilities;
  
boost::python::list RandomGeneratorPython::GetUniformRandomVectorPy(int const n)
{
  return GetPythonVector<Eigen::VectorXd>(RandomGenerator::GetUniformRandomVector(n));
}

boost::python::list RandomGeneratorPython::GetUniformRandomMatrixPy(int const m, int const n)
{
  return GetPythonMatrix(RandomGenerator::GetUniformRandomMatrix(m,n));
}

boost::python::list RandomGeneratorPython::GetUniqueUniformIntsPy(int lb, int ub, int N)
{
  return GetPythonVector<Eigen::VectorXi>(RandomGenerator::GetUniqueUniformInts(lb,ub,N));
}

boost::python::list RandomGeneratorPython::GetNormalRandomVectorPy(int const n)
{
  return GetPythonVector<Eigen::VectorXd>(RandomGenerator::GetNormalRandomVector(n));
}

boost::python::list RandomGeneratorPython::GetNormalRandomMatrixPy(int const m, int const n)
{
  return GetPythonMatrix(RandomGenerator::GetNormalRandomMatrix(m,n));
}


void muq::Utilities::ExportRandomGenerator()
{

  boost::python::class_<RandomGeneratorPython, shared_ptr<RandomGeneratorPython>, boost::noncopyable> exportRandomGenerator("RandomGenerator",boost::python::init<>());
  
  exportRandomGenerator.def("GetUniformRandomVector",&RandomGeneratorPython::GetUniformRandomVectorPy).staticmethod("GetUniformRandomVector");  
  exportRandomGenerator.def("GetUniformRandomMatrix",&RandomGeneratorPython::GetUniformRandomMatrixPy).staticmethod("GetUniformRandomMatrix");
  exportRandomGenerator.def("GetUniqueUniformInts",&RandomGeneratorPython::GetUniqueUniformIntsPy).staticmethod("GetUniqueUniformInts");
  exportRandomGenerator.def("GetNormalRandomVector",&RandomGeneratorPython::GetNormalRandomVectorPy).staticmethod("GetNormalRandomVector");
  exportRandomGenerator.def("GetNormalRandomMatrix",&RandomGeneratorPython::GetNormalRandomMatrixPy).staticmethod("GetNormalRandomMatrix");
  

  exportRandomGenerator.def("GetNormal",&RandomGenerator::GetNormal).staticmethod("GetNormal");
  exportRandomGenerator.def("GetUniform",&RandomGenerator::GetUniform).staticmethod("GetUniform");
  
  exportRandomGenerator.def("GetGamma",&RandomGenerator::GetGamma).staticmethod("GetGamma");
  
  exportRandomGenerator.def("GetUniformInt",&RandomGenerator::GetUniformInt).staticmethod("GetUniformInt");
  exportRandomGenerator.def("SetSeed",&RandomGenerator::SetSeed).staticmethod("SetSeed");
  
}