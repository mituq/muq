#include "MUQ/Optimization/python/OptAlgBasePython.h"

void muq::Optimization::ExportOptAlgBase()
{
  boost::python::class_<OptAlgBase, std::shared_ptr<OptAlgBase>, boost::noncopyable> exportOptAlgBase(
    "OptAlgBase",
    boost::python::
    no_init);

  // make constructors
  exportOptAlgBase.def("__init__", boost::python::make_constructor(CreateOptAlgBase));
  exportOptAlgBase.def("__init__", boost::python::make_constructor(CreateOptAlgBaseFromFile));

  // expose functions
  exportOptAlgBase.def("Solve", &OptAlgBase::PySolve);
  exportOptAlgBase.def("GetStatus", &OptAlgBase::GetStatus);

  // expose public members
  exportOptAlgBase.def_readonly("directlyHandlesUnconst", &OptAlgBase::directlyHandlesUnconst);
  exportOptAlgBase.def_readonly("handlesPartlyBoundConsts", &OptAlgBase::handlesPartlyBoundConsts);
  exportOptAlgBase.def_readonly("requiresFullyBoundConsts", &OptAlgBase::requiresFullyBoundConsts);
  exportOptAlgBase.def_readonly("handlesLinearConsts", &OptAlgBase::handlesLinearConsts);
  exportOptAlgBase.def_readonly("handlesNonlinearConsts", &OptAlgBase::handlesNonlinearConsts);
  exportOptAlgBase.def_readonly("handlesStochastic", &OptAlgBase::handlesStochastic);
}