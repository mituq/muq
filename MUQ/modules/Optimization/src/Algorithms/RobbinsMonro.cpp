//
//  RobbinsMonro.cpp
//
//
//  Created by Matthew Parno on 5/16/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

// self include
#include "MUQ/Optimization/Algorithms/RobbinsMonro.h"

using namespace muq::Optimization;
using namespace std;


REGISTER_OPT_DEF_TYPE(RobbinsMonro)
/** Construct an instance of the RobbinsMonro method with the parameters found in paramList. */
RobbinsMonro::RobbinsMonro(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : OptAlgBase(
                                                                                                              ProbPtr,
                                                                                                              properties,
                                                                                                              true,
                                                                                                              false,
                                                                                                              false,
                                                                                                              false,
                                                                                                              false,
                                                                                                              false,
                                                                                                              false,
                                                                                                              true)
{
  // read in the Robbins-Monro beta parameter
  beta = properties.get("Opt.RobbinsMonro.beta", 1.0);

  // read the weight on the average state in the Robbins-Monro update eqn
  k = properties.get("Opt.RobbinsMonro.AvgWeight", 0.0);

  // read the window parameter for the computing the average state
  b0 = properties.get("Opt.RobbinsMonro.WindowCoeff", 1.0);

  // read in the power on the iteration in the decaying weight
  epsPow = properties.get("Opt.RobbinsMonro.ItPow", 1.0);
}

/** Solve the stochastic optimization problem using x0 as a starting point for the iteration. */
Eigen::VectorXd RobbinsMonro::solve(const Eigen::VectorXd& x0)
{
  int dim = x0.size();

  // allocate space for descent direction
  Eigen::VectorXd delta = Eigen::VectorXd::Zero(dim);

  // set state to initially be the starting position
  Eigen::VectorXd state    = x0;
  Eigen::VectorXd AvgState = x0;

  // flag=0 when we should continue, 1 to stop
  bool stopFlag = false;
  int  it       = 0; // number of iterations


  // optimization loop
  status = -1;
  double fnew = 100, fold = 101;
  while (!stopFlag) {
    if (verbose > 1) {
      std::cout << "Iteration: " << it << "   fval: " << fnew << std::endl;
    }

    // add to number of iterations
    ++it;

    // update the old function value
    fold = fnew;

    // compute the descent direction
    fnew = OptProbPtr->grad(state, delta);
    double epsn = beta / pow(it, epsPow);
    delta = stepLength * epsn * (-delta + k * (AvgState - state));

    // check for convergence:
    if (fnew < fabstol) {
      status   = 2;
      stopFlag = true;
      break;
    }

    // update the state
    state += delta;

    // check to see if we've converged
    if (fabs(fold - fnew) < ftol) {
      status   = 3;
      fold     = fnew;
      stopFlag = true;
    }


    // stop if we've reached the maximum number of iterations
    if (it == maxIts) {
      status   = 5;
      stopFlag = true;
    }


    // if we want to be really verbose, print the iteration and function value
    if (verbose > 2) {
      cout << "Iteration: " << it << "   fval: " << fnew << endl;
    }

    // update the average state
    double bn = b0 / it;
    AvgState = (1 - bn) * AvgState + bn * state;
  }

  return state;
}

