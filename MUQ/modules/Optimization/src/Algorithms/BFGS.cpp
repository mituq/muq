
#include "MUQ/Optimization/Algorithms/BFGS.h"

using namespace muq::Optimization;
using namespace std;

REGISTER_OPT_DEF_TYPE(BFGS_Line)
/** parameter list constructor */
BFGS_Line::BFGS_Line(shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : GradLineSearch(ProbPtr,
                                                                                                                properties)
{
  int dim = ProbPtr->GetDim();

  // allocate some of the variables
  ApproxHessInv = Eigen::MatrixXd::Identity(dim, dim);

  // there have been no calls to step yet
  stepCalls = 0;
}

/** step function, return the BFGS direction*/
Eigen::VectorXd BFGS_Line::step()
{
  Eigen::VectorXd output;

  // find the objective function and gradient
  fold = OptProbPtr->grad(xc, gc);

  // check to see if this is the first call to the step function, if this is not the first call to step, we can update
  // the Hessian
  if (stepCalls > 0) {
    // compute the change in state
    Eigen::VectorXd sk = xc - OldState;

    // compute the change in gradient
    Eigen::VectorXd yk = gc - OldGrad;

    // check to see if we need a restart
    if (yk.dot(sk) < 1e-10) {
      ApproxHessInv = Eigen::MatrixXd::Identity(OptProbPtr->GetDim(), OptProbPtr->GetDim());
    } else {
      // use the Sherman-Morrison formula to update the approximate inverse
      double skyk         = sk.dot(yk);
      Eigen::VectorXd Ayk = ApproxHessInv.selfadjointView<Eigen::Lower>() * yk;
      double scale        = (skyk + yk.dot(Ayk)) / (skyk * skyk);

      ApproxHessInv.selfadjointView<Eigen::Lower>().rankUpdate(sk, scale);
      ApproxHessInv.selfadjointView<Eigen::Lower>().rankUpdate(Ayk, sk, -1.0 / skyk);
    }
  }

  ++stepCalls;

  // if it is, we cannot update the pproximate hessian inverse and we just use what we have:
  output = -1.0 * (ApproxHessInv.selfadjointView<Eigen::Lower>() * gc);

  // store the current position and gradient
  OldState = xc;
  OldGrad  = gc;

  return output;
}

