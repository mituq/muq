
#include "MUQ/Optimization/Constraints/ConstraintList.h"

#include "MUQ/Optimization/Constraints/BoundConstraint.h"
#include "MUQ/Optimization/Constraints/LinearConstraint.h"

using namespace muq::Optimization;
using namespace std;

/** Evaluate all the constraints and put the results in a single vector.  Note that we assume all constraints are of the
 * form g(x)=0 of g(x)<=0. */
Eigen::VectorXd ConstraintList::eval(const Eigen::VectorXd& xc)
{
  // create a vector to hold the entire output
  Eigen::VectorXd output = Eigen::VectorXd::Zero(DimOut);

  // loop through all of the constraints -- first the nonlinear constraints, then the linear constraints, and finally
  // the bound constraints
  int currPos = 0;
  
  for (auto c = Nonlinears.begin(); c < Nonlinears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output.segment(currPos, cSize) = (*c)->eval(xc);
    currPos                       += cSize;
  }

  for (auto c = Linears.begin(); c < Linears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output.segment(currPos, cSize) = (*c)->eval(xc);
    currPos                       += cSize;
  }

  for (auto c = Bounds.begin(); c < Bounds.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output.segment(currPos, cSize) = (*c)->eval(xc);
    currPos                       += cSize;
  }
  
  return output;
}

/** Evaluate the action of the Jacobian of this constraint on a vector. */
Eigen::VectorXd ConstraintList::ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  // make sure the input vectors are the right sizes
  assert(xc.size() == DimIn);
  assert(vecIn.size() == DimOut);

  // create the gradient vector to hold the output
  Eigen::VectorXd output = Eigen::VectorXd::Zero(DimIn);

  // fill in the gradient by summing up the contributions from each model
  // loop through all of the constraints -- first the nonlinear constraints, then the linear constraints, and finally
  // the bound constraints
  int currPos = 0;
  for (auto c = Nonlinears.begin(); c < Nonlinears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output  += (*c)->ApplyJacTrans(xc, vecIn.segment(currPos, cSize));
    currPos += cSize;
  }

  for (auto c = Linears.begin(); c < Linears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output  += (*c)->ApplyJacTrans(xc, vecIn.segment(currPos, cSize));
    currPos += cSize;
  }

  for (auto c = Bounds.begin(); c < Bounds.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output  += (*c)->ApplyJacTrans(xc, vecIn.segment(currPos, cSize));
    currPos += cSize;
  }

  return output;
}

Eigen::MatrixXd ConstraintList::getJacobian(const Eigen::VectorXd& xc)
{
  assert(xc.size() == DimIn);

  Eigen::MatrixXd jac(DimOut, DimIn);

  // fill in the gradient by summing up the contributions from each model
  // loop through all of the constraints -- first the nonlinear constraints, then the linear constraints, and finally
  // the bound constraints
  int currPos = 0;
  for (auto c = Nonlinears.begin(); c < Nonlinears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    jac.block(currPos, 0, cSize, DimIn) = (*c)->getJacobian(xc);
    currPos                            += cSize;
  }

  for (auto c = Linears.begin(); c < Linears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    jac.block(currPos, 0, cSize, DimIn) = (*c)->getJacobian(xc);
    currPos                            += cSize;
  }

  for (auto c = Bounds.begin(); c < Bounds.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    jac.block(currPos, 0, cSize, DimIn) = (*c)->getJacobian(xc);
    currPos                            += cSize;
  }

  return jac;
}

/** Evaluate the action of the Jacobian of this constraint on a vector. */
Eigen::MatrixXd ConstraintList::getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity)
{
  // make sure the input vectors are the right sizes
  assert(xc.size() == DimIn);
  assert(sensitivity.size() == DimOut);

  // create the gradient vector to hold the output
  Eigen::MatrixXd output = Eigen::MatrixXd::Zero(DimIn, DimIn);

  // fill in the hessian by summing up the hessian contributions from each model
  // loop through all of the constraints -- first the nonlinear constraints, then the linear constraints, and finally
  // the bound constraints
  int currPos = 0;
  for (auto c = Nonlinears.begin(); c < Nonlinears.end(); ++c) {
    int cSize = (*c)->NumConstraints();
    output  += (*c)->getHess(xc, sensitivity.segment(currPos, cSize));
    currPos += cSize;
  }

  // while we could include linear and bound constraints, the Hessians of these guys are zero
  //    for(auto c = Linears.begin(); c<Linears.end(); ++c){
  //        int cSize = (*c)->NumConstraints();
  //        output += (*c)->getHess(xc,sensitivity.segment(currPos,cSize));
  //        currPos += cSize;
  //    }
  //
  //    for(auto c = Bounds.begin(); c<Bounds.end(); ++c){
  //        int cSize = (*c)->NumConstraints();
  //        output += (*c)->ApplyJacTrans(xc,vecIn.segment(currPos,cSize));
  //        currPos += cSize;
  //    }

  return output;
}

void ConstraintList::Add(shared_ptr<ConstraintBase> Input)
{
  // make sure the input is the correct size
  assert(Input->GetDim() == DimIn);

  // check to see if the Input constraint has special structure
  auto bptr = dynamic_pointer_cast<BoundConstraint, ConstraintBase>(Input);
  if (bptr.get()) {
    Bounds.push_back(bptr);
  } else {
    // if this is not a bound constraint pointer, is it a linear pointer?
    auto lptr = dynamic_pointer_cast<LinearConstraint, ConstraintBase>(Input);
    if (lptr.get()) {
      Linears.push_back(lptr);
    } else {
      Nonlinears.push_back(Input);
    }
  }

  // add the dimension
  DimOut += Input->NumConstraints();
}
