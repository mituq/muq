
#include "gtest/gtest.h"

#include "MUQ/Optimization/Algorithms/Auctioneer.h"
#include "MUQ/Optimization/Algorithms/FRAuctioneer.h"


#include <boost/property_tree/ptree.hpp>

using namespace muq::Optimization;
using namespace std;


TEST(Optimization_AssignmentTest, BasicAuction)
{
  unsigned int Npts = 3;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(2, Npts);

  xi << 0.0, 0.5, 1.0, 0.0, 0.5, 1.0;

  Eigen::MatrixXd xo(2, Npts);
  xo << 0.4, 0.2, 1.2, 0.51, 0.01, 1.2;

  Auctioneer<double> solver(xi, xo);

  solver.solve();


  std::vector<unsigned int> solAssign = solver.GetSolution();

  EXPECT_EQ(1, solAssign[0]);
  EXPECT_EQ(0, solAssign[1]);
  EXPECT_EQ(2, solAssign[2]);
}

TEST(Optimization_FRAssignmentTest, BasicAuction)
{
  unsigned int Npts = 3;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(2, Npts);

  xi << 0.0, 0.5, 1.0, 0.0, 0.5, 1.0;

  Eigen::MatrixXd xo(2, Npts);
  xo << 0.4, 0.2, 1.2, 0.51, 0.01, 1.2;

  FRAuctioneer solver(xi, xo);

  solver.solve();


  std::vector<int> solAssign = solver.GetSolution();

  EXPECT_EQ(1, solAssign[0]);
  EXPECT_EQ(0, solAssign[1]);
  EXPECT_EQ(2, solAssign[2]);
}


// compute the cost of an assignment using an l2 cost function
template<typename derived>
double L2cost(const Eigen::MatrixXd& xi, const Eigen::MatrixXd& xo, std::vector<derived> asses)
{
  double output     = 0;
  unsigned int Npts = asses.size();

  for (unsigned int i = 0; i < Npts; ++i) {
    output += (xi.col(i) - xo.col(asses[i])).squaredNorm();
  }

  return output;
}

TEST(Optimization_AssignmentTest, LargeAuction)
{
  unsigned int Npts = 2000;
  unsigned int dim  = 10;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(dim, Npts);

  xi.setRandom();

  Eigen::MatrixXd xo(dim, Npts);
  xo.setRandom();

  // set up the solver
  Auctioneer<float> solver(xi, xo);

  // solve the problem
  solver.solve("exampleFiles/AuctionTestParams.xml");

  // get the assignments
  std::vector<unsigned int> solAssign = solver.GetSolution();

  // compute the cost of this assignment
  double baseCost = L2cost(xi, xo, solAssign);

  // check to make sure this assignment is a minimum
  for (unsigned int i = 0; i < Npts - 1; ++i) {
    std::swap(solAssign[i], solAssign[i + 1]);
    EXPECT_LT(baseCost, L2cost(xi, xo, solAssign));
    std::swap(solAssign[i], solAssign[i + 1]);
  }
}


TEST(Optimization_FRAssignmentTest, LargeAuction)
{
  unsigned int Npts = 2000;
  unsigned int dim  = 10;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(dim, Npts);

  xi.setRandom();

  Eigen::MatrixXd xo(dim, Npts);
  xo.setRandom();

  // set up the solver
  FRAuctioneer solver(xi, xo);

  // solve the problem
  solver.solve("exampleFiles/FRAuctionTestParams.xml");

  // get the assignments
  std::vector<int> solAssign = solver.GetSolution();

  // compute the cost of this assignment
  double baseCost = L2cost(xi, xo, solAssign);

  // check to make sure this assignment is a minimum
  for (unsigned int i = 0; i < Npts - 1; ++i) {
    std::swap(solAssign[i], solAssign[i + 1]);
    EXPECT_LT(baseCost, L2cost(xi, xo, solAssign));
    std::swap(solAssign[i], solAssign[i + 1]);
  }
}
