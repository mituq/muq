
#include <memory>

#include "gtest/gtest.h"

#include "MUQ/Optimization/Constraints/ConstraintList.h"

using namespace muq::Optimization;
using namespace std;


class TestNonlin : public ConstraintBase {
public:

  TestNonlin(int dim) : ConstraintBase(dim, dim) {}

  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc)
  {
    Eigen::VectorXd out = Eigen::VectorXd::Zero(xc.size());

    for (int i = 0; i < xc.size(); ++i) {
      out[i] = xc[i] * xc[i] - 1;
    }

    return out;
  }

  virtual Eigen::VectorXd ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
  {
    Eigen::VectorXd output = Eigen::VectorXd::Zero(xc.size());

    for (unsigned int i = 0; i < xc.size(); ++i) {
      output[i] = 2 * xc[i] * vecIn[i];
    }

    return output;
  }
};

class OptimizationConstraintList : public::testing::Test {
public:

  // hold all the constraints
  shared_ptr<ConstraintBase> Nonlin1;
  shared_ptr<ConstraintBase> Nonlin2;
  shared_ptr<ConstraintBase> Lin1;
  shared_ptr<ConstraintBase> Bound1;
  shared_ptr<ConstraintBase> Bound2;
  shared_ptr<ConstraintBase> Bound3;
  ConstraintList tester;

  OptimizationConstraintList() : tester(4)
  {
    // create a few nonlinear tests
    Nonlin1 = make_shared<TestNonlin>(4);
    Nonlin2 = make_shared<TestNonlin>(4);

    // create a few linear tests
    Eigen::MatrixXd A = Eigen::MatrixXd::Random(2, 4);
    Eigen::VectorXd b = Eigen::VectorXd::Ones(2);
    Lin1 = make_shared<LinearConstraint>(A, b);

    // create a few bound tests
    Bound1 = make_shared<BoundConstraint>(4, 0, 1, false);
    Bound2 = make_shared<BoundConstraint>(4, 1, 0, true);
    Bound3 = make_shared<BoundConstraint>(4, 3, -1, true);

    // add to the constraint list
    tester.Add(Nonlin1);
    tester.Add(Nonlin2);
    tester.Add(Lin1);
    tester.Add(Bound1);
    tester.Add(Bound2);
    tester.Add(Bound3);
  }
};

TEST_F(OptimizationConstraintList, AddTest)
{
  EXPECT_EQ(3, tester.NumBound());
  EXPECT_EQ(1, tester.NumLinear());
  EXPECT_EQ(2, tester.NumNonlinear());
}

TEST_F(OptimizationConstraintList, EvalTest)
{
  Eigen::VectorXd xc = 0.5 * Eigen::VectorXd::Ones(4);
  Eigen::VectorXd cc = tester.eval(xc);
}

TEST_F(OptimizationConstraintList, GradTest)
{
  Eigen::VectorXd xc   = 0.5 * Eigen::VectorXd::Ones(4);
  Eigen::VectorXd sens = Eigen::VectorXd::Ones(tester.NumConstraints());
  Eigen::VectorXd grad = tester.ApplyJacTrans(xc, sens);
}
