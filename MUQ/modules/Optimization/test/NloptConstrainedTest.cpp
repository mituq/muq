
#include "ConstrainedTestClasses.h"

using namespace muq::Optimization;
using namespace std;


TEST_F(Optimization_BoundConstrainedRoseTest, COBYLA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "COBYLA");
}


TEST_F(Optimization_BoundConstrainedRoseTest, BOBYQA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "BOBYQA");
}

TEST_F(Optimization_BoundConstrainedRoseTest, NEWUOA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "NEWUOA");
}

TEST_F(Optimization_BoundConstrainedRoseTest, PRAXIS)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "PRAXIS");
}

TEST_F(Optimization_BoundConstrainedRoseTest, NM)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "NM");
}

TEST_F(Optimization_BoundConstrainedRoseTest, Sbplx)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "Sbplx");
}

TEST_F(Optimization_BoundConstrainedRoseTest, MMA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "MMA");
}

TEST_F(Optimization_BoundConstrainedRoseTest, SLSQP)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "SLSQP");
}


TEST_F(Optimization_BoundConstrainedRoseTest, LBFGS)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "LBFGS");
}


TEST_F(Optimization_BoundConstrainedRoseTest, LBFGS_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "AugLag");
  testParams.put("Opt.NLOPT.Method", "LBFGS");
}


TEST_F(Optimization_BoundConstrainedRoseTest, PreTN)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "PreTN");
}


TEST_F(Optimization_BoundConstrainedRoseTest, LMVM)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "LMVM");
}


TEST_F(Optimization_FullyBoundRoseTest, DIRECT)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "DIRECT");
}

TEST_F(Optimization_FullyBoundRoseTest, DIRECTL)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "DIRECTL");
}

TEST_F(Optimization_FullyBoundRoseTest, CRS)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.RandSeed",5192012);
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "CRS");
}

TEST_F(Optimization_FullyBoundRoseTest, MLSL)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.RandSeed",5192012);
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "MLSL");
  testParams.put("Opt.NLOPT.LocalMethod", "MMA");
}

TEST_F(Optimization_FullyBoundRoseTest, ISRES)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.RandSeed",5192012);
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "ISRES");
}


TEST_F(Optimization_NonlinEqualityConstrainedTest, COBYLA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "COBYLA");
}


TEST_F(Optimization_NonlinEqualityConstrainedTest, NM_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "AugLag");
  testParams.put("Opt.NLOPT.Method", "NM");
}

TEST_F(Optimization_NonlinEqualityConstrainedTest, MMA_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "AugLag");
  testParams.put("Opt.NLOPT.Method", "MMA");
}


TEST_F(Optimization_NonlinInequalityConstrainedTest, COBYLA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "Native");
  testParams.put("Opt.NLOPT.Method", "COBYLA");
}


TEST_F(Optimization_NonlinInequalityConstrainedTest, NM_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "AugLag");
  testParams.put("Opt.NLOPT.Method", "NM");
}

TEST_F(Optimization_NonlinInequalityConstrainedTest, MMA_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.ConstraintHandler", "AugLag");
  testParams.put("Opt.NLOPT.Method", "MMA");
}
