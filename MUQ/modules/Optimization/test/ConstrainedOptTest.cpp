
#include "ConstrainedTestClasses.h"

using namespace muq::Optimization;
using namespace std;

TEST_F(Optimization_BoundConstrainedRoseTest, SD_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "SD_Line");
  testParams.put("Opt.ConstraintHandler", "AugLag");
}


TEST_F(Optimization_BoundConstrainedRoseTest, BFGS_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "BFGS_Line");
  testParams.put("Opt.ConstraintHandler", "AugLag");
}

TEST_F(Optimization_NonlinEqualityConstrainedTest, SD_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "SD_Line");
  testParams.put("Opt.ConstraintHandler", "AugLag");
}

TEST_F(Optimization_NonlinEqualityConstrainedTest, BFGS_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "BFGS_Line");
  testParams.put("Opt.ConstraintHandler", "AugLag");
}

TEST_F(Optimization_NonlinInequalityConstrainedTest, SD_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "SD_Line");
  testParams.put("Opt.ConstraintHandler", "AugLag");
}

TEST_F(Optimization_NonlinInequalityConstrainedTest, BFGS_AugLag)
{
  // set the method
  testParams.put("Opt.Method", "BFGS_Line");
  testParams.put("Opt.ConstraintHandler", "AugLag");
}
