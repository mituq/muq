
#include <iostream>
#include <math.h>

// include the google testing header
#include "gtest/gtest.h"

// include the covariance kernel headers
#include "MUQ/Geostatistics/CovKernel.h"
#include "MUQ/Geostatistics/MaternKernel.h"
#include "MUQ/Geostatistics/PowerKernel.h"
#include "MUQ/Geostatistics/RationalFifthKernel.h"
#include "MUQ/Geostatistics/SphericalKernel.h"

#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"

#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"

using namespace muq::Utilities;
using namespace muq::Geostatistics;
using namespace std;

/** A simple text fixture for testing the various covariance functions. */
class GeostatsCovKernelTest : public::testing::Test {
protected:

  // set up the mesh that we will use, as well as an empty covariance matrix
  virtual void SetUp()
  {
    // ////////////////////////////////
    // Set up for grid based tests

    // define a two dimensional grid
    Nx = 20;
    Ny = 20;
    double dx = 0.1;
    double dy = 0.1;

    // build the covariance matrix
    Eigen::MatrixXd CovMat;

    // build a mesh over the unit square in 2d
    auto lb                             = Eigen::Matrix<double, 2, 1>::Zero(2, 1);
    auto ub                             = Eigen::Matrix<double, 2, 1>::Ones(2, 1);
    Eigen::Matrix<unsigned int, 2, 1> N = { Nx, Ny }; // = Eigen::Matrix<unsigned int,2,1>::Zero(2,1);
    //N[0] = Nx;
    //N[1] = Ny;

    // create the mesh
    MeshPtr = make_shared < StructuredQuadMesh < 2 >> (lb, ub, N);


    // ////////////////////////////////
    // Set up for vector based tests


    // define a two dimensional grid
    x.resize(Nx * Ny, Eigen::VectorXd::Zero(2)); // vector to hold x positions of nodes
    for (unsigned int elex = 0; elex < Nx; ++elex) {
      for (unsigned int eley = 0; eley < Ny; ++eley) {
        x[eley + elex * Ny][0] = elex * dx;
        x[eley + elex * Ny][1] = eley * dy;
      }
    }
  }

  // store the mesh
  std::shared_ptr < Mesh < 2 >> MeshPtr;

  // store the covariance
  Eigen::MatrixXd CovMat;

  vector<Eigen::VectorXd> x;

  unsigned int Nx, Ny;
};

TEST_F(GeostatsCovKernelTest, PowerBasicCreation)
{
  // define the length scale
  double L = 0.7;

  // define the power
  double P = 1.5;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, P, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(MeshPtr);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}

TEST_F(GeostatsCovKernelTest, PowerVectorCreation)
{
  // define the length scale
  double L = 0.7;

  // define the power
  double P = 1.5;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, P, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(x);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}


TEST_F(GeostatsCovKernelTest, RationalFifthBasicCreation)
{
  // define the length scale
  double C = 0.7;

  // define the variance
  double sig = 1.2;

  // create the kernel
  auto testKern = make_shared<RationalFifthKernel>(C, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(MeshPtr);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}

TEST_F(GeostatsCovKernelTest, RationalFifthVectorCreation)
{
  // define the length scale
  double C = 0.7;

  // define the variance
  double sig = 1.2;

  // create the kernel
  auto testKern = make_shared<RationalFifthKernel>(C, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(x);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}


TEST_F(GeostatsCovKernelTest, MaternBasicCreation)
{
  // define the length scale
  double Length = 0.7;

  // define the decay
  double rho = 1.5;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<MaternKernel>(Length, rho, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(MeshPtr);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}

TEST_F(GeostatsCovKernelTest, MaternVectorCreation)
{
  // define the length scale
  double Length = 0.7;

  // define the decay
  double rho = 1.5;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<MaternKernel>(Length, rho, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(x);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}


TEST_F(GeostatsCovKernelTest, SphericalBasicCreation)
{
  // define the length scale
  double L = 0.7;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<SphericalKernel>(L, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(MeshPtr);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}

TEST_F(GeostatsCovKernelTest, SphericalVectorCreation)
{
  // define the length scale
  double L = 0.7;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<SphericalKernel>(L, sig);

  // fill in the covariance matrix
  CovMat = testKern->BuildCov(x);

  // make sure the diagonal is what we expect
  for (unsigned int i = 0; i < Nx * Ny; ++i) {
    EXPECT_NEAR(CovMat(i, i), sig, 1e-8);
  }
}


TEST_F(GeostatsCovKernelTest, QuadPts1dTest)
{
  // define the length scale
  double L = 0.2;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, 2.0, sig);

  // build a rectangular mesh
  Eigen::Matrix<double, 1, 1> lb = Eigen::Matrix<double, 1, 1>::Zero(1, 1);
  Eigen::Matrix<double, 1, 1> ub = Eigen::Matrix<double, 1, 1>::Ones(1, 1);

  // get the quadrature points and weights
  Eigen::Matrix<double, 1, Eigen::Dynamic> pts;
  Eigen::Matrix<double, 1, Eigen::Dynamic> wts;

  testKern->GetQuadLocs<1>(30, pts, wts, lb, ub, 'L');

  // try a test integral to see if everything is working just fine
  double integral = 0;
  for (unsigned int i = 0; i < pts.cols(); ++i) {
    integral += sin(pts(0, i)) * wts(0, i);
  }

  // make sure the integral is what we know is the true integral
  EXPECT_NEAR(-cos(1) + 1, integral, 1e-3);
}

TEST_F(GeostatsCovKernelTest, QuadPts2dTest)
{
  // define the length scale
  double L = 0.2;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, 2.0, sig);

  // build a rectangular mesh
  Eigen::Matrix<double, 2, 1> lb = Eigen::Matrix<double, 2, 1>::Zero(2, 1);
  Eigen::Matrix<double, 2, 1> ub = Eigen::Matrix<double, 2, 1>::Ones(2, 1);

  // get the quadrature points and weights
  Eigen::Matrix<double, 2, Eigen::Dynamic> pts;
  Eigen::Matrix<double, 1, Eigen::Dynamic> wts;

  testKern->GetQuadLocs<2>(30, pts, wts, lb, ub, 'L');

  // try a test integral to see if everything is working just fine
  double integral = 0;
  for (unsigned int i = 0; i < pts.cols(); ++i) {
    integral += sin(pts(0, i)) * cos(pts(1, i)) * wts(0, i);
  }

  // make sure the integral is what we know is the true integral
  EXPECT_NEAR(-sin(1) * (cos(1) - 1), integral, 1e-3);
}


TEST_F(GeostatsCovKernelTest, KL1dRectangularMesh)
{
  // define the length scale
  double L = 0.2;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, 2.0, sig);

  // build a rectangular mesh
  Eigen::Matrix<double, 1, 1> lb      = Eigen::Matrix<double, 1, 1>::Zero(1, 1);
  Eigen::Matrix<double, 1, 1> ub      = Eigen::Matrix<double, 1, 1>::Ones(1, 1);
  unsigned int Nall                   = 50;
  Eigen::Matrix<unsigned int, 1, 1> N = Nall * Eigen::Matrix<unsigned int, 1, 1>::Ones(1, 1);

  shared_ptr < Mesh < 1 >> MeshPtr = make_shared < StructuredQuadMesh < 1 >> (lb, ub, N);

  // build the first few bases using the Nystrom method
  unsigned int NumBasis = 10;
  Eigen::MatrixXd Bases = testKern->BuildKL(MeshPtr, NumBasis);
}


TEST_F(GeostatsCovKernelTest, KL2dRectangularMesh)
{
  // define the length scale
  double L = 0.2;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, 2.0, sig);

  // build a rectangular mesh
  Eigen::Matrix<double, 2, 1> lb      = Eigen::Matrix<double, 2, 1>::Zero(2, 1);
  Eigen::Matrix<double, 2, 1> ub      = Eigen::Matrix<double, 2, 1>::Ones(2, 1);
  unsigned int Nall                   = 20;
  Eigen::Matrix<unsigned int, 2, 1> N = Nall * Eigen::Matrix<unsigned int, 2, 1>::Ones(2, 1);

  shared_ptr < Mesh < 2 >> MeshPtr = make_shared < StructuredQuadMesh < 2 >> (lb, ub, N);


  // build the first few bases using the Nystrom method
  unsigned int NumBasis = 4;
  Eigen::MatrixXd Bases = testKern->BuildKL(MeshPtr, NumBasis);
}


TEST_F(GeostatsCovKernelTest, KL1dRectangularMesh_ShortLength)
{
  // define the length scale
  double L = 0.005;

  // define the variance
  double sig = 1.0;

  // create the kernel
  auto testKern = make_shared<PowerKernel>(L, 2.0, sig);

  // build a rectangular mesh
  Eigen::Matrix<double, 1, 1> lb      = Eigen::Matrix<double, 1, 1>::Zero(1, 1);
  Eigen::Matrix<double, 1, 1> ub      = Eigen::Matrix<double, 1, 1>::Ones(1, 1);
  unsigned int Nall                   = 100;
  Eigen::Matrix<unsigned int, 1, 1> N = Nall * Eigen::Matrix<unsigned int, 1, 1>::Ones(1, 1);

  shared_ptr < Mesh < 1 >> MeshPtr = make_shared < StructuredQuadMesh < 1 >> (lb, ub, N);

  // build the first few bases using the Nystrom method
  unsigned int NumBasis = 10;
  Eigen::MatrixXd Bases = testKern->BuildKL(MeshPtr, NumBasis);
}


