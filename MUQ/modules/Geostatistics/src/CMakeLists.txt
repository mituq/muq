
# define the source files in this directory that we want to include in the
#   geostats library
SET(Geostats_SOURCES
	CovKernel.cpp
    IsotropicCovKernel.cpp
    PowerKernel.cpp
    MaternKernel.cpp
    SphericalKernel.cpp
    CompactPolynomialKernel.cpp
	RationalFifthKernel.cpp
    AnisotropicPowerKernel.cpp
    AnisotropicPowerKernelNugget.cpp
  )


##########################################
#  Objects to build

if(MUQ_USE_PYTHON)
  set(Geostats_SOURCES ${Geostats_SOURCES}
    ../python/GeostatsPythonLibrary.cpp 
    )
endif(MUQ_USE_PYTHON)


# Define the Geostats library
ADD_LIBRARY(muqGeostatistics ${Geostats_SOURCES})
TARGET_LINK_LIBRARIES(muqGeostatistics muqUtilities muqModelling muqOptimization ${MUQ_LINK_LIBS})

if(APPLE AND MUQ_USE_PYTHON)
    set_property(TARGET muqGeostatistics PROPERTY PREFIX "lib")
    set_property(TARGET muqGeostatistics PROPERTY OUTPUT_NAME "muqGeostatistics.so")
    set_property(TARGET muqGeostatistics PROPERTY SUFFIX "")
    set_property(TARGET muqGeostatistics PROPERTY SOVERSION "32.1.2.0")
endif(APPLE AND MUQ_USE_PYTHON)

install(TARGETS muqGeostatistics
    EXPORT MUQDepends
    LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
    ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")
