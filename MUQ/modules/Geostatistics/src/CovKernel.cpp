#include "MUQ/Geostatistics/CovKernel.h"

#include <assert.h>
#include <iostream>

#include <boost/property_tree/ptree.hpp>

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include "MUQ/Utilities/mesh/Mesh.h"

#include "MUQ/Utilities/Hmatrix/Hmatrix.h"
#include "MUQ/Utilities/LanczosEigenSolver.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Geostatistics;

/** Construct the covariance matrix using the points (x,y) stored in the position vectors.  The result is written into
 *  the Cov matrix.*/
Eigen::MatrixXd CovKernel::BuildCov(const std::vector<Eigen::VectorXd>& x, const std::vector<Eigen::VectorXd>& y) const
{
  // get the size of the covariance
  unsigned int M = x.size();
  unsigned int N = y.size();


  // create the output covariance matrix
  Eigen::MatrixXd CovOut;

  // if N is zero, no y was passed and we will create a symmetric covariance based only on x
  if (N == 0) {
    // the output matrix will be square in this case
    N      = M;
    CovOut = Eigen::MatrixXd::Zero(M, N);

    // ////////////
    // Fill in the lower triangular portion

    // column loop
    for (unsigned int j = 0; j < N; ++j) {
      // row loop
      for (unsigned int i = j; i < M; ++i) {
        CovOut(i, j) = Kernel(x[i], x[j]);
      }
    }

    // ////////////
    // Copy lower triangular portion to upper triangular part

    // column loop
    for (unsigned int j = 0; j < N; ++j) {
      // row loop
      for (unsigned int i = 0; i < j; ++i) {
        CovOut(i, j) = CovOut(j, i);
      }
    }


    // compute the covariance between the points in x and the points in y
  } else {
    CovOut = Eigen::MatrixXd::Zero(M, N);

    // column loop
    for (unsigned int j = 0; j < N; ++j) {
      // row loop
      for (unsigned int i = 0; i < M; ++i) {
        CovOut(i, j) = Kernel(x[i], y[j]);
      }
    }
  }

  // add a little nugget to ensure matrix is SPD
  CovOut += 1.0e-10 * Eigen::MatrixXd::Identity(M, N);

  // return the constructed covariance matrix
  return CovOut;
}

Eigen::MatrixXd CovKernel::BuildCovSpatialDerivative(Eigen::MatrixXd const& xs, Eigen::VectorXd const& x)
{
  Eigen::MatrixXd deriv = Eigen::MatrixXd::Zero(xs.rows(), xs.cols());


  for (int i = 0; i < deriv.cols(); ++i) {
    deriv.col(i) = KernelSpatialDerivative(xs.col(i), x);
  }

  return deriv;
}

#if MUQ_PYTHON==1

boost::python::list CovKernel::PyBuildCovOneInput(boost::python::list const& pyx)
{
  vector<Eigen::VectorXd> x = PythonListToVector(pyx);

  return GetPythonMatrix(BuildCov(x));
}

boost::python::list CovKernel::PyBuildCovTwoInputs(boost::python::list const& pyx, boost::python::list const& pyy)
{
  vector<Eigen::VectorXd> x = PythonListToVector(pyx);
  vector<Eigen::VectorXd> y = PythonListToVector(pyy);

  return GetPythonMatrix(BuildCov(x, y));
}

#endif

/** Construct the covariance matrix using a general mesh */
template<unsigned int dim>
Eigen::MatrixXd CovKernel::BuildCov(std::shared_ptr < muq::Utilities::Mesh < dim >>& MeshPtr) const
{
  // make sure all the dimensions match
  unsigned int Neles = MeshPtr->NumEles();

  Eigen::MatrixXd Cov = Eigen::MatrixXd::Zero(Neles, Neles);

  // outer loop -- column index of covariance matrix
  Eigen::VectorXd Pos1;
  Eigen::VectorXd Pos2;

  for (unsigned int col = 0; col < Neles; ++col) {
    Pos1 = MeshPtr->GetElePos(col);

    // inner loop -- row index of covariance matrx
    for (unsigned int row = col; row < Neles; ++row) {
      Pos2 = MeshPtr->GetElePos(row);

      // set the covariance
      Cov(row, col) = Kernel(Pos1, Pos2);
      Cov(col, row) = Cov(row, col);
    }
  }

  Cov += 1.0e-10 * Eigen::MatrixXd::Identity(Neles, Neles);

  return Cov;
}

/** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh. */
template<unsigned int dim>
Eigen::MatrixXd CovKernel::BuildKL(std::shared_ptr < muq::Utilities::Mesh < dim >>& MeshPtr,
                                   unsigned int                                            NumBasis)
{
  // check the mesh and the kernel for separability.
  if (MeshPtr->HasQuad()) {
    return BuildKL_TensorQuad(MeshPtr, NumBasis);

    // if the mesh is big enough, then it will be more efficient to use the Hierarchical matrix.  Otherwise, we should
    // just use the dense covariance.
  } else if (MeshPtr->NumEles() > 1e4) {
    return BuildKL_Hierarchical(MeshPtr, NumBasis);

    // use the dense covariance
  } else {
    return BuildKL_FullCov(MeshPtr, NumBasis);
  }
}

/** Construct the discretized Karhunen-Loeve modes of the kernel on the given points. */
Eigen::MatrixXd CovKernel::BuildKL(std::vector<Eigen::VectorXd> const& pts,
                                   unsigned int                        NumBasis)
{
  return BuildKL_FullCov(pts, NumBasis);
}

/** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh. */
template<unsigned int dim>
Eigen::MatrixXd CovKernel::BuildKL_TensorQuad(std::shared_ptr < muq::Utilities::Mesh < dim >>& MeshPtr,
                                              unsigned int                                            NumBasis) const
{
  // get an approximate "lengthscale" of the kernel
  double Lapp = EstimateLength();

  // figure out the quadrature order that we will use (this is just a heuristic that seems to work alright)
  unsigned int QuadOrder = std::max(int(3 * NumBasis), int(std::floor(4 / Lapp)));

  // We will use the Nystrom method to solve for the KL modes.

  // get the quadrature nodes and weights
  Eigen::Matrix<double, dim, Eigen::Dynamic> qpts;
  Eigen::Matrix<double, 1, Eigen::Dynamic>   qwts;

  Eigen::Matrix<double, dim, Eigen::Dynamic> lb = MeshPtr->GetLowerBounds();
  Eigen::Matrix<double, dim, Eigen::Dynamic> ub = MeshPtr->GetUpperBounds();

  GetQuadLocs<dim>(QuadOrder, qpts, qwts, lb, ub, 'L');

  Eigen::Matrix<double, 1, Eigen::Dynamic> sqrtwts = qwts.cwiseSqrt();

  // get the covariance within the quadrature points
  Eigen::MatrixXd QQcov = BuildCov(qpts).cwiseProduct(sqrtwts.transpose() * sqrtwts);

  // get the mesh element locations and build the covariance between them and the quadrature points
  Eigen::Matrix<double, dim, Eigen::Dynamic> mpts = MeshPtr->GetAllElePos();

  // build the covariance between qpts and mpts
  Eigen::MatrixXd MQcov = BuildCov(mpts, qpts);

  // compute the eigenvalues and eigenvectors of the quadrature point covariance
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> qEigSolver;
  qEigSolver.compute(QQcov);

  // check to make sure the eigensolve was successful
  assert(qEigSolver.info() == Eigen::Success);

  // get the eigenvalues and eigenvectors from the solver
  Eigen::MatrixXd QEigVecs = qEigSolver.eigenvectors().topRightCorner(QQcov.rows(), NumBasis);
  Eigen::VectorXd QEigVals = qEigSolver.eigenvalues().tail(NumBasis);

  // scale each eigenvector by the sqrt of the quadrature weights
  QEigVecs = (sqrtwts.asDiagonal() * QEigVecs).eval();

  // compute scaled kl modes
  Eigen::MatrixXd modes = MQcov * QEigVecs * QEigVals.cwiseSqrt().cwiseInverse().asDiagonal();

  // now flip the modes so they are in order of descending eigenvalue
  for (unsigned int i = 0; i < floor(NumBasis / 2.0); ++i) {
    modes.col(i).swap(modes.col(NumBasis - 1 - i));
  }


  //std::cout << modes << std::endl;

  return modes;
}

/** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh using the full dense covariance
 *  matrix. */
template<unsigned int dim>
Eigen::MatrixXd CovKernel::BuildKL_FullCov(std::shared_ptr < muq::Utilities::Mesh < dim >>& MeshPtr,
                                           unsigned int                                            NumBasis) const
{
  // build the full covariance
  Eigen::MatrixXd FullCov = BuildCov(MeshPtr);

  // compute the eigenvalues of the full covariance
  //Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> EigSolver;
  LanczosEigenSolver EigSolver(NumBasis);

  EigSolver.compute(FullCov.selfadjointView<Eigen::Lower>());


  Eigen::MatrixXd QEigVecs = EigSolver.eigenvectors().topLeftCorner(FullCov.rows(), NumBasis);
  Eigen::VectorXd QEigVals = EigSolver.eigenvalues().head(NumBasis);

  // scale the eigenvectors by the sqrt of the eigenvalues
  QEigVecs = QEigVecs * QEigVals.cwiseSqrt().asDiagonal();

  // now flip the eigen vectors to be in order of descending eigenvalue
  //     for(unsigned int i=0; i<floor(NumBasis/2.0); ++i)
  //        QEigVecs.col(i).swap(QEigVecs.col(NumBasis-1-i));
  //
  return QEigVecs;
}

/** Construct the discretized Karhunen-Loeve modes of the kernel on the given points using the full dense covariance
 *  matrix. */
Eigen::MatrixXd CovKernel::BuildKL_FullCov(std::vector<Eigen::VectorXd> const& pts, unsigned int NumBasis) const
{
  // build the full covariance
  Eigen::MatrixXd FullCov = BuildCov(pts);

  // compute the eigenvalues of the full covariance
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> EigSolver;

  //LanczosEigenSolver EigSolver(NumBasis);

  EigSolver.compute(FullCov.selfadjointView<Eigen::Lower>());

  //Eigen::MatrixXd QEigVecs = EigSolver.eigenvectors().topLeftCorner(FullCov.rows(), NumBasis);
  //Eigen::VectorXd QEigVals = EigSolver.eigenvalues().head(NumBasis);

  Eigen::MatrixXd QEigVecs = EigSolver.eigenvectors().topRightCorner(FullCov.rows(), NumBasis);
  Eigen::VectorXd QEigVals = EigSolver.eigenvalues().tail(NumBasis);

  // scale the eigenvectors by the sqrt of the eigenvalues
  QEigVecs = QEigVecs * QEigVals.cwiseSqrt().asDiagonal();

  // now flip the eigen vectors to be in order of descending eigenvalue
  for (unsigned int i = 0; i < floor(NumBasis / 2.0); ++i) {
    QEigVecs.col(i).swap(QEigVecs.col(NumBasis - 1 - i));
  }

  return QEigVecs;
}

/** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh using a Hierarchical matrix
 *  approximation to the covariance matrix. */
template<unsigned int dim>
Eigen::MatrixXd CovKernel::BuildKL_Hierarchical(std::shared_ptr < muq::Utilities::Mesh < dim >>& MeshPtr,
                                                unsigned int                                            NumBasis)
{
  // the first step is to build a Hierarchical matrix approximating the full covariance matrix
  CovKernelPtr temp = shared_from_this();

  HMatrix<dim> HmatCov(temp, MeshPtr);

  // using the Hierarchical matrix, use a Lanczos eigensolver to find the eigenvalues and eigenvectors
  LanczosEigenSolver EigSolver(NumBasis);
  EigSolver.compute(HmatCov);


  Eigen::MatrixXd QEigVecs = EigSolver.eigenvectors().topLeftCorner(HmatCov.rows(), NumBasis);
  Eigen::VectorXd QEigVals = EigSolver.eigenvalues().head(NumBasis);

  std::cout << QEigVals.transpose() << std::endl;

  // scale the eigenvectors by the sqrt of the eigenvalues
  return QEigVecs * QEigVals.cwiseSqrt().asDiagonal();
}

std::shared_ptr<CovKernel::AvailableKernelMap> CovKernel::GetKernelMap()
{
  static std::shared_ptr<AvailableKernelMap> map;

  if (!map) {
    map = std::make_shared<AvailableKernelMap>(); //construct the static map
  }

  return map;
}

std::shared_ptr<CovKernel> CovKernel::Create(boost::property_tree::ptree& properties)
{
  //index into the map and call with the property tree
  return GetKernelMap()->at(properties.get<std::string>("Kernel.Type")) (properties);
}

/** Get quadrature locations for the mesh elements.  This is used to compute KL modes of a covariance kernel.
 *  \todo Implement StructuredQuadMesh<dim>::GetQuadLocs(unsigned int NumBasis)
 */

//template<unsigned int dim>
//void CovKernel::GetQuadLocs(unsigned int NumBasis, Eigen::Matrix<double,dim,Eigen::Dynamic> &pts,
// Eigen::Matrix<double,1,Eigen::Dynamic> &wts, const Eigen::Matrix<double,dim,1> &lb, const Eigen::Matrix<double,dim,1>
// &ub) const{
//
//    // create the multiindex that will allow us to compute quadrature weights and locations
//    unsigned int MaxOrder = 4*NumBasis;
//    TotalOrderMultiIndexFamily::Ptr Indices = TotalOrderMultiIndexFamily::Create(dim,MaxOrder-1);
//
//    // get the one dimensional nodes and weights
//    muq::polychaos::GaussLegendreQuadrature1D QuadRule;
//
//    // the nodes and weights will be returned as boost smart pointers
//    std::shared_ptr<Eigen::RowVectorXd> nodes(QuadRule.GetNodes(MaxOrder));
//    std::shared_ptr<Eigen::RowVectorXd> weights(QuadRule.GetWeights(MaxOrder));
//
//    // use the multiindex family to loop through the nodes and weights to compute the multi-dimensional nodes and
// weights
//    unsigned int NumDimPts = Indices->GetNumberOfIndices();
//    pts.resize(dim,NumDimPts);
//    wts.resize(1,NumDimPts);
//
//    // loop over all the higher dimensional quadrature points
//    for(unsigned int i=0; i<NumDimPts; ++i){
//
//        // initialize the weight to 1.0
//        wts(0,i) = 1.0;
//
//        // grab the current index set
//        Eigen::RowVectorXu TempInd = Indices->IndexToMulti(i);
//
//        // loop over the dimensions, computing the weights and nodes
//        for(unsigned int d=0; d<dim; ++d){
//            double L = ub[d]-lb[d];
//            pts(d,i) = lb[d] + 0.5*(1.0+(*nodes)[TempInd[d]])/L;
//            wts(0,i) *= (*weights)[TempInd[d]]*L*0.5;
//        }
//    }
//
//}


///** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh assuming both a rectangular mesh
// and a separable kernel.  The KL modes are computed in each direction and then tensor products are taken to build up
// the 2 or 3 dimensional modes. */
//template<unsigned int dim>
//Eigen::MatrixXd CovKernel::BuildKL_Separable(std::shared_ptr<muq::Utilities::Mesh<dim>> &MeshPtr, unsigned int
// NumBasis) const{
//
//    // first things first, get the node locations in each direction
//    std::vector<Eigen::VectorXd> NodePos = MeshPtr->GetSeparableMeshes();
//
//    // get the number of elements in each direction
//    Eigen::RowVectorXu Sizes(dim);
//    for(unsigned int d=0; d<dim; ++d)
//        Sizes[d] = NodePos[d].size();
//
//    // now build the KL modes in each direction
//    std::vector<Eigen::VectorXd> OneDimModes(dim);
//    for(unsigned int d=0; d<dim; ++d)
//        OneDimModes[d] = BuildKL1d(NodePos[d],NumBasis);
//
//    // use a max-order restricted multiindex to find the tensor product of the KL modes
//    FullTensorMultiIndexFamily::Ptr Indices = FullTensorMultiIndexFamily::Create(dim,Sizes);
//
//    unsigned int NumDimPts = Indices->GetNumberOfIndices();
//    Eigen::MatrixXd FullModes = Eigen::MatrixXd::Zero(MeshPtr->NumEles(),NumDimPts);
//
//
//    return FullModes;
//
//}

///** When using BuildKL_Separable we need to compute KL modes in one dimension.  This function uses one dimensional
// Gauss-Legendre quadrature within the Nystrom method to compute the modes. */
//Eigen::MatrixXd CovKernel::BuildKL1d(const Eigen::VectorXd &NodePos, unsigned int NumBasis) const{
//
//
//}

//template void CovKernel::GetQuadLocs<1>(unsigned int NumBasis, Eigen::Matrix<double,1,Eigen::Dynamic> &pts,
// Eigen::Matrix<double,1,Eigen::Dynamic> &wts, const Eigen::Matrix<double,1,1> &lb, const Eigen::Matrix<double,1,1>
// &ub) const;
//template void CovKernel::GetQuadLocs<2>(unsigned int NumBasis, Eigen::Matrix<double,2,Eigen::Dynamic> &pts,
// Eigen::Matrix<double,1,Eigen::Dynamic> &wts, const Eigen::Matrix<double,2,1> &lb, const Eigen::Matrix<double,2,1>
// &ub) const;
//template void CovKernel::GetQuadLocs<3>(unsigned int NumBasis, Eigen::Matrix<double,3,Eigen::Dynamic> &pts,
// Eigen::Matrix<double,1,Eigen::Dynamic> &wts, const Eigen::Matrix<double,3,1> &lb, const Eigen::Matrix<double,3,1>
// &ub) const;


template Eigen::MatrixXd CovKernel::BuildCov<1>(std::shared_ptr < muq::Utilities::Mesh < 1 >> &MeshPtr) const;
template Eigen::MatrixXd CovKernel::BuildCov<2>(std::shared_ptr < muq::Utilities::Mesh < 2 >> &MeshPtr) const;
template Eigen::MatrixXd CovKernel::BuildCov<3>(std::shared_ptr < muq::Utilities::Mesh < 3 >> &MeshPtr) const;

template Eigen::MatrixXd CovKernel::BuildKL<1>(std::shared_ptr < muq::Utilities::Mesh < 1 >> &MeshPtr,
                                               unsigned int NumBasis);
template Eigen::MatrixXd CovKernel::BuildKL<2>(std::shared_ptr < muq::Utilities::Mesh < 2 >> &MeshPtr,
                                               unsigned int NumBasis);
template Eigen::MatrixXd CovKernel::BuildKL<3>(std::shared_ptr < muq::Utilities::Mesh < 3 >> &MeshPtr,
                                               unsigned int NumBasis);

template Eigen::MatrixXd CovKernel::BuildKL_TensorQuad<1>(std::shared_ptr < muq::Utilities::Mesh < 1 >> &MeshPtr,
                                                          unsigned int NumBasis) const;
template Eigen::MatrixXd CovKernel::BuildKL_TensorQuad<2>(std::shared_ptr < muq::Utilities::Mesh < 2 >> &MeshPtr,
                                                          unsigned int NumBasis) const;
template Eigen::MatrixXd CovKernel::BuildKL_TensorQuad<3>(std::shared_ptr < muq::Utilities::Mesh < 3 >> &MeshPtr,
                                                          unsigned int NumBasis) const;

template Eigen::MatrixXd CovKernel::BuildKL_FullCov<1>(std::shared_ptr < muq::Utilities::Mesh < 1 >> &MeshPtr,
                                                       unsigned int NumBasis) const;
template Eigen::MatrixXd CovKernel::BuildKL_FullCov<2>(std::shared_ptr < muq::Utilities::Mesh < 2 >> &MeshPtr,
                                                       unsigned int NumBasis) const;
template Eigen::MatrixXd CovKernel::BuildKL_FullCov<3>(std::shared_ptr < muq::Utilities::Mesh < 3 >> &MeshPtr,
                                                       unsigned int NumBasis) const;

template Eigen::MatrixXd CovKernel::BuildKL_Hierarchical<1>(
  std::shared_ptr < muq::Utilities::Mesh < 1 >> &MeshPtr,
  unsigned int NumBasis);
template Eigen::MatrixXd CovKernel::BuildKL_Hierarchical<2>(
  std::shared_ptr < muq::Utilities::Mesh < 2 >> &MeshPtr,
  unsigned int NumBasis);
template Eigen::MatrixXd CovKernel::BuildKL_Hierarchical<3>(
  std::shared_ptr < muq::Utilities::Mesh < 3 >> &MeshPtr,
  unsigned int NumBasis);
