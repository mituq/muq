import unittest

import libmuqModelling
import libmuqUtilities
import libmuqPde

# something random to sense 
class SomethingToSense(libmuqPde.MeshParameter):
    def __init__(self, system, param):
        super(SomethingToSense, self).__init__(system, param)

    def ParameterEvaluate(self, x, y, z, var):
        if( var=="s1" ):
            return  1.0 + x * y + 2.0 * x * x - 3.0 * y * y
        if( var=="s2" ):
            return 2.0 - 2.0 * (1.0 - x) * y - 4.0 * x * x + 6.0 * y * y
        return 0.0

class SensorTest(unittest.TestCase):
    def testPointSensor(self):
        param = dict()

        param["mesh.type"] = "SquareMesh"
        param["mesh.Nx"]   = 25
        param["mesh.Ny"]   = 20
        param["mesh.Lx"]   = 1.0
        param["mesh.Ly"]   = 1.0

        # create a system of equations
        system = libmuqPde.GenericEquationSystems(param)

        param["MeshParameter.SystemName"]     = "SomethingToSense"
        param["MeshParameter.VariableNames"]  = "s1,s2"
        param["MeshParameter.VariableOrders"] = "1,2"

        # create a model of the basal topography
        toSense = SomethingToSense(system, param)

        # evaluate the field 
        result = toSense.Evaluate()

        # choose some random ponts 
        points = [None]*6
        for i in range(6):
            points[i] = [i/5.0, (5.0-i)/5.0]

        # create the sensor 
        sensor = libmuqPde.PointSensor(toSense.outputSize, points, "SomethingToSense", system)

        # evaluate the sensor 
        sensedPoints = sensor.Evaluate(result)

        for i in range(6):
            x = points[i][0]
            y = points[i][1]

            self.assertAlmostEqual(sensedPoints[2*i],   1.0 + x * y + 2.0 * x * x - 3.0 * y * y)
            self.assertAlmostEqual(sensedPoints[2*i+1], 2.0 - 2.0 * (1.0 - x) * y - 4.0 * x * x + 6.0 * y * y)

