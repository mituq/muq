#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Pde/MeshParameter.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Pde;

class NoInputsTest : public MeshParameter {
  public:

  NoInputsTest(string const& systemName, shared_ptr<GenericEquationSystems> const& system,
	       boost::property_tree::ptree const& para) :
    MeshParameter(systemName, system, para) {}
    
  private:

  virtual double ParameterEvaluate(double const x, double const y, double const z, string const& var) const override 
  {
    return x;
  }
};

REGISTER_MESH_PARAMETER(NoInputsTest)

class InputsTest : public MeshParameter {
  public:

  InputsTest(string const& systemName, shared_ptr<GenericEquationSystems> const& system,
	     boost::property_tree::ptree const& para) :
  MeshParameter(systemName, system, para) {}
    
  private:

virtual double ParameterEvaluate(double const x, double const y, double const z, string const& var) const override 
  {
    // input a (double since it one dimensional)
    const double a = system->GetEquationSystemsPtr()->parameters.get<double>(inputNames[0]);

    // input b (Eigen::VectorXd since it is multi dimensional
    const Eigen::VectorXd b = system->GetEquationSystemsPtr()->parameters.get<Eigen::VectorXd>(inputNames[1]);

    return b(1) * (x + a + b(0));
  }
};

REGISTER_MESH_PARAMETER(InputsTest)

TEST(MeshParameterTest, NoInputs) 
{
  boost::property_tree::ptree pt;

  // Parameters to create a mesh
  pt.put("mesh.type", "LineMesh");
  pt.put("mesh.N", 10);
  pt.put("mesh.L", 1.0);

  // create an equation system
  auto system = make_shared<GenericEquationSystems>(pt);
  
  // parameters for the spatially dependent parameter
  pt.put("MeshParameter.SystemName", "NoInputsTest");
  pt.put("MeshParameter.VariableNames", "x,y");
  pt.put("MeshParameter.VariableOrders", "1,2");

  // create the spatially dependent parameter
  auto mod = MeshParameter::Create(system, pt);

  // expected result 
  Eigen::VectorXd expected(32);
  expected << 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
              0.0, 0.1, 0.05, 0.2, 0.15, 0.3, 0.25, 0.4, 0.35, 0.5, 0.45, 0.6, 0.55, 0.7, 0.65, 0.8, 0.75, 0.9, 0.85, 1.0, 0.95;

  // evaluate the determinist parameter
  const Eigen::VectorXd result = mod->Evaluate();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, expected, result, 1.0e-12);
}

TEST(MeshParameterTest, Inputs) 
{
  boost::property_tree::ptree pt;

  // Parameters to create a mesh
  pt.put("mesh.type", "LineMesh");
  pt.put("mesh.N", 10);
  pt.put("mesh.L", 1.0);

  // create an equation system
  auto system = make_shared<GenericEquationSystems>(pt);
  
  // parameters for the spatially dependent parameter
  pt.put("MeshParameter.SystemName", "InputsTest");
  pt.put("MeshParameter.VariableNames", "x,y");
  pt.put("MeshParameter.VariableOrders", "1,2");
  pt.put("MeshParameter.InputNames", "a,b");
  pt.put("MeshParameter.InputSizes", "1,2");

  // create the spatially dependent parameter
  auto mod = MeshParameter::Create(system, pt);

  // expected result 
  Eigen::VectorXd expected(32);
  expected << 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0,
              2.0, 2.1, 2.05, 2.2, 2.15, 2.3, 2.25, 2.4, 2.35, 2.5, 2.45, 2.6, 2.55, 2.7, 2.65, 2.8, 2.75, 2.9, 2.85, 3.0, 2.95;

  // the inputs 
  Eigen::VectorXd a(1), b(2);
  a << 1.0; b << 1.0, 2.0;

  // evaluate the determinist parameter
  const Eigen::VectorXd result = mod->Evaluate(a, b);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, 2.0 * expected, result, 1.0e-12);
}

