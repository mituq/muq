#include "gtest/gtest.h"

#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Pde/MeshParameter.h"
#include "MUQ/Pde/FieldIntegrator1D.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Pde;
using namespace muq::Modelling;

class IntegrateThisField : public MeshParameter {
public:

  IntegrateThisField(string const                            & paraName,
                     shared_ptr<GenericEquationSystems> const& parasystem,
                     boost::property_tree::ptree const       & para) :
    MeshParameter(paraName, parasystem, para) {}

private:

  /// Evaluate the mesh parameter
  virtual inline libMesh::Number ParameterEvaluate(double const x, double const, double const,
                                                   string const&) const override
  {
    return 3.0 * x * x + 2.0 * x - 1.0;
  }
};

REGISTER_MESH_PARAMETER(IntegrateThisField)

TEST(FieldIntegrator1DTest, BasicTest)
{
  boost::property_tree::ptree param;

  // parameters to create a mesh
  param.put("mesh.type", "LineMesh");
  param.put("mesh.N", 50);
  param.put("mesh.L", 1.0);

  // some of the PDE parameters
  param.put("PDE.Unknowns", "u");
  param.put("PDE.UnknownOrders", "2");
  param.put("PDE.GaussianQuadratureOrder", 4);
  param.put("PDE.Name", "FieldIntegrator1D");
  param.put("PDE.Parameters", "IntegrateThisField");
  param.put("PDE.IsScalarParameter", "false");
  param.put("PDE.NeedSpatialGradient", "IntegrateThisField");

  // create an equation system (holds the PDEs, spatially dependent parameters, and mesh)
  auto system = make_shared<GenericEquationSystems>(param);

  param.put("MeshParameter.SystemName", "IntegrateThisField");
  param.put("MeshParameter.VariableNames", "field");
  param.put("MeshParameter.VariableOrders", "1");

  auto uMod = MeshParameter::Create(system, param);

  Eigen::VectorXi inputSizes(1);
  inputSizes << uMod->outputSize;

  vector<Eigen::VectorXd> pnts(4);
  pnts[0] = 0.25 * Eigen::VectorXd::Ones(1);
  pnts[1] = 0.5 * Eigen::VectorXd::Ones(1);
  pnts[2] = 0.75 * Eigen::VectorXd::Ones(1);
  pnts[3] = 1.0 * Eigen::VectorXd::Ones(1);

  // create a nontransient PDE
  auto pde = ConstructPDE(inputSizes, pnts, system, param);

  // inputs
  vector<Eigen::VectorXd> ins(2);
  ins[0] = Eigen::VectorXd::Ones(pde->inputSizes(0));
  ins[1] = uMod->Evaluate();

  // evalaute
  Eigen::VectorXd result = pde->Evaluate(ins);

  // check result
  EXPECT_NEAR(result(0), pow(0.25, 3.0) + pow(0.25, 2.0) - 0.25, 1.0e-3);
  EXPECT_NEAR(result(1), pow(0.5, 3.0) + pow(0.5, 2.0) - 0.5,    1.0e-3);
  EXPECT_NEAR(result(2), pow(0.75, 3.0) + pow(0.75, 2.0) - 0.75, 1.0e-3);
  EXPECT_NEAR(result(3), pow(1.0, 3.0) + pow(1.0, 2.0) - 1.0,    1.0e-3);
}
