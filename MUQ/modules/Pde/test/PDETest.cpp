#include "MUQ/Pde/PDE.h"

#include "gtest/gtest.h"

#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Pde/PDEModPiece.h"
#include "MUQ/Pde/MeshParameter.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Pde;
using namespace muq::Modelling;

/// Should have exact solution f(x,y) = x^2 + y^2
class PoissonExample : public Nontransient {
public:

PoissonExample(Eigen::VectorXi const& inputSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
  Nontransient(inputSizes, system, para) {}

private:

  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline returnScalar ResidualImpl(string const& varName, libMesh::Real const phi, libMesh::RealGradient const& dphi, vector<unknownScalar> const& soln, vector<vector<unknownScalar> > const& grad) const
  {
    // get the parameter number form the system
    const unsigned int var = vars.right.at(varName);

    const parameterScalar force = GetScalarParameter<parameterScalar>("force");
    const parameterScalar kappa = GetScalarParameter<parameterScalar>("kappa");

    returnScalar resid = 0.0;

    for (unsigned int i = 0; i < 3; ++i) {
      resid -= dphi(i) * grad[var][i];
    }

    return kappa * resid - phi * force;
  }

  RESIDUAL(ResidualImpl)

  inline virtual bool IsDirichlet(libMesh::Point const& point) const override 
  {
    if (point(0) == 1.0) { // right side is dirichlet
      return true;
    }
    return false;
  }

  inline virtual double DirichletBCs(string const&, libMesh::Point const& pt) const override
  {
    return pow(pt(0), 2.0) + pow(pt(1), 2.0);
  }

  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline vector<returnScalar> NeumannBCs(string const& varName, libMesh::Point const& pt, vector<unknownScalar> const& soln, vector<vector<unknownScalar> > const& grad) const
  {
    vector<returnScalar> neumman(3, 0.0);

    neumman[0] = 2.0 * pt(0);
    neumman[1] = 2.0 * pt(1);

    return neumman;
  }

  NEUMANN(NeumannBCs)
};

REGISTER_PDE(PoissonExample)

class TransientPoissonExample : public Transient {
public:

TransientPoissonExample(Eigen::VectorXi const& inputSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
  Transient(inputSizes, system, para) {}

  private:

  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline returnScalar ResidualImpl(string const& varName, libMesh::Real const phi, libMesh::RealGradient const& dphi, vector<unknownScalar> const& soln, vector<vector<unknownScalar> > const& grad) const
  {
    const unsigned int var = vars.right.at(varName);

    const parameterScalar force = GetScalarParameter<parameterScalar>("force");
    const parameterScalar kappa = GetScalarParameter<parameterScalar>("kappa");

    returnScalar resid = 0.0;

    for (unsigned int i = 0; i < 3; ++i) {
      resid -= dphi(i) * grad[var][i];
    }

    return kappa * resid - phi * force;
  }

  RESIDUAL(ResidualImpl)

  inline virtual bool IsDirichlet(libMesh::Point const& point) const override
  {
    if (point(0) == 1.0) { // right side is dirichlet
      return true;
    }
    return false;
  }

  template<typename scalar>
  inline scalar DirichletBCs(string const&, libMesh::Point const& pt, scalar time) const
  {
    return time * (pow(pt(0), 2.0) + pow(pt(1), 2.0));
  }

  TRANSIENT_DIRICHLET(DirichletBCs)

  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline vector<returnScalar> NeumannBCs(string const& varName, libMesh::Point const& pt, vector<unknownScalar> const& soln, vector<vector<unknownScalar> > const& grad) const
  {
    vector<returnScalar> neumman(3, 0.0);

    neumman[0] = 2.0 * pt(0);
    neumman[1] = 2.0 * pt(1);

    return neumman;
  }

  NEUMANN(NeumannBCs)

};

REGISTER_PDE(TransientPoissonExample)

/// A (spatially dependent) forcing parameter
class SpatialForcingExample : public MeshParameter {
public:

  SpatialForcingExample(string const                            & paraName,
                        shared_ptr<GenericEquationSystems> const& parasystem,
                        boost::property_tree::ptree const       & para) :
    MeshParameter(paraName, parasystem, para) {}

private:

  /// Evaluate the mesh parameter
  virtual inline libMesh::Number ParameterEvaluate(double const x, double const y, double const,
                                                   string const&) const override
  {
    return 4.0;
  }
};

REGISTER_MESH_PARAMETER(SpatialForcingExample)

class PDETest : public::testing::Test {
public:

  PDETest()
  {
    // parameters to create a mesh
    param.put("mesh.type", "SquareMesh");

    param.put("mesh.Nx", 5);
    param.put("mesh.Ny", 8);
    param.put("mesh.Lx", 1.0);
    param.put("mesh.Ly", 1.0);

    // some of the PDE parameters
    param.put("PDE.Unknowns", "u,v");
    param.put("PDE.UnknownOrders", "1,2");
    param.put("PDE.GaussianQuadratureOrder", 4);
    param.put("PDE.Parameters", "kappa,SpatialForcingExample");
    param.put("PDE.IsScalarParameter", "true,false");
    //param.put("PDE.NeedSpatialGradient", "SpatialForcingExample");

    // create an equation system (holds the PDEs, spatially dependent parameters, and mesh)
    system = make_shared<GenericEquationSystems>(param);

    param.put("MeshParameter.SystemName", "SpatialForcingExample");
    param.put("MeshParameter.VariableNames", "force");
    param.put("MeshParameter.VariableOrders", "2");

    auto forceMod = MeshParameter::Create(system, param);

    inputSizes << 1, forceMod->outputSize;

    Eigen::VectorXd forcing = forceMod->Evaluate();

    ins.resize(3);
    ins[1] = Eigen::VectorXd::Ones(1);
    ins[2] = forcing;
  }

  shared_ptr<HDF5Wrapper> hdf5file = make_shared<HDF5Wrapper>();

  const string filename = "modules/Pde/test/PDETest.h5";

  boost::property_tree::ptree param;

  shared_ptr<GenericEquationSystems> system;

  Eigen::Vector2i inputSizes;

  vector<Eigen::VectorXd> ins;
};

TEST_F(PDETest, NontransientPDE)
{
  // parameters for the PDE
  param.put("PDE.Name", "PoissonExample");

  // create a nontransient PDE
  auto pde = ConstructPDE(inputSizes, system, param);

  // initial guess
  ins[0] = Eigen::VectorXd::Zero(pde->inputSizes[0]);

  // quick check 
  assert(pde);

  // solve PDE
  const Eigen::VectorXd result = pde->Evaluate(ins);

  hdf5file->OpenFile(filename);

  // get the precomputed truth from file
  assert(hdf5file->DoesDataSetExist("NontransientPDETest/result"));
  const Eigen::VectorXd truthResult = hdf5file->ReadMatrix("NontransientPDETest/result");

  hdf5file->CloseFile();
  
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthResult, result, 1e-8);

  // print result
  pde->PrintToExodus("results/tests/NontransientPDETest.exo", result);

  // compute the gradient using AD (force)
  const Eigen::VectorXd gradForce = pde->Gradient(ins, Eigen::VectorXd::Ones(pde->outputSize), 1);

  EXPECT_NEAR(147.14838933780595, gradForce(0), 1.0e-12);

  // compute the gradient using AD (kappa)
  const Eigen::VectorXd gradKappa = pde->Gradient(ins, Eigen::VectorXd::Ones(pde->outputSize), 2);

  hdf5file->OpenFile(filename);

  // get the precomputed truth from file
  assert(hdf5file->DoesDataSetExist("NontransientPDETest/gradKappa"));
  const Eigen::VectorXd truthGradKappa = hdf5file->ReadMatrix("NontransientPDETest/gradKappa");

  hdf5file->CloseFile();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthGradKappa, gradKappa, 1e-8);
}

TEST_F(PDETest, TransientPDE)
{
  // parameters for the PDE
  param.put("PDE.TimeDependentMassMatrix", false); // defaults to false too
  param.put("PDE.Name", "TransientPoissonExample");

  // times to observe the output
  Eigen::VectorXd obsTimes = Eigen::VectorXd::LinSpaced(5, 0.0, 1.0);

  // create a nontransient PDE
  auto pde = ConstructPDE(inputSizes, obsTimes, system, param);

  // initial guess
  ins[0] = Eigen::VectorXd::Zero(pde->inputSizes[0]);

  // quick check 
  assert(pde);

  // solve PDE
  const Eigen::VectorXd result = pde->Evaluate(ins);

  hdf5file->OpenFile(filename);

  // get the precomputed truth from file
  assert(hdf5file->DoesDataSetExist("TransientPDETest/result"));
  const Eigen::VectorXd truthResult = hdf5file->ReadMatrix("TransientPDETest/result");

  hdf5file->CloseFile();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthResult, result, 1e-8);

  // print result
  pde->PrintToExodus("results/tests/TransientPDETest.exo", result);

  // compute the gradient using AD (kappa)
  const Eigen::VectorXd gradForce = pde->Gradient(ins, Eigen::VectorXd::Ones(pde->outputSize), 1);

  EXPECT_NEAR(438.62647149396298, gradForce(0), 1.0e-12);

  // compute the gradient using AD (kappa)
  const Eigen::VectorXd gradKappa = pde->Gradient(ins, Eigen::VectorXd::Ones(pde->outputSize), 2);

  hdf5file->OpenFile(filename);

  // get the precomputed truth from file
  assert(hdf5file->DoesDataSetExist("TransientPDETest/gradKappa"));
  const Eigen::VectorXd truthGradKappa = hdf5file->ReadMatrix("TransientPDETest/gradKappa");

  hdf5file->CloseFile();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, truthGradKappa, gradKappa, 1e-8);
}
