
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Geostatistics/PowerKernel.h"

#include "MUQ/Pde/KernelModel.h"

using namespace std;
using namespace muq::Pde;
using namespace muq::Utilities;
using namespace muq::Geostatistics;

TEST(KernelModel, BasicTest)
{
  double L   = 0.7; // length scale
  double P   = 1.5; // kernel power
  double sig = 1.0; // variance

  // create the kernel
  auto kernel = make_shared<PowerKernel>(L, P, sig);

  // dimension of the spatial "domain"
  const unsigned int Nx = 13;
  const unsigned int Ny = 8;

  // distance bewteen points
  const double dx = 1.0 / (Nx - 1.0);
  const double dy = 1.0 / (Ny - 1.0);

  // compute the points
  vector<Eigen::VectorXd> x(Nx * Ny, Eigen::Vector2d());
  for (unsigned int elex = 0; elex < Nx; ++elex) {
    for (unsigned int eley = 0; eley < Ny; ++eley) {
      x[eley + elex * Ny][0] = elex * dx;
      x[eley + elex * Ny][1] = eley * dy;
    }
  }

  auto kernelModel = make_shared<KernelModel>(x, Nx * Ny, kernel);

  Eigen::MatrixXd cov     = kernelModel->GetCovariance();
  Eigen::MatrixXd trueCov = kernel->BuildCov(x);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueCov, cov, 1e-10);
}

TEST(KernelModel, TruncatedTest)
{
  double L   = 0.7; // length scale
  double P   = 2.0; // kernel power
  double sig = 1.0; // variance

  // create the kernel
  auto kernel = make_shared<PowerKernel>(L, P, sig);

  // dimension of the spatial "domain"
  const unsigned int Nx = 13;
  const unsigned int Ny = 8;

  // number of basis to use in the approximation
  const unsigned int numBasis = (Nx * Ny) / 4;

  // distance bewteen points
  const double dx = 1.0 / (Nx - 1.0);
  const double dy = 1.0 / (Ny - 1.0);

  // compute the points
  vector<Eigen::VectorXd> x(Nx * Ny, Eigen::Vector2d());
  for (unsigned int elex = 0; elex < Nx; ++elex) {
    for (unsigned int eley = 0; eley < Ny; ++eley) {
      x[eley + elex * Ny][0] = elex * dx;
      x[eley + elex * Ny][1] = eley * dy;
    }
  }

  auto kernelModel = make_shared<KernelModel>(x, numBasis, kernel);

  Eigen::MatrixXd cov     = kernelModel->GetCovariance();
  Eigen::MatrixXd trueCov = kernel->BuildCov(x);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueCov, cov, 1e-6);
}

