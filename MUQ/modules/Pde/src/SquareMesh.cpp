#include "MUQ/Pde/SquareMesh.h"
#include "MUQ/Pde/LibMeshInit.h"

#include "libmesh/libmesh.h"

using namespace std;
using namespace muq::Pde;

REGISTER_MESH(SquareMesh)

SquareMesh::SquareMesh(boost::property_tree::ptree const& param) : GenericMesh(param)
{
  // get numerical mesh size
  const int Nx = param.get<int>("mesh.Nx", 0);
  const int Ny = param.get<int>("mesh.Ny", 0);

  // get dimensions of the (rectangular) domain
  const double Lx = param.get<double>("mesh.Lx", 0.0);
  const double Ly = param.get<double>("mesh.Ly", 0.0);
  const double Bx = param.get<double>("mesh.Bx", 0.0);
  const double By = param.get<double>("mesh.By", 0.0);

  // create a mesh
  assert(LibMeshInit::libmeshInit);
  mesh = make_shared<libMesh::Mesh>(LibMeshInit::libmeshInit->comm());

  libMesh::MeshTools::Generation::build_square(*mesh, Nx, Ny, Bx, Lx, By, Ly, libMesh::QUAD9);
}

