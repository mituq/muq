#This demo builds all sources into a library, and then compiles and links  main  against the library and all Trilinos libraries.

SET(PDE_SOURCES
  GenericMesh.cpp
  SquareMesh.cpp
  LineMesh.cpp

  LibMeshInit.cpp
  
  ElementInformation.cpp
  
  GenericEquationSystems.cpp
  
  MeshParameter.cpp
  
  KernelModel.cpp
  MeshKernelModel.cpp
  
  PDEModPiece.cpp
  
  PDE.cpp
  Transient.cpp
  Nontransient.cpp
  
  FieldIntegrator1D.cpp
  
  PointObserver.cpp
  PointSensor.cpp
  )

if(MUQ_USE_PYTHON)
  set(PDE_SOURCES ${PDE_SOURCES}
    ../python/PDEPythonLibrary.cpp

    ../python/PDEModPiecePython.cpp

    ../python/LibMeshInitPython.cpp
    
    ../python/MeshParameterPython.cpp
    ../python/KernelModelPython.cpp
    ../python/MeshKernelModelPython.cpp
    )
endif(MUQ_USE_PYTHON)

## ## Define library
ADD_LIBRARY(muqPde ${PDE_SOURCES})
TARGET_LINK_LIBRARIES(muqPde muqUtilities muqModelling muqGeostatistics ${MUQ_LINK_LIBS})

if(APPLE AND MUQ_USE_PYTHON)
    set_property(TARGET muqPde PROPERTY PREFIX "lib")
    set_property(TARGET muqPde PROPERTY OUTPUT_NAME "muqPde.so")
    set_property(TARGET muqPde PROPERTY SUFFIX "")
    set_property(TARGET muqPde PROPERTY SOVERSION "32.1.2.0")
endif(APPLE AND MUQ_USE_PYTHON)

install(TARGETS muqPde
  EXPORT MUQDepends
  LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
  ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")

