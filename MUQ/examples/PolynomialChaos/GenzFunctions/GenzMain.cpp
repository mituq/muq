/** \example GenzMain.cpp

    <h1>Introduction</h1>
    <p>This example shows convergence rate of the Polynomial Chaos expansions of the Genz functions. The output of this benchmark is written in GenzResultsAdaptive.h5. See the README contained in this folder for more instructions.</p>

    <p>Usage: <pre>GenzMain \<type\> \<fnum\> \<dim\> \<ntrials\> \<timescale\></pre><\br>
    <pre>\<type\></pre>: standard (0) or modified (1) Genz Functions</br>
    <pre>\<fnum\></pre>: (1-6) function number</br>
    <pre>\<dim\></pre>: dimension</br>
    <pre>\<ntrials\></pre>: number of trials for each function</br>
    <pre>\<timescale\></pre>: integer multiplier for the total work effort in the construction. Default is 1.</p>

    <p>The standard Genz functions are defined in:<\br>
    Genz, A. (1984). Testing multidimensional integration routines. Proc. of International Conference on Tools, Methods and Languages for Scientific and Engineering Computation, 81–94.<\br></p>
    
    <p>The modified Genz functions are defined in:<\br>
    Bigoni, D., Engsig-Karup, A. P., & Marzouk, Y. M. (). Spectral tensor-train decomposition, 28. Numerical Analysis. Retrieved from http://arxiv.org/abs/1405.5713<\p>

    <p>The main difference between the standard and the modified Genz functions is that in the standard Genz function the paramters \${\bf c},{\bf w}\$ are normalized, i.e. \$\vert {\bf c} \vert_{l^1} = \vert {\bf w} \vert_{l^1} = 1 \$, whereas in the modified Genz functions this normalization is not applied.</p>
 **/

#include <boost/property_tree/ptree.hpp>

#include <assert.h>
#include <fstream>
#include <iostream>
#include <math.h>

#include <Eigen/Core>

#include <MUQ/Modelling/CachedModPiece.h>
#include <MUQ/Approximation/smolyak/SmolyakPCEFactory.h>
#include <MUQ/Approximation/utilities/PolychaosXMLHelpers.h>
#include <MUQ/Utilities/RandomGenerator.h>
#include <MUQ/Utilities/HDF5Wrapper.h>
#include <MUQ/Utilities/LogConfig.h>

#include "GenzMain.hpp"
#include "GenzFunctions.hpp"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Approximation;
using namespace muq::Utilities;

int main(int argc, char **argv)
{
  //Initialization boilerplate for logging. Works regardless of whether GLOG is installed.
  InitializeLogging(argc, argv);

  unsigned int type;
  unsigned int fnum;
  unsigned int dim;
  unsigned int ntrials;
  unsigned int timescale;
  if (argc != 6)
    {
      cout << "Input error. Usage:" << endl;
      cout << "  GenzMain <type> <fnum> <dim> <ntrials> <timescale>" << endl;
      cout << "  type: standard (0) or modified (1) Genz Functions" << endl;
      cout << "  fnum: (1-6) function number" << endl;
      cout << "  dim: dimensions" << endl;
      cout << "  ntrials: number of trials for each function" << endl;
      cout << "  timescale: multiplier for the adapting times" << endl;
      exit(EXIT_FAILURE);
    }
  else
    {
      type = stoi( argv[1] );
      fnum = stoi( argv[2] );
      dim = stoi( argv[3] );
      ntrials = stoi( argv[4] );
      timescale = stoi( argv[5] );
    }
  
  RunAdaptiveGenzBenchmarking(type, fnum, dim, ntrials, timescale );
  
};

/**
   Run the benchmark.
 **/
void RunAdaptiveGenzBenchmarking(unsigned int type, 
                                 unsigned int fnum, 
                                 unsigned int dim, 
                                 unsigned int numTrials,
                                 unsigned int timescale)
{

  unsigned int initialOrder = 1;

  //Create subtrees and tree
  boost::property_tree::ptree ptree;
  boost::property_tree::ptree subtree;
  subtree.put("index", 1);
  subtree.put("type", "Legendre");
  subtree.put("count", dim);
  ptree.add_child("Variable", subtree);
  
  //Create the variables collection
  auto variableCollection = ConstructVariableCollection(ptree);
  
  Eigen::VectorXd timeToAdapt(8);
  timeToAdapt << 1e-6 , 1e-5 ,1e-4 , 1e-3 , 1e-2 , 1e-1 , 1. , 10.;
  if (fnum == 6)
    //For discontinuous function adapt for less in order to avoid
    //exceeding the maximum polynomial order
    timeToAdapt /= 10.;
  timeToAdapt *= static_cast<double>(timescale);
  
  //define name and types
  string genzType;
  switch(type)
    {
    case 0:
      genzType = "Standard";
      break;
    case 1:
      genzType = "Modified";
      break;
    default:
      assert(false);
    };

  string genzName;
  switch(fnum)
    {
    case 1:   
      genzName = "Oscillatory";
      break;
    case 2:
      genzName = "ProductPeak";
      break;
    case 3:
      genzName = "CornerPeak";
      break;
    case 4:
      genzName = "Gaussian";
      break;
    case 5:
      genzName = "Continuous";
      break;
    case 6:
      genzName = "Discontinuous";
      break;
    default:
      assert(false);
    };
  string outFileName = "GenzResultsAdaptive.h5";

  Eigen::MatrixXd fevals = Eigen::MatrixXd::Zero(numTrials, timeToAdapt.size());
  Eigen::MatrixXd LinfErr = Eigen::MatrixXd::Zero(numTrials, timeToAdapt.size());
  Eigen::MatrixXd L2Err = Eigen::MatrixXd::Zero(numTrials, timeToAdapt.size());

  cout << "Starting: " << genzType << " " << genzName << endl;
      
  //loop over trials
  for(unsigned int i=0; i<numTrials; ++i)
    {
      cout << "Trial " << i+1 << "/" << numTrials << endl;
          
      //Create an instance of the ModPiece
      shared_ptr<OneInputNoDerivModPiece> trueModel = GetRandomGenzFunction(type, fnum, dim);
          
      //Wrap the model with a cache
      auto cachedTrueModel = make_shared<CachedModPiece>(trueModel);
          
      //Smolyak object
      auto pceFactory = make_shared<SmolyakPCEFactory>(variableCollection, cachedTrueModel);
          
      //Start construction
      auto pce = pceFactory->StartFixedTerms(initialOrder);
          
      //Start adaptation
      for (unsigned int j=0; j < timeToAdapt.size(); ++j)
        {
          pce = pceFactory->AdaptForTime(timeToAdapt(j), 0);
          
          Eigen::VectorXd estError = 
            EstimateApproximationError( trueModel, pce, dim );

          fevals(i,j)   = static_cast<double>(cachedTrueModel->GetNumOfEvals());
          L2Err(i,j)   = estError(0);
          LinfErr(i,j) = estError(1);
          cout << "L2err = " << L2Err(i,j) << endl;
                    
        };
      
    };

  // open the HDF5 file
  HDF5Wrapper::OpenFile( outFileName );

  // define the group to hold the results of this test
  string group = "/"        + genzType            +
                 "/"        + genzName            +
                 "/dim="    + to_string(dim)      +
                 "/tscale=" + to_string(timescale);
 
  HDF5Wrapper::WriteMatrix( group + "/fevals",  fevals );
  HDF5Wrapper::WriteMatrix( group + "/LinfErr", LinfErr);
  HDF5Wrapper::WriteMatrix( group + "/L2Err",   L2Err  );
      
  //Close file
  HDF5Wrapper::CloseFile( );

};

/**
   \brief Compute the weighted L2 norm of the error between the true function and its Polynomial Chaos approximation.

   \param trueModel the exact analytical function class
   \param pce the Polynomial Chaos approximation to the exact function
   \param dim number of dimensions

   Given the true function \$f \in L^2(\mathbb{R}^d)\$ and its Polynomial Chaos approximation \$\hat{f} \in L^2(\mathbb{R}^d)\$, the weighted \$L^2\$ error \$\frac{\Vert f - \hat{f} \Vert_{L^2}}{\Vert f \Vert_{L^2}}\$ is computed by Monte Carlo simulation.
 **/
Eigen::VectorXd EstimateApproximationError( std::shared_ptr<muq::Modelling::OneInputNoDerivModPiece> trueModel, 
                                            std::shared_ptr<muq::Approximation::PolynomialChaosExpansion> pce,
                                            unsigned int dim)
{
  //number of samples
  unsigned int samples = 1000;
  
  //allocate space for all the errors, and for one at a time
  Eigen::VectorXd termErrors(samples);
  Eigen::VectorXd trueVals(samples);
  Eigen::VectorXd oneError(1);
  Eigen::VectorXd out(2);
  
  //loop over samples
  for(unsigned int i=0; i<samples; ++i)
    {
      //get a random point using the armadillo random fn
      Eigen::VectorXd randPoint = RandomGenerator::GetUniformRandomVector(dim);
      randPoint = 2.0 * randPoint.array() - 1.0; //shift from [0,1] to [-1,1]
      
      //compute the difference between the true and approximation.
      trueVals(i) = (trueModel->Evaluate(randPoint))(0);
      oneError = trueModel->Evaluate(randPoint) - pce->Evaluate(randPoint);
      
      //compute the norm of this one term 
      termErrors(i) = oneError.norm();
    };
    
  //return a tuple with the scaled norm (L_2) and the max (L_\infty)
  // out(0) = termErrors.norm() / sqrt( static_cast<double>(samples) );
  // out(0) /= trueVals.norm() / sqrt( static_cast<double>(samples) );
  out(0) = termErrors.norm() / trueVals.norm();
  out(1) = termErrors.maxCoeff();
  return out;

};
