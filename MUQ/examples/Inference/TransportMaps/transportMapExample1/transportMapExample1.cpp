
#include "MUQ/Inference/TransportMaps/TransportMap.h"

#include "MUQ/Inference/TransportMaps/MapFactory.h"
#include "MUQ/Inference/TransportMaps/BasisFactory.h"
#include "MUQ/Inference/TransportMaps/PolynomialBasis.h"
#include "MUQ/Inference/TransportMaps/BasisFactory.h"

#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Modelling/GaussianRV.h"


using namespace muq::Inference;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;



int main(){
	
    // first, generate a bunch of samples from a univariate lognormal distribution
    const int numSamps = 5e3;
    Eigen::MatrixXd samps(2,numSamps);
	
    // create a univariate standard normal random variable
    auto gaussRv = make_shared<GaussianRV>(2); 
	
	  const double a = 1.0;
	  const double b = 2;
    Eigen::VectorXd tempSamp;
    for(int i=0; i<numSamps; ++i){
        tempSamp = gaussRv->Sample();
        samps(0,i) = a*tempSamp(0);
		    samps(1,i) = tempSamp(1)/a + b*(pow(samps(0,i),2.0)+a*a);
    }


   auto bases = BasisFactory::TotalOrderMonomial(2,3);
  
   // build the map
   auto map = MapFactory::BuildSampsToStdSerial(samps,bases);
  
   auto mapCoeffs = map->GetCoeffs();
  
   // evaluate the forward map at a point
   Eigen::VectorXd mapInput = Eigen::VectorXd::Ones(2);
   Eigen::VectorXd mapOutput = map->Evaluate(mapInput);

   std::cout << "Map input = " << mapInput.transpose() << std::endl;
   std::cout << "Forward evaluation = " << mapOutput.transpose() << std::endl;
   
   
   // evaluate the inverse map at a point
   Eigen::VectorXd inverseInput = mapOutput;
   Eigen::VectorXd inverseGuess = Eigen::VectorXd::Zero(2);
   Eigen::VectorXd inverseOutput = map->EvaluateInverse(inverseInput,inverseGuess);
   
   std::cout << "Inverse input = " << inverseInput.transpose() << std::endl;
   std::cout << "Inverse evaluation = " << inverseOutput.transpose() << std::endl;
  
  
    // print the map multiindices to cout
  for(int d=0; d<2; ++d){
    std::cout << "Multi-indices and coefficients for dimension " << d << std::endl;
    
    for(int term=0; term<bases.at(d).size(); ++term){
      Eigen::VectorXd multi = Eigen::VectorXd::Zero(2);
      
      // cast the general basis to a polynomial
      auto polyTerm = dynamic_pointer_cast<PolynomialBasisBase>(bases.at(d).at(term));
      
      // print the polynomial order
      std::cout << "    MI = [" << polyTerm->GetPower(0);
      for(int dd=1; dd<2; ++dd)
        std::cout << ",  " << polyTerm->GetPower(dd);
      std::cout << "]      ";
      
      // print the coefficient
      std::cout << "Coeff = " << mapCoeffs.at(d)(term) << std::endl;
    }
  }
   
	return 0;
}